# FuelUp-iOS
## How to install
* Clone FuelUp repository
* Run
```
pod install
```
* In project folder: 
```
git clone https://github.com/googlemaps/google-maps-ios-utils.git
```
* Open the umbrella header GMUMarkerClustering.h (under the Google-Maps-iOS-Utils/Cluster group) and change all the import paths to relative. For example, change #import <Google-Maps-iOS-Utils/GMUCluster.h> to #import "GMUCluster.h". (The 'Use Header Map' setting will resolve the relative path correctly).
* Build the project. Full instruction on how to install Google Maps iOS Utils here: https://github.com/googlemaps/google-maps-ios-utils/blob/master/Swift.md

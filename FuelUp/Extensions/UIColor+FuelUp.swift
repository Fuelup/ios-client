//
//  UIColor+PASSCITY.swift
//  PASSCITY
//
//  Created by Алексей on 28.09.17.
//  Copyright © 2017 PASSCITY. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
  func lighter(percentage: CGFloat = 0.1) -> UIColor {
    return self.colorWithBrightness(factor: 1 + abs(percentage))
  }

  func darker(percentage: CGFloat = 0.1) -> UIColor {
    return self.colorWithBrightness(factor: 1 - abs(percentage))
  }

  func colorWithBrightness(factor: CGFloat) -> UIColor {
    var hue: CGFloat = 0
    var saturation: CGFloat = 0
    var brightness: CGFloat = 0
    var alpha: CGFloat = 0

    if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
      return UIColor(hue: hue, saturation: saturation, brightness: brightness * factor, alpha: alpha)
    } else {
      return self
    }
  }

  class var backgroundGrey: UIColor {
    return UIColor(white: 216.0 / 255.0, alpha: 1.0)
  }

  class var disabledBtnColor: UIColor {
    return UIColor(white: 214.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var warmGrey: UIColor {
    return UIColor(white: 153.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var seafoamBlue: UIColor {
    return UIColor(red: 80.0 / 255.0, green: 195.0 / 255.0, blue: 208.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var lowGrey: UIColor {
    return UIColor(red: 214.0 / 255.0, green: 214.0 / 255.0, blue: 214.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var tealish: UIColor {
    return UIColor(red: 35.0 / 255.0, green: 172.0 / 255.0, blue: 203.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var greyishBrown: UIColor {
    return UIColor(white: 64.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var lipstick: UIColor {
    return UIColor(red: 222.0 / 255.0, green: 33.0 / 255.0, blue: 44.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var whiteThree: UIColor {
    return UIColor(white: 240.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var tomato: UIColor {
    return UIColor(red: 229.0 / 255.0, green: 93.0 / 255.0, blue: 35.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var tableBackground: UIColor {
    return UIColor(red: 244.0 / 255.0, green: 244.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var coolGrey: UIColor {
    return UIColor(red: 139.0 / 255.0, green: 143.0 / 255.0, blue: 149.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var greenyBlue: UIColor {
    return UIColor(red: 66.0 / 255.0, green: 189.0 / 255.0, blue: 196.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var silver: UIColor {
    return UIColor(red: 209.0 / 255.0, green: 213.0 / 255.0, blue: 219.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var warmGreyTwo: UIColor {
    return UIColor(white: 126.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var pinkishGrey: UIColor {
    return UIColor(white: 202.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var coolBlue: UIColor {
    return UIColor(red: 63.0 / 255.0, green: 165.0 / 255.0, blue: 186.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var blackTwo: UIColor {
    return UIColor(white: 33.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var aquaMarine: UIColor {
    return UIColor(red: 68.0 / 255.0, green: 219.0 / 255.0, blue: 193.0 / 255.0, alpha: 1.0)
  }


  @nonobjc class var tomatoTwo: UIColor {
    return UIColor(red: 226.0 / 255.0, green: 37.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var bloodOrange: UIColor {
    return UIColor(red: 252.0 / 255.0, green: 79.0 / 255.0, blue: 0.0, alpha: 1.0)
  }

  @nonobjc class var fadedRed: UIColor {
    return UIColor(red: 206.0 / 255.0, green: 71.0 / 255.0, blue: 72.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var dustyBlue: UIColor {
    return UIColor(red: 88.0 / 255.0, green: 165.0 / 255.0, blue: 173.0 / 255.0, alpha: 1.0)
  }
}

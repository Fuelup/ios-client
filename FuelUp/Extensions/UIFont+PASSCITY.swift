//
//  UIFont+PASSCITY.swift
//  PASSCITY
//
//  Created by Алексей on 28.09.17.
//  Copyright © 2017 PASSCITY. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
  class var titleLightFont: UIFont {
    return UIFont.systemFont(ofSize: 22.0, weight: UIFont.Weight.light)
  }

  class var buttonCommonFont: UIFont {
    return UIFont.systemFont(ofSize: 21.0, weight: UIFont.Weight.medium)
  }
}

//
//  String+luhnCheck.swift
//  FuelUp
//
//  Created by Алексей on 18.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation

extension String {
  var luhnCheck: Bool {
    var sum = 0
    let reversedCharacters = self.reversed().map { String($0) }
    for (idx, element) in reversedCharacters.enumerated() {
      guard let digit = Int(element) else { return false }
      switch ((idx % 2 == 1), digit) {
      case (true, 9): sum += 9
      case (true, 0...8): sum += (digit * 2) % 9
      default: sum += digit
      }
    }
    return sum % 10 == 0
  }
}

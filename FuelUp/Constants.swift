//
//  Constants.swift
//  FuelUp
//
//  Created by Алексей on 17.10.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation

struct Constants {
  
  static let apiPath = "https://dev.fuelup.ru/v1"
  
  static let googleMapsApiKey = "AIzaSyDTSsnBmJWu6VxN7mjE1lH6HSiwuAAKhbA"
  static let googleMapsDirectionsKey = "AIzaSyAaGdmLBfgdZQczRF5SrymOmxNToPYaIAg"
  
  static let emailRegexp = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
  static let maxGasTank = 999.0
}



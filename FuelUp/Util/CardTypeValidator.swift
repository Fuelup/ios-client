//
//  CardTypeValidator.swift
//  FuelUp
//
//  Created by Алексей on 01.02.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation

enum CreditCardType: Int {
  case visa = 4
  case mastercard = 5
  case maestro = 6
  case mir = 2

  static var all: [CreditCardType] {
    return [
      .visa,
      .mastercard,
      .maestro,
    ]
  }

  static func test(card: CardData) -> CreditCardType? {
    for type in CreditCardType.all {
      if card.maskedPan.first == "\(type.rawValue)".first {
        return type
      }
    }
    return nil
  }

  var image: UIImage? {
    switch self {
    case .visa:
      return #imageLiteral(resourceName: "visaDarkCopy")
    case .maestro, .mastercard:
      return #imageLiteral(resourceName: "masterCardDark")
    default:
      return #imageLiteral(resourceName: "americanExpressDark")
    }

  }
}

//
//  UIImage+addBorder.swift
//  FuelUp
//
//  Created by Алексей on 04.04.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
  func round(image: UIImage) -> UIImage {
    let imageWidth = image.size.width
    let imageHeight = image.size.height

    let diameter = min(imageWidth, imageHeight)
    let isLandscape = imageWidth > imageHeight

    let xOffset = isLandscape ? (imageWidth - diameter) / 2 : 0
    let yOffset = isLandscape ? 0 : (imageHeight - diameter) / 2

    let imageSize = CGSize(width: diameter, height: diameter)

    return UIGraphicsImageRenderer(size: imageSize).image { _ in

      let ovalPath = UIBezierPath(ovalIn: CGRect(origin: .zero, size: imageSize))
      ovalPath.addClip()
      image.draw(at: CGPoint(x: -xOffset, y: -yOffset))
      UIColor.white.setStroke()
      ovalPath.lineWidth = diameter / 50
      ovalPath.stroke()
    }
  }
}

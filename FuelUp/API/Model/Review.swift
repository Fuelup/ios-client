//
//  Review.swift
//  FuelUp
//
//  Created by Алексей on 28.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

class Review: Mappable {
  var id: String = ""
  var comment: String = ""
  var rate: Int = 0

  required init?(map: Map) { }

  init() { }

  func mapping(map: Map) {
    id <- map["id"]
    comment <- map["comment"]
    rate <- map["rate"]
  }
}

//
//  Order.swift
//  FuelUp
//
//  Created by Алексей on 22.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

struct CreateOrder {
  var station: String
  var pump: Int = 0
  var fuel: String
  var value: Int
  var cost: Double
  var payment: String
}

class Order: Mappable {
  var id: String = ""
  var gasStationId: String = ""
  var pump: Int = 0
  var fuelType: Int = 0
  var orderType: OrderType = .money
  var amount: Int = 0
  var cost: Int = 0
  var dtFinish: Date = Date()
  var dateCreated: Date = Date()
  var checkUrl: String = ""
  var status: OrderStatus = .creating
  var gasStation: GasStation? = nil
  var statusRaw: Int {
    get {
      return status.rawValue
    }
    set {
      status  =                                                                                                                                                                                                                                                                                                                                         OrderStatus(rawValue: newValue) ?? .incorrectParams
    }
  }

  var amountRubles: Double {
    get {
      return Double(amount) / 100
    }
    set {
      amount = Int(newValue * 100)
    }
  }

  var costRubles: Double {
    get {
      return Double(cost) / 100
    }
    set {
      amount = Int(newValue * 100)
    }
  }

  required init?(map: Map) { }

  init() { }

  func mapping(map: Map) {
    id <- map["id"]
    gasStationId <- map["gas_station_id"]
    pump <- map["pump_num"]
    fuelType <- map["fuel_id"]
    amountRubles <- map["order_count"]
    if map.mappingType == .fromJSON {
      dateCreated <- (map["date_created"], DateTransform())
      dtFinish <- (map["dt_finish"], DateTransform())
      statusRaw <- map["status"]
      checkUrl <- map["check_url"]
      cost <- map["cost"]
      gasStation <- map["gas_station"]
    }
    orderType <- (map["order_type"], EnumTransform<OrderType>())
  }
}

enum OrderType: String {
  case liter
  case money
}

enum OrderStatus: Int {
  case creating = 1
  case holding = 2
  case holdingMoney = 3
  case fueling = 4
  case finishedFull = 5
  case interruptedByUser = 6
  case interruptedByService = 7
  case interruptedAccident = 8
  case pumpNotAvailable = 9
  case incorrectParams = 10

  var success: Bool {
    switch self {
    case .finishedFull, .interruptedByUser, .interruptedByService:
      return true
    default:
      return false
    }
  }

  var error: Bool {
    switch self {
    case .interruptedAccident, .pumpNotAvailable, .incorrectParams:
      return true
    default:
      return false
    }
  }

  var message: String? {
    switch self {
    case .creating:
      return "Создание заказа"
    case .holding, .holdingMoney:
      return "Проверка наличия средств"
    case .fueling:
      return "Заправка в процессе"
    case .finishedFull:
      return nil
    case .interruptedByUser:
      return "Отмена"
    case .interruptedAccident, .interruptedByService:
      return "Колонка отключена"
    case .pumpNotAvailable:
      return "Колонка недоступна"
    case .incorrectParams:
      return "Ошибка"
    }
  }

  var description: String? {
    switch self {
    case .creating:
      return "Создание заказа"
    case .holding, .holdingMoney:
      return "Проверка наличия средств"
    case .fueling:
      return "Заправка в процессе"
    case .interruptedByUser:
      return "Отмена была произведена успешно. Неиспользованные средства будут возвращены на вашу карту"
    case .interruptedAccident:
      return "Отказ выполнения, авария в СУ АЗС. Неиспользованные средства на вашей карте будут разморожены"
    case .interruptedByService:
      return "Колонка была отключена оператором"
    default:
      return nil
    }
  }
}

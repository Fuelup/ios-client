//
//  Settings.swift
//  FuelUp
//
//  Created by Алексей on 14.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

struct Settings: Mappable, Equatable {
  var gasTankVolume: Double = 0
  var alwaysFull: Bool = false
  var email: String = ""
  
  init?(map: Map) {
  }

  init() { }

  mutating func mapping(map: Map) {
    gasTankVolume <- map["volume"]
    alwaysFull <- map["always_full"]
    email <- map["email"]
  }

  static var current: Settings? {
    get {
      guard let string: String = StorageHelper.loadObjectForKey(.currentSettings) else { return nil }
      return Mapper<Settings>().map(JSONString: string)
    }
    set {
      try? StorageHelper.save(newValue?.toJSONString(), forKey: .currentSettings)
    }
  }

  static func ==(lhs: Settings, rhs: Settings) -> Bool {
    return lhs.alwaysFull == rhs.alwaysFull && lhs.gasTankVolume == rhs.gasTankVolume
  }
}

//
//  Company.swift
//  FuelUp
//
//  Created by Алексей on 15.01.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

class Company: Mappable {
  var id: String = ""
  var name: String = ""
  // Bonus
  var isActive: Bool = false
  var url: URL? = nil
  var cardLength: Int = 0

  required init?(map: Map) { }

  init() { }

  func mapping(map: Map) {
    id <- map["_id"]
    isActive <- map["bonus.is_active"]
    url <- (map["bonus.url"], URLTransform())
    cardLength <- map["bonus.card_length"]
    name <- map["name"]
  }

}

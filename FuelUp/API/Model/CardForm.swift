//
//  CardForm.swift
//  FuelUp
//
//  Created by Mikhail Mukminov on 21/06/2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit
import ObjectMapper

class CardForm: Mappable {
  
  var url: String = ""
  var post: String?
  var successUrl: String = ""
  var failUrl: String = ""
  
  required init?(map: Map) {
    guard map["url"].value() != nil, map["successUrl"].value() != nil, map["failUrl"].value() != nil else {
      return nil
    }
  }
  
  init() { }
  
  func mapping(map: Map) {
    url <- map["url"]
    post <- map["post"]
    successUrl <- map["successUrl"]
    failUrl <- map["failUrl"]
  }
}

//
//  FuelType.swift
//  FuelUp
//
//  Created by Mikhail Mukminov on 28/06/2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

class Fuel: Mappable {

  var id: Int = 0
  var name: String = ""
  var code: String = ""
  
  required init(map: Map) {
    
  }
  
  func mapping(map: Map) {
    id <- map["id"]
    code <- map["code"]
    name <- map["name"]
  }
}

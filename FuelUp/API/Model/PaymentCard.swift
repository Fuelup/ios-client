//
//  PaymentCard.swift
//  FuelUp
//
//  Created by Алексей on 01.02.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentCard: Mappable {
  var pan: String = ""
  var cvc: String = ""
  var yyyy: String = ""
  var mm: String = ""
  var text: String = ""

  required init?(map: Map) { }

  init() { }

  func mapping(map: Map) {
    pan <- map["pan"]
    cvc <- map["cvc"]
    yyyy <- map["yyyy"]
    mm <- map["mm"]
    text <- map["text"]
  }

}

private var dateKey = "date"

class CardData: Mappable {
  
  var bindingId: String = ""
  var maskedPan: String = ""
  var expiryDate: String = ""
  
  var maskedLastDigitsPan: String {
    return "**** **** **** " + String(maskedPan.suffix(4))
  }
  
  var isActive: Bool {
    get {
      guard let selectedCardId: String? = StorageHelper.loadObjectForKey(.currentCard) else { return false }
      return bindingId == selectedCardId
    }
    set {
      try? StorageHelper.save(bindingId, forKey: .currentCard)
    }
  }
  
  var createdDate: Date {
    guard let date = UserDefaults.standard.object(forKey: bindingId + dateKey) as? Date else {
      let date = Date()
      UserDefaults.standard.set(date, forKey: bindingId + dateKey)
      return date
    }
    return date
  }
  
  var cardType: CreditCardType? {
    return CreditCardType.test(card: self)
  }
  
  required init?(map: Map) {
    guard map["bindingId"].currentValue != nil else {
      return nil
    }
    self.mapping(map: map)
  }
  
  func mapping(map: Map) {
    bindingId <- map["bindingId"]
    maskedPan <- map["maskedPan"]
    expiryDate <- map["expiryDate"]
  }
}

class TaxiData: Mappable {
  
  var identifier: Int = 0
  var company: String = ""
  var balance: Float = 0
  var logo: String?
  
  required init?(map: Map) {
    guard map["company"].currentValue != nil else {
      return nil
    }
    self.mapping(map: map)
  }
  
  func mapping(map: Map) {
    identifier <- map["id"]
    company <- map["company"]
    balance <- map["balance"]
    logo <- map["logo"]
  }
}

class PaymentHistoryData: Mappable {
  
  enum PaymentType {
    case taxi(TaxiData)
    case card(CardData)
  }
  
  var identifier: String {
    return cardData?.bindingId ?? "\(taxiData!.identifier)"
  }
  
  var cardData: CardData? = nil
  var taxiData: TaxiData? = nil
  
  var type: PaymentType {
    if let cardData = cardData {
      return .card(cardData)
    }
    if let taxiData = taxiData {
      return .taxi(taxiData)
    }
    fatalError()
  }
  
  required init?(map: Map) {
    guard map["company"].currentValue != nil || map["bindingId"].currentValue != nil else {
      return nil
    }
  }
  
  func mapping(map: Map) {
    switch map.mappingType {
    case .fromJSON:
      cardData = CardData(map: map)
      taxiData = TaxiData(map: map)
    case .toJSON: do {
      print(self.type)
      switch self.type {
      case .card(let card):
        card.mapping(map: map)
      case .taxi(let taxi):
        taxi.mapping(map: map)
      }
      }
    }
  }
}

//        return [
//          "bindingId": card.bindingId,
//          "maskedPan": card.maskedPan,
//          "expiryDate": card.expiryDate
//        ]
//        return [
//          "id": taxi.identifier,
//          "company": taxi.company,
//          "balance": taxi.balance,
//          "logo": taxi.logo ?? NSNull()
//        ]

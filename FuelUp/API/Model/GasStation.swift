//
//  GasStation.swift
//  FuelUp
//
//  Created by Алексей on 30.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

class GasStation: Mappable {
  var id: String = ""
  var address: String = ""
  var name: String = ""
  var isActive: Bool = true
  var coordinates: CLLocationCoordinate2D? = nil
  var fuelTypes: [FuelType] = []
  var pumps: [Pump] = []

  var minified: Bool = true

  let numberFormatter = NumberFormatter()

  init() {

  }
  
  required init(map: Map) {

  }

  func mapping(map: Map) {
    id <- map["id"]
    address <- map["address"]
    name <- map["name"]
    isActive <- map["is_active"]
    coordinates <- (map["coordinates"], CoordinateTransform())
    fuelTypes <- map["fuel_types"]
    pumps <- map["pumps"]
  }

  func distanceTo(_ point: CLLocationCoordinate2D?) -> Double {
    guard let point = point, let coordinates = coordinates else { return 0 }
    let coordinatesLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
    let pointLocation = CLLocation(latitude: point.latitude, longitude: point.longitude)

    return coordinatesLocation.distance(from: pointLocation) / 1000
  }

  func distanceToString(_ point: CLLocationCoordinate2D?) -> String {
    let distance = distanceTo(point)
    numberFormatter.maximumFractionDigits = 1
    numberFormatter.minimumIntegerDigits = 1
    let distString = numberFormatter.string(from: NSNumber(value: distance)) ?? ""
    return "\(distString) км"
  }
}

struct FuelType: Mappable {
  var id: Int = 0
  var name: String = ""
  var price: Double = 0
  var code: String = ""

  init?(map: Map) {
  }

  mutating func mapping(map: Map) {
    id <- map["fuel"]
    let fuel = GasStationsServce.instance.fuels.first(where: { $0.id == id })
    name = fuel?.name ?? ""
    code = fuel?.code ?? ""
    price <- map["price"]
    
  }
}

struct CoordinateTransform: TransformType {
  typealias Object = CLLocationCoordinate2D
  typealias JSON = [String: Double]

  public func transformFromJSON(_ value: Any?) -> Object? {
    guard let value = value as? [String: Double] else { return nil }
    guard let latitude = value["lat"], let longitude = value["lon"] else { return nil }
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }

  public func transformToJSON(_ value: Object?) -> JSON? {
    guard let location = value else { return [:] }
    return [
      "lat": location.latitude,
      "lon": location.longitude
    ]
  }

}

extension CLLocationCoordinate2D: Equatable {
  public static func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return lhs.latitude == rhs.longitude
  }
}

//
//  Profile.swift
//  FuelUp
//
//  Created by Алексей on 26.02.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

class Profile: Mappable {
  var phoneNumber: String = ""
  var email: String = ""
  var emailConfirm: Bool = false
  var bindingId: String = ""
  var isActive: Bool = false
  var companies: [String: BonusCardData] = [:]

  required init?(map: Map) { }

  init() { }

  func mapping(map: Map) {
    phoneNumber <- map["phone"]
    email <- map["email"]
  }

  struct BonusCardData: Mappable {
    var card: String = ""

    init?(map: Map) { }

    mutating func mapping(map: Map) {
      card <- map["card"]
    }

  }
}

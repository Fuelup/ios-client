//
//  Pump.swift
//  FuelUp
//
//  Created by Алексей on 13.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import ObjectMapper

struct Pump: Mappable {
  var num: Int = 1
  var isBlock: Bool = false
  var fuelTypes: [FuelType] = []
  
  init?(map: Map) { }
  
  init() { }
  
  mutating func mapping(map: Map) {
    num <- map["num"]
//    isBlock <- map["is_block"]
    fuelTypes <- map["fuel_types"]
  }
}

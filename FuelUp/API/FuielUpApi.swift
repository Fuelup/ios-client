//
//  FuielUpApi.swift
//  FuelUp
//
//  Created by Алексей on 28.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Result
import Moya
import SwiftyJSON

enum DataError: Error {
  case unknown
  case smsValidationFailded
  case incorrectNumber

  case serverError(String?)
  case backendError(Int)

  var localizedDescription: String {
    switch self {
    case .unknown:
      return "Неизвестная ошибка"
    case .smsValidationFailded:
      return "Ошибка проверки кода"
    case .incorrectNumber:
      return "Неизвестная ошибка"
    case .serverError(let string):
      return string ?? "Ошибка сервера"
    case .backendError(let code):
      switch code {
      case 14:
        return "Ошибка сервиса Альфа-Банк"
      case 15:
        return "Карта с данным номером не валидна, пожалуйста введите другую карточку"
      case 16:
        return "Связки с таким id нет"
      case 19:
        return "Ошибка оплаты"
      case 21:
        return "Повторно отправить сообщение пока нельзя"
      case 22:
        return "Превышен лимит отправки кодов на этот номер"
      default:
        return "Ошибка сервера"
      }
    }
  }
}

typealias ServiceResult<Object> = Result<Object, DataError>
typealias ServiceCompletion<Object> = ((ServiceResult<Object>) -> Void)


struct FuelUpApiUtils {
  static func providerWithPlugins<T: TargetType>() -> MoyaProvider<T> {
    var plugins: [PluginType] = []

    let networkActivityPlugin = NetworkActivityPlugin(networkActivityClosure: { state, _  in
      switch state {
      case .began:
        NetworkActivityIndicator.sharedIndicator.visible = true
      case .ended:
        NetworkActivityIndicator.sharedIndicator.visible = false
      }
    })
    plugins.append(networkActivityPlugin)
    plugins.append(NetworkLoggerPlugin(verbose: true))
    plugins.append(BearerTokenPlugin())

    return RetryMoyaProvider<T>(plugins: plugins)
  }
}

class RetryMoyaProvider<Target>: MoyaProvider<Target> where Target: TargetType {
  
  func validateError(json: JSON) -> DataError? {
    if let error = json["error"].int, error != 0 {
      return DataError.backendError(error)
    }
    return nil
  }
  
  override func request(_ target: Target, callbackQueue: DispatchQueue?,
                        progress: ProgressBlock?,
                        completion: @escaping Completion) -> Cancellable {
    return _request(tryCount: 1, target, callbackQueue: callbackQueue, progress: progress, completion: completion)
  }
  
  @discardableResult
  private func _request(tryCount: Int, _ target: Target,
                        callbackQueue: DispatchQueue?,
                        progress: ProgressBlock?,
                        completion: @escaping Completion) -> Cancellable {
    return super.request(target, callbackQueue: callbackQueue, progress: progress) { (result) in
      if target is AuthenticationTarget {
        switch result {
        case .failure(_):
          ProfileService.instance.logout()
        default: break
        }
        completion(result)
        return
      }
      
      switch result {
      case .success(let response):
        if (try? response.filterSuccessfulStatusCodes()) != nil {
          completion(result)
        }
        else {
          if response.statusCode == 403 {
            BearerTokenPlugin.token = nil
            if tryCount > 1 {
              ProfileService.instance.logout()
              return
            }
            ProfileService.instance.needRefreshToken(completion: { (tokenResponse) in
              switch tokenResponse {
              case .success(_):
                self._request(tryCount: tryCount+1, target, callbackQueue: callbackQueue, progress: progress, completion: completion)
              case .failure(_):
                completion(result)
              }
            })
          }
          else {
            completion(result)
          }
        }
      case .failure(let error):
        switch error {
        case .statusCode(let response):
          if response.statusCode == 403 {
            BearerTokenPlugin.token = nil
            if tryCount > 1 {
              ProfileService.instance.logout()
              return
            }
            ProfileService.instance.needRefreshToken(completion: { (tokenResponse) in
              switch tokenResponse {
              case .success(_):
                self._request(tryCount: tryCount+1, target, callbackQueue: callbackQueue, progress: progress, completion: completion)
              case .failure(_):
                completion(result)
              }
            })
          }
        default:
          completion(result)
          break
        }
      }
    }
  }


}

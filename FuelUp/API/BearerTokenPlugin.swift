//
//  BearerTokenPlugin.swift
//  FuelUp
//
//  Created by Алексей on 28.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Moya

public struct BearerTokenPlugin: PluginType {

  static var token: String? {
    get {
      let accessToken: String? = StorageHelper.loadObjectForKey(.accessToken)
      return accessToken
    }
    set {
      try? StorageHelper.save(token, forKey: .accessToken)
    }
  }

  private var authVal: String? {
    guard let token = BearerTokenPlugin.token else { return nil }
    return token
  }

  public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {

    var request = request
    if let authVal = authVal {
      request.addValue(authVal, forHTTPHeaderField: "X-AuthToken")
    }

    return request
  }
}

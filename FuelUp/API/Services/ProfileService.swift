//
//  AuthorizationManager.swift
//  FuelUp
//
//  Created by Алексей on 27.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import SwiftyJSON

class ProfileService {
  static let instance = ProfileService()
  
  private let authProvider: MoyaProvider<AuthenticationTarget> = FuelUpApiUtils.providerWithPlugins()

  private var token: String? {
    didSet {
      try? StorageHelper.save(token, forKey: .accessToken)
      if token != nil {
        BearerTokenPlugin.token = token
        fetchProfile(completion: { _ in })
      }
    }
  }
  
  private var refreshToken: String? {
    didSet {
      try? StorageHelper.save(refreshToken, forKey: .refreshToken)
    }
  }

  var profile: Profile? = nil {
    didSet {
      try? StorageHelper.save(profile?.toJSONString(), forKey: .currentProfile)
    }
  }
  
  var tempPhone: String? = nil
  
  var isAuthorized: Bool {
    get {
      return token != nil
    }
    set {
      guard !newValue else { return }
      self.token = nil
    }
  }
  
  private init() {
    token = StorageHelper.loadObjectForKey(.accessToken)
    refreshToken = StorageHelper.loadObjectForKey(.refreshToken)
    guard let profileJSON: String = StorageHelper.loadObjectForKey(.currentProfile) else { return }
    profile = Mapper<Profile>().map(JSONString: profileJSON)
  }
  
  func logout() {
    isAuthorized = false
    profile = nil
    DispatchQueue.main.async {
      RootViewController.instance.setAuthViewController()
    }
  }
  
  func auth(phoneString: String? = nil, completion: @escaping ServiceCompletion<Void>) {
    self.tempPhone = phoneString ?? tempPhone
    authProvider.request(AuthenticationTarget.sendCode(phoneNumber: phoneString ?? "")) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = self.validateError(json: json) {
          completion(.failure(error))
        } else {
          completion(.success(()))
        }
      case .failure(let error):
        completion(.failure(DataError.serverError(error.errorDescription)))
      }
    }
  }
  
  func validateError(json: JSON) -> DataError? {
    if let string = json.string, string.lowercased().contains("error") {
      return DataError.serverError(string)
    }
    guard let errorJSON = json.dictionaryValue["error"] else {
      return DataError.serverError(nil)
    }
    if let code = errorJSON.int, code == 0 {
      return nil
    } else {
      return DataError.serverError(errorJSON.stringValue)
    }
  }
  
  func checkCode(code: String, completion: @escaping ServiceCompletion<Void>) {
    guard let phone = tempPhone else { return }
    authProvider.request(.login(phoneNumber: phone, code: code)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = self.validateError(json: json) {
          completion(.failure(error))
        } else {
          if let token = json["access_token"].string {
            self.token = token
          }
          if let token = json["refresh_token"].string {
            self.refreshToken = token
          }
          completion(.success(()))
        }
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }
  
  func needRefreshToken(completion: @escaping ServiceCompletion<Void>) {
    authProvider.request(.refreshTokens(refreshToken: self.refreshToken ?? "")) { (result) in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = self.validateError(json: json) {
          completion(.failure(error))
        } else {
          if let token = json["access_token"].string {
            self.token = token
          }
          if let token = json["refresh_token"].string {
            self.refreshToken = token
          }
          completion(.success(()))
        }
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }
  
  func fetchProfile(completion: @escaping ServiceCompletion<Profile>) {
    authProvider.request(.fetchProfile) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = self.validateError(json: json) {
          completion(.failure(error))
        } else {
          let json = JSON(response.data)
          let result = json["result"].rawValue
          guard let profile = Mapper<Profile>().map(JSONObject: result) else {
            completion(.failure(DataError.unknown))
            return
          }
          self.profile = profile
          completion(.success(profile))
        }
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }
  
  func updateEmail(newEmail: String, completion: @escaping ServiceCompletion<Void>) {
    authProvider.request(.updateEmail(newEmail)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = self.validateError(json: json) {
          completion(.failure(error))
        } else {
          let profile = self.profile
          profile?.email = newEmail
          self.profile = profile
          completion(.success(()))
        }
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
    
  }
  
  func updatePushToken(token: String, completion: @escaping ServiceCompletion<Void>) {
    authProvider.request(.setPushToken(token: token)) { result in
      switch result {
      case .success(_):
        completion(.success(()))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func addBonusCard(card: String, companyId: String, completion: @escaping ServiceCompletion<Void>) {
    authProvider.request(.addBonusCard(id: companyId, code: card)) { result in
      switch result {
      case .success(_):
        self.fetchProfile(completion: { result in
          completion(.success(()))
        })
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func removeBonusCard(id: String, completion: @escaping ServiceCompletion<Void>) {
    authProvider.request(.removeBonusCard(id: id)) { result in
      switch result {
      case .success(_):
        self.fetchProfile(completion: { result in
          completion(.success(()))
        })
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }
}


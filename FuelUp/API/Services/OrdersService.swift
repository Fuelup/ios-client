//
//  OrdersService.swift
//  FuelUp
//
//  Created by Алексей on 12.02.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation

import Moya
import ObjectMapper
import SwiftyJSON

class OrdersService {
  static let instance = OrdersService()
  private let gasStationsProvider: MoyaProvider<GasStationsTarget> = FuelUpApiUtils.providerWithPlugins()

  private var orderTimer: Timer? = nil
  var currentHandler: ((Order) -> Void)? = nil
  private var currentOrder: Order? = nil
  private var currentRequest: Cancellable? = nil

  private init() {
  }

  func createOrder(order: Order, _ completion: @escaping ServiceCompletion<Order>) -> Cancellable {
    return gasStationsProvider.request(.createOrder(order)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].dictionaryValue
        guard let id = result["id"]?.string else {
          completion(.failure(DataError.unknown))
          return
        }
        order.id = id
        completion(.success(order))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func fetchOrder(orderId: String, _ completion: @escaping ServiceCompletion<Order>) -> Cancellable {
    return gasStationsProvider.request(.fetchOrder(orderId)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].rawValue
        guard let order = Mapper<Order>().map(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        completion(.success(order))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func cancelOrder(orderId: String, _ completion: @escaping ServiceCompletion<Void>) {
    gasStationsProvider.request(.cancelOrder(orderId)) { result in
      switch result {
      case .success(_):
        completion(.success(()))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func beginRefreshOrder(_ order: Order, _ handler: @escaping ((Order) -> Void)) {
    self.currentHandler = handler
    self.currentOrder = order
    orderTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.refreshOrder), userInfo: nil, repeats: true)
  }

  @objc private func refreshOrder() {
    currentRequest?.cancel()
    guard let order = currentOrder else { return }
    currentRequest = fetchOrder(orderId: order.id, { result in
      switch result {
      case .success(let orderData):
        self.currentHandler?(orderData)
        if orderData.status.success || orderData.status.error {
          self.orderTimer?.invalidate()
          self.orderTimer = nil
        }
      case .failure(_):
        return
      }
    })
  }
}

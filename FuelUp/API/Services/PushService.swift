//
//  PushService.swift
//  FuelUp
//
//  Created by Mikhail Mukminov on 30/05/2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit
import UserNotifications

class PushService: NSObject {
  
  static let instance = PushService()
  
  func registerForPushNotifications() {
    UNUserNotificationCenter.current().delegate = self
    UNUserNotificationCenter.current()
      .requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
      print("Permission granted: \(granted)")
      guard granted else { return }
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    }
  }
  
  func sendPushToken(token: String) {
    ProfileService.instance.updatePushToken(token: token) { (result) in
      if let error = result.error {
        print(error)
      }
    }
  }

}

extension PushService: UNUserNotificationCenterDelegate {
  
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
                              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void)
  {
    
  }
  
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Swift.Void)
  {
    
  }
}

//
//  GasStationsManager.swift
//  FuelUp
//
//  Created by Алексей on 01.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import SwiftyJSON

class GasStationsServce {
  static let instance = GasStationsServce()

  var fuels: [Fuel] = []
  var gasStations: [GasStation] = []
  var orders: [Order] = []
  var companies: [Company] = []

  private let gasStationsProvider: MoyaProvider<GasStationsTarget> = FuelUpApiUtils.providerWithPlugins()
  
  func fetchFuels(_ completion: @escaping ServiceCompletion<[Fuel]>) {
    gasStationsProvider.request(.fetchFuels) { (result) in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].rawValue
        guard let fuels = Mapper<Fuel>().mapArray(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        self.fuels = fuels
        completion(.success(fuels))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func fetchStations(_ completion: @escaping ServiceCompletion<[GasStation]>) {
    gasStationsProvider.request(.fetchGasStations) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].rawValue
        guard let stations = Mapper<GasStation>().mapArray(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        self.gasStations = stations
        completion(.success(stations))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  @discardableResult
  func fetchStation(id: String, _ completion: @escaping ServiceCompletion<GasStation>) -> Cancellable {
    return gasStationsProvider.request(.fetchGasStation(id)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].rawValue
        guard let station = Mapper<GasStation>().map(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        station.minified = false
        completion(.success(station))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  @discardableResult
  func createOrder(order: Order, _ completion: @escaping ServiceCompletion<Order>) -> Cancellable {
    return gasStationsProvider.request(.createOrder(order)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].dictionaryValue
        guard let id = result["id"]?.string else {
          completion(.failure(DataError.unknown))
          return
        }
        order.id = id
        completion(.success(order))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func createReview(review: Review, _ completion: @escaping ServiceCompletion<Void>) {
    gasStationsProvider.request(.createReview(review)) { result in
      switch result {
      case .success(_):
        completion(.success(()))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func fetchOrder(orderId: String, _ completion: @escaping ServiceCompletion<Order>) {
    gasStationsProvider.request(.fetchOrder(orderId)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].rawValue
        guard let order = Mapper<Order>().map(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        completion(.success(order))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func fetchOrders(_ completion: @escaping ServiceCompletion<[Order]>) {
    gasStationsProvider.request(.fetchOrders) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].dictionaryValue["items"]?.rawValue
        guard let orders = Mapper<Order>().mapArray(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        self.orders = orders
        completion(.success(orders))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

  func fetchCompanies(_ completion: @escaping ServiceCompletion<[Company]>) {
    gasStationsProvider.request(.fetchCompanies) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        let result = json["result"].rawValue
        guard let companies = Mapper<Company>().mapArray(JSONObject: result) else {
          completion(.failure(DataError.unknown))
          return
        }
        self.companies = companies
        completion(.success(companies))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

}


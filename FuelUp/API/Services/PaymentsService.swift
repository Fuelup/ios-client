//
//  PaymentsService.swift
//  FuelUp
//
//  Created by Алексей on 18.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import SwiftyJSON

extension Array where Element:PaymentHistoryData {
  
  func sorted() -> [PaymentHistoryData] {
    return self.sorted { (first, second) -> Bool in
      if first.taxiData != nil && second.cardData != nil {
        return true
      }
      if first.cardData != nil && second.taxiData != nil {
        return true
      }
      if let firstDate = first.cardData?.createdDate, let secondDate = second.cardData?.createdDate {
        return firstDate > secondDate
      }
      if let firstName = first.taxiData?.company, let secondName = second.taxiData?.company {
        return firstName < secondName
      }
      return false
    }
  }
  
}

class PaymentsService {
  static let instance = PaymentsService()
  private let paymentsProvider: MoyaProvider<PaymentTarget> = FuelUpApiUtils.providerWithPlugins()

  private init() {
    guard let paymentsString: String = StorageHelper.loadObjectForKey(.payments) else { return }
    let payments = Mapper<PaymentHistoryData>().mapArray(JSONString: paymentsString) ?? []
    self.payments = payments.sorted()
  }
  
  var payments: [PaymentHistoryData] = [] {
    didSet {
      try? StorageHelper.save(payments.toJSONString(), forKey: .payments)
    }
  }
  
  var currentCard: PaymentHistoryData? {
    return payments.first(where: { $0.cardData?.isActive == true })
  }
  
  func parseList(json: JSON) -> (result: [PaymentHistoryData], error: DataError?) {
    if let error = json["error"].int, error != 0 {
      return ([], DataError.backendError(error))
    }
    let cardsResult = json["list"].rawValue
    guard let payments = Mapper<PaymentHistoryData>().mapArray(JSONObject: cardsResult) else {
      return ([], DataError.unknown)
    }
    let result = payments.sorted()
    if result.count == 1 {
      result.first?.cardData?.isActive = true
    }
    return (result, nil)
  }

  func refreshCardList(completion: @escaping ServiceCompletion<[PaymentHistoryData]>) {
    paymentsProvider.request(.fetchCards) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = json["error"].int, error != 0 {
          completion(.failure(DataError.backendError(error)))
          return
        }
        let cardsJson = json["cards"]
        let cardsResult = self.parseList(json: cardsJson)
        if let error = cardsResult.error {
          completion(.failure(error))
        }
        let taxisJson = json["taxis"]
        let taxiResult = self.parseList(json: taxisJson)
        if let error = taxiResult.error {
          completion(.failure(error))
        }
        self.payments = cardsResult.result + taxiResult.result
        completion(.success(self.payments))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.errorDescription)))
      }
    }
  }

  func addCard(card: PaymentCard, _ completion: @escaping ServiceCompletion<CardForm?>) {
    paymentsProvider.request(.addCard(card)) { result in
      switch result {
      case .success(let response):
        let json = JSON(response.data)
        if let error = json["error"].int, error != 0 {
          completion(.failure(DataError.backendError(error)))
          return
        }
        let cardForm = Mapper<CardForm>().map(JSONObject: json.rawValue)
        completion(.success((cardForm)))
      case .failure(let error):
        completion(.failure(DataError.serverError(error.errorDescription)))
      }
    }
  }

  func deleteCard(card: PaymentHistoryData, _ completion: @escaping ServiceCompletion<Void>) {
    if card.cardData != nil {
      paymentsProvider.request(.deleteCard(card.identifier)) { result in
        switch result {
        case .success(let response):
          let json = JSON(response.data)
          if let error = json["error"].int, error != 0 {
            completion(.failure(DataError.backendError(error)))
            return
          }
          if card.cardData!.isActive {
            card.cardData!.isActive = false
          }
          if let index = self.payments.index(where: { $0.identifier == card.identifier }) {
            self.payments.remove(at: index)
          }
          completion(.success(()))
        case .failure(let error):
          completion(.failure(DataError.serverError(error.errorDescription)))
        }
      }
    }
  }
  
}

//
//  GasStationsTarget.swift
//  FuelUp
//
//  Created by Алексей on 01.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Moya
import ObjectMapper

enum GasStationsTarget: TargetType {
  var task: Task {
    switch self {
    case .fetchGasStations, .fetchFuels:
      return .requestPlain
    default:
      switch method {
      case .post:
        return .requestParameters(parameters: params, encoding: JSONEncoding.default)
      default:
        return .requestParameters(parameters: params, encoding: URLEncoding.default)
      }
    }
  }

  case fetchFuels
  case fetchGasStations
  case fetchGasStation(String)
  case createOrder(Order)
  case createReview(Review)
  case fetchOrders
  case fetchOrder(String)
  case cancelOrder(String)
  case fetchCompanies

  var method: Moya.Method {
    switch self {
    case .createReview(_), .cancelOrder(_):
      return .put
    case .createOrder(_):
      return .post
    default:
      return .get
    }
  }

  var sampleData: Data {
    return Data()
  }

  var params: [String: Any] {
    switch self {
    case .fetchGasStation(let id):
      return [
        "id": id
      ]
    case .fetchOrder(let orderId):
      return [
        "id": orderId
      ]
    case .cancelOrder(let orderId):
      return [
        "id": orderId
      ]
    case .createOrder(let order):
      return order.toJSON()
    case .createReview(let review):
      return review.toJSON()
    case .fetchOrders:
      return [
        "page_num": 1,
        "page_size": 10
      ]
    default:
      return [:]
    }
  }

  var headers: [String : String]? {
    return nil
  }


  var path: String {
    switch self {
    case .fetchFuels:
      return "/fuels"
    case .fetchGasStation(_):
      return "/gas-station"
    case .fetchGasStations:
      return "/gas-stations"
    case .createOrder(_), .fetchOrder(_):
      return "/order"
    case .cancelOrder(_):
      return "/order/cancel"
    case .createReview(_):
      return "/order/review"
    case .fetchOrders:
      return "/orders"
    case .fetchCompanies:
      return "/companies"
    }
  }

  var baseURL: URL {
    return URL(string: Constants.apiPath)!
  }

}

//
//  PaymentTarget.swift
//  FuelUp
//
//  Created by Алексей on 01.02.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

enum PaymentTarget: TargetType {
  var task: Task {
    switch self {
    case .fetchCards:
      return .requestPlain
    case .addCard(_), .deleteCard(_):
      return .requestParameters(parameters: params, encoding: JSONEncoding.default)
    }
  }

  case fetchCards
  case addCard(PaymentCard)
  case deleteCard(String)

  var method: Moya.Method {
    switch self {
    case .addCard(_):
      return .post
    case .deleteCard(_):
      return .delete
    default:
      return .get
    }
  }

  var sampleData: Data {
    return Data()
  }

  var params: [String: Any] {
    switch self {
    case .addCard(let card):
      return card.toJSON()
    case .deleteCard(let bindingId):
      return [
        "bindingId": bindingId
      ]
    default:
      return [:]
    }
  }

  var headers: [String : String]? {
    return nil
  }


  var path: String {
    switch self {
    case .addCard(_), .deleteCard(_):
      return "/card"
    case .fetchCards:
      return "/cards"
    }
  }

  var baseURL: URL {
    return URL(string: Constants.apiPath)!
  }

}

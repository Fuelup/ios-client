//
//  ProfileTarget.swift
//  FuelUp
//
//  Created by Алексей on 27.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Moya
import ObjectMapper

enum AuthenticationTarget: TargetType {
  //Auth
  case sendCode(phoneNumber: String)
  case login(phoneNumber: String, code: String)
  case refreshTokens(refreshToken: String)
  
  case fetchProfile
  case updateEmail(String)
  case setPushToken(token: String)
  case addBonusCard(id: String, code: String)
  case removeBonusCard(id: String)
  
  var method: Moya.Method {
    switch self {
    case .fetchProfile:
      return .get
    case .updateEmail(_), .addBonusCard(_, _):
      return .put
    case .removeBonusCard(_):
      return .delete
    default:
      return .post
    }
  }
  
  var sampleData: Data {
    return Data()
  }
  
  var params: [String: Any] {
    switch self {
    case .sendCode(let phoneNumber):
      return [
        "phone": phoneNumber,
        "sign": ""
      ]
    case .login(let phoneNumber, let code):
      return [
        "phone": phoneNumber,
        "code": code
      ]
    case .refreshTokens(let refreshToken):
      return [
        "refresh_token": refreshToken
      ]
    case .fetchProfile:
      return [:]
    case .updateEmail(let email):
      return [
        "email": email
      ]
    case .addBonusCard(let id, let code):
      return [
        "company_id": id,
        "card": code
      ]
    case .removeBonusCard(let id):
      return [
        "company_id": id,
      ]
    case .setPushToken(let token):
      return [
        "pushToken": token
      ]
    }
  }
  
  var task: Task {
    switch self {
    case .updateEmail, .addBonusCard(_, _), .removeBonusCard(_), .setPushToken(_):
      return .requestParameters(parameters: params, encoding: JSONEncoding.default)
    case .fetchProfile:
      return .requestPlain
    default:
      //auth
      return .requestParameters(parameters: params, encoding: JSONEncoding.default)
    }
  }
  
  var headers: [String : String]? {
    return nil
  }
  
  var path: String {
    let auth = "auth"
    var postfix: String
    switch self {
    case .login(_, _):
      postfix = "login"
    case .sendCode(_):
      postfix = "send_code"
    case .refreshTokens(_):
      postfix = "refresh_tokens"
      
    case .fetchProfile:
      return "/profile"
    case .updateEmail(_):
      return "/profile/email"
    case .addBonusCard(_, _), .removeBonusCard(_):
      return "/profile/card"
    case .setPushToken(_):
      return "/profile/setPushToken"
    }
    return "/\(auth)/\(postfix)"
  }
  
  var baseURL: URL {
    return URL(string: Constants.apiPath)!
  }
  
}


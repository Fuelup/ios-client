//
//  GoogleMapsService.swift
//  FuelUp
//
//  Created by Алексей on 05.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Foundation
import Moya
import ObjectMapper
import SwiftyJSON
import CoreLocation

class GoogleMapsService {
  static let instance = GoogleMapsService()

  private let googleMapsProvider: MoyaProvider<GoogleMapsTarget> = FuelUpApiUtils.providerWithPlugins()

  func fetchRoutePoints(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, _ completion: @escaping ServiceCompletion<GoogleRoute>) {
    googleMapsProvider.request(.fetchRoute(origin: origin, destination: destination)) { result in
      switch result {
      case .success(let response):
        let json = JSON(data: response.data)
        let routes = json["routes"].arrayValue
        let points: [String] = routes.map { route in
          let routeOverviewPolyline = route["overview_polyline"].dictionary
          return routeOverviewPolyline?["points"]?.stringValue ?? ""
        }
        let routeData = routes.first?["legs"].arrayValue.first
        let distance = routeData?.dictionaryValue["distance"]?.dictionaryValue["text"]?.string ?? ""
        let duration = routeData?.dictionaryValue["duration"]?.dictionaryValue["text"]?.string ?? ""
        let route = GoogleRoute(routes: points, distance: distance, duration: duration)
        completion(.success(route))
        return
      case .failure(let error):
        completion(.failure(DataError.serverError(error.localizedDescription)))
      }
    }
  }

}

struct LinksHelper {
  static func startNavigation(coordinate: CLLocationCoordinate2D) {
    struct Links {
      static let kGoogleMapsSchema = "comgooglemaps://"
      static let kGoogleMaps = "\(kGoogleMapsSchema)?daddr=%f,%f&directionsmode=driving"
      static let kAppleMaps = "https://maps.apple.com/?saddr=Current Location&daddr=%f,%f&z=10&t=s"
    }

    var path: String!

    if let googleMapsSchemaUrl = URL(string:Links.kGoogleMapsSchema), UIApplication.shared.canOpenURL(googleMapsSchemaUrl) {
      path = Links.kGoogleMaps
    } else {
      path = Links.kAppleMaps
    }
    guard let str = String(format: path, coordinate.latitude, coordinate.longitude).addingPercentEncoding(
      withAllowedCharacters: .urlQueryAllowed) else {
        return
    }

    guard let url = URL(string: str) else {
      return
    }

    UIApplication.shared.open(url, options: [:], completionHandler: nil)
  }
}

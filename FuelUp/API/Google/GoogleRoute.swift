//
//  GoogleRoute.swift
//  FuelUp
//
//  Created by Алексей on 06.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

struct GoogleRoute {
  var routes: [String]
  var distance: String
  var duration: String
}

//
//  GoogleMapsTargetType.swift
//  FuelUp
//
//  Created by Алексей on 05.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

enum GoogleMapsTarget: TargetType {
  var task: Task {
    return .requestParameters(parameters: params, encoding: URLEncoding.default)
  }

  case fetchRoute(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D)

  var method: Moya.Method {
    return .get
  }

  var sampleData: Data {
    return Data()
  }

  var params: [String: Any] {
    switch self {
    case .fetchRoute(let origin, let destination):
      let origin = "\(origin.latitude),\(origin.longitude)"
      let destination = "\(destination.latitude),\(destination.longitude)"
      return [
        "origin": origin,
        "destination": destination,
        "mode": "driving",
        "key": Constants.googleMapsDirectionsKey
      ]
    }
  }

  var headers: [String : String]? {
    return nil
  }


  var path: String {
    switch self {
    case .fetchRoute(_, _):
      return "/directions/json"
    }
  }

  var baseURL: URL {
    return URL(string: "https://maps.googleapis.com/maps/api")!
  }

}

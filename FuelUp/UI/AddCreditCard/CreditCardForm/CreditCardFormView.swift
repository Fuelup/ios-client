//
//  CreditCardFormView.swift
//  FuelUp
//
//  Created by Алексей on 18.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

private let emailRegExp = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

class CreditCardFormView: UIView {
  @IBOutlet weak private var cardNumberField: UITextField!
  let maskedCardNumberInput = MaskedInput(formattingType: .pattern("**** **** **** ****"))
  @IBOutlet weak private var cardDatesField: UITextField!
  let maskedCardDatesInput = MaskedInput(formattingType: .pattern("** / **"))
  @IBOutlet weak private var cardCVCField: UITextField!
  let maskedCardCVCInput = MaskedInput(formattingType: .pattern("***"))

  @IBOutlet weak var cardTypeView: UIImageView!
  @IBOutlet weak var nameField: UITextField!

  var cardNumber: String {
    return cardNumberField.text?.onlyDigits ?? ""
  }

  var cvc: String {
    return cardCVCField.text?.onlyDigits ?? ""
  }

  var dates: String {
    return cardDatesField.text ?? ""
  }

  var mm: String {
    return (dates.components(separatedBy: " / ").first ?? "").onlyDigits
  }

  var yyyy: String {
    return "20\((dates.components(separatedBy: " / ").last ?? "").onlyDigits)"
  }

  var name: String {
    return nameField.text ?? ""
  }

  var fullName: String {
    guard isValidFirstName else { return "" }
    return name.uppercased()
  }

  var textFields: [UITextField] {
    return [cardNumberField, cardDatesField, cardCVCField, nameField]
  }

  var maskedInputs: [MaskedInput] {
    return [maskedCardNumberInput, maskedCardDatesInput, maskedCardCVCInput]
  }
  @IBOutlet weak var cardNumberLineView: UIView!
  @IBOutlet weak var cardDatesLineView: UIView!
  @IBOutlet weak var cardCVCLineView: UIView!
  @IBOutlet weak var nameLineView: UIView!

  var isValidCard: Bool {
    return maskedCardNumberInput.isValid ?? false
  }

  var isValidDates: Bool {
    return maskedCardDatesInput.isValid ?? false
  }

  var isValidCVC: Bool {
    return maskedCardCVCInput.isValid ?? false
  }

  var isValidFirstName: Bool {
    return !(nameField.text ?? "").isEmpty
  }

  var isValidForm: Bool {
    return isValidCard && isValidDates && isValidCVC && isValidFirstName
  }

  var formCompleteHandler: ((Bool) -> Void)? = nil

  func validateAndCompleteFormFill() {
    let defColor = UIColor.whiteThree
    let redColor = UIColor.bloodOrange

    if let char = maskedCardNumberInput.textField?.text?.first, let int = Int("\(char)"), let card = CreditCardType(rawValue: int) {
      self.cardTypeView.isHidden = false
      self.cardTypeView.image = card.image
    } else {
      self.cardTypeView.isHidden = true
    }


    UIView.animate(withDuration: 0.2) { [weak self] in
      guard let `self` = self else { return }
      if !self.isValidCard && self.cardNumberField.text != nil && !self.cardNumberField.text!.isEmpty {
        self.cardNumberLineView.backgroundColor = redColor
      } else {
        self.cardNumberLineView.backgroundColor = defColor
      }

      if !self.isValidDates && self.cardDatesField.text != nil && !self.cardDatesField.text!.isEmpty {
        self.cardDatesLineView.backgroundColor = redColor
      } else {
        self.cardDatesLineView.backgroundColor = defColor
      }

      if !self.isValidCVC && self.cardCVCField.text != nil && !self.cardCVCField.text!.isEmpty {
        self.cardCVCLineView.backgroundColor = redColor
      } else {
        self.cardCVCLineView.backgroundColor = defColor
      }

      if !self.isValidFirstName && !self.name.isEmpty {
        self.nameLineView.backgroundColor = redColor
      } else {
        self.nameLineView.backgroundColor = defColor
      }
    }

    self.formCompleteHandler?(isValidForm)
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    cardTypeView.isHidden = true
    maskedCardNumberInput.validators.append { $0.onlyDigits.luhnCheck }
    maskedCardDatesInput.validators.append { value in
      let components = value.components(separatedBy: " / ")
      guard components.count == 2 else {
        return false
      }
      let currentDateComponents = Calendar.current.dateComponents([.month, .year], from: Date())
      guard let yearComponent = components.last, let year = Int(yearComponent), year >= currentDateComponents.year!%1000 else {
        return false
      }
      guard let monthComponent = components.first, let month = Int(monthComponent), (month > currentDateComponents.month!%1000 || year > currentDateComponents.year!%1000) && month <= 12 else {
        return false
      }
      return true
    }

    for (index, input) in maskedInputs.enumerated() {
      input.configure(textField: textFields[index])
      input.isValidHandler = { [unowned self] isValid in
        self.validateAndCompleteFormFill()
        guard let isValid = isValid else {
          return
        }
        if input == self.maskedCardNumberInput {
          if let char = input.textField?.text?.first, let int = Int("\(char)"), let card = CreditCardType(rawValue: int) {
            self.cardTypeView.isHidden = false
            self.cardTypeView.image = card.image
          } else {
            self.cardTypeView.isHidden = true
          }
        } else {
          self.cardTypeView.isHidden = true
        }
        guard isValid else {
          if index > 0 && (self.textFields[index].text == nil || self.textFields[index].text!.isEmpty) {
            self.textFields[index - 1].becomeFirstResponder()
          }
          return
        }
        self.textFields[index + 1].becomeFirstResponder()
      }
    }

    nameField.delegate = self
  }

  @IBAction func nameEditingChangedAction(_ sender: Any) {
    validateAndCompleteFormFill()
  }
}

extension CreditCardFormView: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    validateAndCompleteFormFill()
    return true
  }
}

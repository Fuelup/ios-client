//
//  AddCreditCardViewController.swift
//  FuelUp
//
//  Created by Алексей on 18.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import EasyPeasy
import SVProgressHUD

private extension URL {
  
  func absoluteStringByTrimmingQuery() -> String? {
    if var urlcomponents = URLComponents(url: self, resolvingAgainstBaseURL:false) {
      urlcomponents.query = nil
      return urlcomponents.string
    }
    return nil
  }
}

class AddCreditCardViewController: UIViewController {
  let titleLabel = UILabel()
  let checkLabel = UILabel()
  let footerButton = FuelUpButtonView(color: .tealish, title: "Добавить карту")
  let creditCardView = CreditCardFormView.nibInstance()!
  
  var cardFrom: CardForm?

  var bottomConstraint: NSLayoutConstraint!
  var observers: [Any] = []

  var dismissHandler: (() -> Void)? = nil

  override func viewDidLoad() {
    title = "Добавить карту"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
    setupView()
  }

  @objc func navigationLeftButtonAction() {
    dismiss(animated: true, completion: nil)
  }

  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    dismissHandler?()
  }

  func setupView() {
    extendedLayoutIncludesOpaqueBars = false
    edgesForExtendedLayout = []
    view.backgroundColor = UIColor.whiteThree
    hideKeyboardWhenTappedAround()
    for label in [titleLabel, checkLabel] {
      label.font = UIFont.systemFont(ofSize: 14, weight: .light)
      label.numberOfLines = 3
      label.textAlignment = .center
    }
    titleLabel.text = "Для проверки будет снят и возвращен 1 руб\nСрок возврата зависит от вашего банка"

    footerButton.cornerRadius = 4
    creditCardView.cornerRadius = 8

    view.addSubview(titleLabel)
    view.addSubview(creditCardView)
    view.addSubview(checkLabel)
    view.addSubview(footerButton)

    titleLabel.easy.layout(
      Top(16).to(topLayoutGuide).with(.medium),
      Top(>=0),
      Left(16),
      Right(16),
      Bottom(16).to(creditCardView, .top).with(.medium),
      Bottom(>=0).to(creditCardView)
    )

    creditCardView.easy.layout(
      Left(16), Right(16), Height(172), Bottom(16).to(checkLabel, .top)
    )

    checkLabel.easy.layout(
      Left(16), Right(16)
    )

    footerButton.easy.layout(
      Top(>=0).to(checkLabel),
      Height(54),
      Left(16), Right(16)
    )

    footerButton.isEnabled = false

    footerButton.disabledHandler = { [unowned self] in
      self.creditCardView.validateAndCompleteFormFill()
    }

    footerButton.handler = { [unowned self] in
      debugPrint("validated success")
      let cardView = self.creditCardView
      guard cardView.isValidForm else { return }
      self.addCard(number: cardView.cardNumber, cvc: cardView.cvc, yyyy: cardView.yyyy, mm: cardView.mm, text: cardView.fullName)
    }

    creditCardView.formCompleteHandler = { isValid in
      self.footerButton.isEnabled = isValid
    }

    bottomConstraint = footerButton.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: -16)
    bottomConstraint.priority = .required
    bottomConstraint.isActive = true
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.observers = bottomConstraint.addObserversUpdateWithKeyboard(view: view)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    observers.forEach { NotificationCenter.default.removeObserver($0) }
    observers = []
  }
  
  func showForm(cardForm: CardForm) {
    let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
    let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    let wkUController = WKUserContentController()
    wkUController.addUserScript(userScript)
    let wkWebConfig = WKWebViewConfiguration()
    wkWebConfig.userContentController = wkUController
    let viewController = UIViewController()
    let webView = WKWebView(frame: .zero, configuration: wkWebConfig)
    webView.navigationDelegate = self
    webView.uiDelegate = self
    viewController.view = webView
    
    var request = URLRequest(url: URL(string: cardForm.url)!)
    if let post = cardForm.post {
      request.httpMethod = "POST"
      request.httpBody = post.data(using: .utf8)
    }
    webView.load(request)
    
    self.pushViewController(viewController)
  }

  func addCard(number: String, cvc: String, yyyy: String, mm: String, text: String) {
    let card = PaymentCard()
    card.pan = number
    card.cvc = cvc
    card.mm = mm
    card.yyyy = yyyy
    card.text = text
    SVProgressHUD.show()
    PaymentsService.instance.addCard(card: card) { [weak self] result in
      switch result {
      case .success(let cardForm):
        if let form = cardForm {
          self?.cardFrom = form
          self?.showForm(cardForm: form)
        } else {
          self?.dismissHandler?()
          self?.dismiss(animated: true, completion: nil)
        }
      case .failure(let error):
        self?.presentErrorAlert(message: error.localizedDescription)
      }
      SVProgressHUD.dismiss()
    }

  }

}

extension AddCreditCardViewController: WKNavigationDelegate {
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    SVProgressHUD.dismiss()
  }
  
}

extension AddCreditCardViewController: WKUIDelegate {
  
  func success() {
    self.dismissHandler?()
    self.dismiss(animated: true, completion: nil)
  }
  
  func fail(webView: WKWebView) {
    self.navigationController?.popToRootViewController(animated: true)
    DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
      self.presentErrorAlert(message: "Проверка не пройдена")
    }
  }
  
  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    if navigationAction.targetFrame == nil {
      webView.load(navigationAction.request)
    }
    let success = cardFrom!.successUrl
    let fail = cardFrom!.failUrl
    if let url = navigationAction.request.url?.absoluteStringByTrimmingQuery() {
      if success == url {
        self.success()
        decisionHandler(.cancel)
        return
      }
      if fail == url {
        self.fail(webView: webView)
        decisionHandler(.cancel)
        return
      }
    }
    decisionHandler(.allow)
  }
}

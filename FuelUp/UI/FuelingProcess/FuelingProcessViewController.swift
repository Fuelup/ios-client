//
//  FuelingProcessViewController.swift
//  FuelUp
//
//  Created by Алексей on 26.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class FuelingProcessViewController: UIViewController {
  let titleLabel = UILabel()
  let descriptionLabel = UILabel()
  let indicatorView = FuelingProcessIndicatorView()
  let headerView = UIView()
  let emergencyView = UIView()
  let checkoutCell = PaymentConfirmCheckoutCell()
  let footerButton = FuelUpButtonView(color: .tealish, title: "Отмена")

  var finishHandler: (() -> Void)? = nil

  override func viewDidLoad() {
    title = "Заправка"
    edgesForExtendedLayout = []
    extendedLayoutIncludesOpaqueBars = false
    navigationItem.leftBarButtonItem = nil
    setupView()
  }

  private var gasStation: GasStation!
  private var order: Order! {
    didSet {
      let fuelType = gasStation.fuelTypes.first(where: { $0.id == order.fuelType })
      let fuelPrice = fuelType?.price ?? 0
      let volume = fuelPrice == 0 ? 0 : order.amountRubles/fuelPrice
      checkoutCell.configure(fuelName: fuelType?.name ?? "", pumpNum: order.pump, volume: volume, amount: order.amountRubles)
    }
  }

  func configure(station: GasStation, order: Order) {
    self.gasStation = station
    self.order = order
    if order.status == .fueling {
      titleLabel.text = "Идет отпуск топлива"
      self.prepareLoaderView()
    }
    OrdersService.instance.currentHandler = { order in
      self.order = order
      if order.status == .fueling {
        self.titleLabel.text = "Идет отпуск топлива"
        self.prepareLoaderView()
      } else if order.status.error {
        self.indicatorView.stopAnimating()
        let alertViewController = FuelUpAlertViewController()
        alertViewController.configure(item: order.status)
        alertViewController.modalPresentationStyle = .overCurrentContext
        alertViewController.addAction(title: "Попробовать еще раз", handler: { [unowned self] in
          self.dismiss(animated: true, completion: nil)
        })
      } else if order.status.success {
        self.indicatorView.stopAnimating()
        if order.status.message != nil {
          let alertViewController = FuelUpAlertViewController()
          alertViewController.configure(item: order.status)
          alertViewController.modalPresentationStyle = .overCurrentContext
          alertViewController.addAction(title: "Ок", handler: { [unowned self] in
            let viewController = FuelingFinishedViewController()
            viewController.configure(station: self.gasStation, order: self.order)
            self.finishHandler?()
            self.navigationController?.setViewControllers([viewController], animated: true)
          })
          self.present(alertViewController)
        } else {
          self.finishHandler?()
          let viewController = FuelingFinishedViewController()
          viewController.configure(station: self.gasStation, order: self.order)
          self.navigationController?.setViewControllers([viewController], animated: true)
        }
      }
    }
  }

  func prepareLoaderView() {
    descriptionLabel.isHidden = true
    descriptionLabel.removeFromSuperview()
    if indicatorView.superview != self.view {
      self.view.addSubview(indicatorView)
    }
    indicatorView.easy.layout(
      Top(16).to(titleLabel),
      Height(20),
      CenterX()
    )
    emergencyView.easy.layout(
      Left(),
      Right(),
      Height(73),
      Top(34).to(indicatorView)
    )
    indicatorView.isHidden = false
    view.layoutIfNeeded()
    indicatorView.startAnimating()

  }

  func prepareDescriptionView() {
    indicatorView.isHidden = true
    indicatorView.removeFromSuperview()
    if descriptionLabel.superview != self.view {
      self.view.addSubview(descriptionLabel)
    }
    descriptionLabel.easy.layout(
      Top(16).to(titleLabel),
      Left(16),
      Right(16)
    )
    emergencyView.easy.layout(
      Left(),
      Right(),
      Height(73),
      Top(34).to(descriptionLabel)
    )
    view.layoutIfNeeded()
    descriptionLabel.isHidden = false
  }

  func setupView() {
    view.backgroundColor = UIColor.whiteThree

    titleLabel.font = UIFont.boldSystemFont(ofSize: 21)
    titleLabel.textColor = UIColor.greyishBrown
    descriptionLabel.font = UIFont.systemFont(ofSize: 15)
    descriptionLabel.numberOfLines = 3

    titleLabel.textAlignment = .center
    descriptionLabel.textAlignment = .center

    titleLabel.text = "Колонка включена"
    descriptionLabel.text = "Вставьте пистолет в бак, или сообщите заправщику."
    view.addSubview(titleLabel)
    view.addSubview(descriptionLabel)

    titleLabel.easy.layout(
      Top(16),
      Left(16),
      Right(16)
    )

    descriptionLabel.easy.layout(
      Top(16).to(titleLabel),
      Left(16),
      Right(16)
    )

    view.addSubview(emergencyView)

    emergencyView.backgroundColor = UIColor.white
    let emergencyLabel = UILabel()
    emergencyLabel.textColor = UIColor.greyishBrown
    emergencyLabel.font = UIFont.systemFont(ofSize: 17)
    emergencyLabel.textAlignment = .left

    let emergencyIndicatorView = UIImageView(image: #imageLiteral(resourceName: "emergencyIndicator"))

    emergencyLabel.text = "Включите аварийку, чтобы подозвать заправщика"
    emergencyLabel.numberOfLines = 2
    emergencyView.addSubview(emergencyLabel)
    emergencyView.addSubview(emergencyIndicatorView)

    emergencyLabel.easy.layout(
      Left(16),
      CenterY()
    )

    emergencyIndicatorView.easy.layout(
      Right(13),
      CenterY(),
      Left().to(emergencyLabel)
    )

    emergencyView.easy.layout(
      Left(),
      Right(),
      Height(73),
      Top(34).to(descriptionLabel)
    )

    view.addSubview(checkoutCell)
    checkoutCell.backgroundColor = .white
    checkoutCell.easy.layout(
      Top(11).to(emergencyView),
      Left(),
      Right(),
      Height(160)
    )

    view.addSubview(footerButton)
    footerButton.cornerRadius = 4
    
    footerButton.easy.layout(
      Bottom(30),
      Left(16),
      Right(16),
      Height(54)
    )

    footerButton.handler = { [unowned self] in
      let alertViewController = FuelUpAlertViewController()
      alertViewController.configure(item: .cancel)

      alertViewController.addAction(title: "Нет", handler: {})
      alertViewController.addAction(title: "Да", color: .fadedRed, handler: { [unowned self] in
        self.footerButton.isEnabled = false
        OrdersService.instance.cancelOrder(orderId: self.order.id, { _ in
          self.dismiss(animated: true, completion: nil)
        })
      })

      alertViewController.modalPresentationStyle = .overCurrentContext
      self.present(alertViewController, animated: false)
    }

    let footerLabel = UILabel()

    view.addSubview(footerLabel)
    footerLabel.text = "Оператор может отключить колонку"
    footerLabel.font = UIFont.systemFont(ofSize: 12)
    footerLabel.textColor = .greyishBrown
    footerLabel.textAlignment = .center

    footerLabel.easy.layout(
      Left(16),
      Right(16),
      Bottom(24).to(footerButton)
    )
  }
}

//
//  FuelingProcessIndicatorView.swift
//  FuelUp
//
//  Created by Алексей on 21.02.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class FuelingProcessIndicatorView: UIView {
  private let stackView = UIStackView()
  private var timer: Timer?

  var isLoading: Bool = false

  var selectedIndex: Int? = nil {
    didSet {
      guard let selectedIndex = selectedIndex else { return }
      for (offset, _) in self.stackView.arrangedSubviews.enumerated() {
        self.stackView.arrangedSubviews[offset].backgroundColor = offset == selectedIndex ? .dustyBlue : .coolGrey
        self.stackView.arrangedSubviews[offset].alpha = offset == selectedIndex ? 1 : 0.6
      }
      self.layoutIfNeeded()
      startAnimating()
    }
  }

  init(count: Int = 3) {
    super.init(frame: CGRect.null)
    stackView.axis = .horizontal
    stackView.alignment = .center
    stackView.distribution = .equalSpacing
    stackView.spacing = 16
    addSubview(stackView)
    stackView.easy.layout(
      Edges(),
      Height(20)
    )
    for _ in 0...count {
      let circleView = UIView()
      stackView.addArrangedSubview(circleView)
      circleView.backgroundColor = .coolBlue
      circleView.easy.layout(Size(10))
      circleView.alpha = 0.6
      circleView.cornerRadius = 5
      stackView.addArrangedSubview(circleView)
    }
  }

  func startAnimating() {
    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.increaseSelection), userInfo: nil, repeats: false)
  }

  func stopAnimating() {
    self.timer?.invalidate()
    self.timer = nil
  }

  @objc private func increaseSelection() {
    guard !isLoading else { return }
    stopAnimating()
    isLoading = true
    guard let index = self.selectedIndex, index < self.stackView.arrangedSubviews.count else {
      selectedIndex = 0
      isLoading = false
      return
    }
    self.selectedIndex = index + 1
    isLoading = false
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

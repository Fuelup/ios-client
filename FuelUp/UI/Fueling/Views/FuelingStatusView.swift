//
//  FuelingStatusView.swift
//  FuelUp
//
//  Created by Алексей on 22.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

enum FuelingStatus {
  case pumpFree
  case inProgress
  case notSelected
  case finished(fuelType: String, volume: Double, price: Double)

  var title: String {
    switch self {
    case .pumpFree:
      return "Колонка свободна"
    case .inProgress:
      return "Идет заправка"
    case .notSelected:
      return "Колонка не выбрана!"
    case .finished(_, _, _):
      return "Заправка завершена"
    }
  }

  var description: String? {
    switch self {
    case .pumpFree:
      return "Вставьте пистолет в бак или сообщите заправщику"
    case .inProgress:
      return "Если это вы, ожидайте завершения заправки"
    case .notSelected:
      return nil
    case .finished(_, _, _):
      return "Подтвердите, что это ваш заказ"
    }
  }
}

class FuelingStatusView: UIView {
  let itemsView = UIStackView()
  let statusTitleLabel = UILabel()
  let statusDescriptionLabel = UILabel()
  let checkoutView = UIView()

  var status: FuelingStatus! {
    didSet {
      statusTitleLabel.text = status.title
      if let description = status.description {
        if statusDescriptionLabel.superview == nil {
          itemsView.insertArrangedSubview(statusDescriptionLabel, at: 1)
        }
        statusDescriptionLabel.text = description
      } else {
        itemsView.removeArrangedSubview(statusDescriptionLabel)
        statusDescriptionLabel.removeFromSuperview()
      }
    }
  }

  init() {
    super.init(frame: CGRect.null)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setup() {
    backgroundColor = .white
    itemsView.axis = .vertical
    itemsView.distribution = .equalSpacing
    itemsView.spacing = 20

    addSubview(itemsView)
    itemsView.easy.layout(
      Left(20),
      Right(20),
      CenterY()
    )

    statusTitleLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    statusTitleLabel.textAlignment = .center
    statusTitleLabel.numberOfLines = 3
    statusDescriptionLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
    statusDescriptionLabel.textAlignment = .center
    statusDescriptionLabel.numberOfLines = 3

    itemsView.addArrangedSubview(statusTitleLabel)
    itemsView.addArrangedSubview(statusDescriptionLabel)
  }
}


class FuelingCheckoutView: UIView {
  let checkoutStackView = UIStackView()
  let lineViews: [UIView] = [UIView(), UIView()]
  let totalView = CheckoutItemView(title: "К оплате", titleTextColor: .black, valueFont: .boldSystemFont(ofSize: 14))

  var paymentItems: [PaymentConfirmCheckoutItem] {
    return [.fuel, .volume]
  }

  func setup() {
    addSubview(checkoutStackView)
    checkoutStackView.axis = .vertical
    checkoutStackView.spacing = 8
    checkoutStackView.alignment = .fill
    for item in paymentItems {
      checkoutStackView.addArrangedSubview(CheckoutItemView(title: item.name))
    }

    checkoutStackView.easy.layout(
      TopMargin(),
      LeftMargin(),
      RightMargin()
    )
  }
}

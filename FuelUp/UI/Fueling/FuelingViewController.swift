//
//  FuelingViewController.swift
//  FuelUp
//
//  Created by Алексей on 22.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class FuelingViewController: UIViewController {
  let pumpSelectLabel = UILabel()
  let titleLabel = UILabel()
  let headerView = UIView()
  let statusView = FuelingStatusView()
  let footerView = UIView()
  let pumpSelectCell = GasStationPumpSelectTableViewCell()
  let footerButton = FuelUpButtonView(color: .tealish, title: "Подтверждаю")

  var status: FuelingStatus {
    get {
      return statusView.status
    }
    set {
      statusView.status = newValue
    }
  }

  private var gasStation: GasStation! {
    didSet {

    }
  }

  private var order: Order! {
    didSet {
      pumpSelectCell.selectedPump = gasStation.pumps.first(where: { $0.num == order.pump })
      pumpSelectCell.pumps = gasStation.pumps
    }
  }

  func configure(station: GasStation, order: Order) {
    self.gasStation = station
    self.order = order
    self.status = .pumpFree
    pumpSelectCell.selectedPump = gasStation.pumps.first(where: { $0.num == order.pump })
    pumpSelectCell.pumps = gasStation.pumps
  }

  override func viewDidLoad() {
    title = "Заправка"
    view.backgroundColor = .whiteThree

    edgesForExtendedLayout = []
    extendedLayoutIncludesOpaqueBars = false

    view.addSubview(statusView)
    view.addSubview(pumpSelectCell)
    view.addSubview(pumpSelectLabel)
    view.addSubview(footerView)

    setupHeader()
    setupFooter()
    setupPumpSelectCell()
    setupStatusView()
  }

  private func setupHeader() {
    headerView.backgroundColor = .white
    titleLabel.font = UIFont.systemFont(ofSize: 20)
    titleLabel.text = "Сначала заправляйся, \nа потом плати"
    view.addSubview(headerView)
    headerView.addSubview(titleLabel)
    titleLabel.textAlignment = .center
    titleLabel.numberOfLines = 3

    titleLabel.easy.layout(
      Top(>=5),
      Left(>=5),
      Right(>=5),
      Bottom(>=5),
      Center()
    )

    headerView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Height(80).with(.low)
    )
  }

  private func setupPumpSelectCell() {
    pumpSelectLabel.text = "Выберите колонку:"
    pumpSelectLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    pumpSelectCell.isUserInteractionEnabled = false

    pumpSelectLabel.easy.layout(
      Left(20),
      Top(16).to(headerView, .bottom).with(.medium),
      Top(>=8).to(headerView, .bottom).with(.high),
      Bottom(8).to(pumpSelectCell, .top)
    )

    pumpSelectCell.layoutIfNeeded()
    pumpSelectCell.easy.layout(
      Left(),
      Right(),
      Height(pumpSelectCell.collectionView.collectionViewLayout.collectionViewContentSize.height),
      Bottom(40).to(statusView).with(.medium)
    )
  }

  private func setupStatusView() {
    statusView.easy.layout(
      Left(),
      Right(),
      Bottom().to(footerView)
    )
  }

  private func setupFooter() {
    footerView.backgroundColor = .whiteThree
    footerButton.handler = { [unowned self] in
      let viewController = FuelingFinishedViewController()
      viewController.configure(station: self.gasStation, order: self.order)
      self.navigationController?.setViewControllers([viewController], animated: true)
    }

    footerView.addSubview(footerButton)
    footerButton.isEnabled = false
    footerButton.cornerRadius = 4
    footerButton.easy.layout(
      Top(16),
      Bottom(16),
      Left(16),
      Right(16),
      Height(54)
    )

    footerView.easy.layout(
      Left(),
      Right(),
      Bottom()
    )
  }

}

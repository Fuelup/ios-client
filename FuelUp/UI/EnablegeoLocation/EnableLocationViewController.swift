//
//  EnableLocationViewController.swift
//  FuelUp
//
//  Created by Алексей on 30.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import EasyPeasy
import UIKit

class EnableLocationViewController: UIViewController, LightContentViewController, TransparentViewController {
  let contentView = UIView()
  let titleLabel = UILabel()
  let descriptionLabel = UILabel()
  let closeButton = UIButton(type: .system)
  let geoLocationImageView = UIImageView(image: #imageLiteral(resourceName: "geolocationEnableIcon"))
  let footerButton = FuelUpButtonView(color: .white, title: "Перейти в настройки", textColor: .tealish, font: UIFont.systemFont(ofSize: 21, weight: .medium))

  override func viewDidLoad() {
    closeButton.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
    closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
    view.backgroundColor = UIColor.clear
    view.isOpaque = false

    contentView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    contentView.isOpaque = false
    
    footerButton.handler = {
      if !CLLocationManager.locationServicesEnabled() {
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
          UIApplication.shared.open(url)
        }
      } else {
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
          UIApplication.shared.open(url)
        }
      }
    }
    footerButton.arrowView.tintColor = .tealish
    setupView()

  }

  func setupView() {
    view.addSubview(contentView)
    contentView.easy.layout(Edges())

    contentView.addSubview(titleLabel)
    contentView.addSubview(closeButton)
    contentView.addSubview(footerButton)
    contentView.addSubview(geoLocationImageView)
    contentView.addSubview(descriptionLabel)

    titleLabel.easy.layout(
      Top(27).to(topLayoutGuide),
      CenterX()
    )

    geoLocationImageView.easy.layout(Center())

    closeButton.easy.layout(
      Top(0),
      Left(0),
      Size(60)
    )

    closeButton.tintColor = .white

    titleLabel.numberOfLines = 2
    titleLabel.textColor = .white
    titleLabel.numberOfLines = 2
    titleLabel.font = UIFont.systemFont(ofSize: 21, weight: .bold)

    titleLabel.text = "Включите службы\nгеолокации"
    titleLabel.textAlignment = .center

    descriptionLabel.font = UIFont.systemFont(ofSize: 15)
    descriptionLabel.textColor = .white
    descriptionLabel.numberOfLines = 2
    descriptionLabel.textAlignment = .center

    descriptionLabel.text = "Определение геолокации необходимо для корректоной работы сервиса"
    descriptionLabel.easy.layout(Left(16), Right(16), Bottom(54).to(footerButton))

    footerButton.easy.layout(
      Bottom(32),
      Left(32),
      Right(32),
      Height(54)
    )

    footerButton.cornerRadius = 4
  }

  @objc func closeButtonTapped() {
    UIView.animate(withDuration: 0.2, animations: { [weak self] in
      self?.contentView.alpha = 0
    }, completion: { [weak self] _ in
      self?.dismiss(animated: false)
    })
  }

  override func viewWillAppear(_ animated: Bool) {
    UIView.animate(withDuration: 0.2) { [weak self] in
      self?.contentView.alpha = 0
      self?.contentView.alpha = 1
    }
  }
}

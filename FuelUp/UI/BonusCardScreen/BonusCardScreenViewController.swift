//
//  BonusCardScreenViewController.swift
//  FuelUp
//
//  Created by Алексей on 15.01.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import WebKit

class BonusCardsScreenViewController: UIViewController {
  let titleLabel = UILabel()
  let cardView = UIView()
  let numberTextField = UITextField()
  let infoLinkButtonView = UIView()
  let footerButton = FuelUpButtonView()
  var bottomConstraint: NSLayoutConstraint!
  var observers: [Any] = []

  var company: Company! {
    didSet {
      let isActive = self.isActive
      titleLabel.text = company.name
      footerButton.title = isActive ? "Удалить" : "Сохранить"
      footerButton.backgroundColor = isActive ? .bloodOrange : .tealish

      numberTextField.placeholder = isActive ? code.uppercased() : String([Character](repeating: "0", count: company.cardLength))
      textFieldDidChange()
    }
  }

  var maxCharsLength: Int {
    guard company != nil else { return 10 }
    return company.cardLength
  }

  var isActive: Bool {
    return ProfileService.instance.profile?.companies[self.company.id] != nil
  }

  var code: String {
    return ProfileService.instance.profile?.companies[self.company.id]?.card ?? self.company.id
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Бонусная карта"
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    setupView()
  }

  func setupView() {
    view.backgroundColor = UIColor.whiteThree
    view.addSubview(titleLabel)
    view.addSubview(cardView)
    cardView.addSubview(numberTextField)

    titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
    titleLabel.easy.layout(
      Top(),
      CenterX(),
      Height(54).with(.medium),
      Bottom(>=5).to(cardView)
    )

    cardView.easy.layout(
      Left(16),
      Top(>=5),
      Right(16),
      Height(170).with(.medium),
      Height(<=170).with(.high)
    )

    cardView.backgroundColor = .white
    cardView.clipsToBounds = false
    cardView.shadow = true
    cardView.cornerRadius = 4

    numberTextField.autocapitalizationType = .none
    numberTextField.autocorrectionType = .no
    numberTextField.keyboardType = .numberPad
    numberTextField.textAlignment = .center
    numberTextField.delegate = self
    cardView.addSubview(numberTextField)
    numberTextField.font = UIFont.systemFont(ofSize: 19)
    numberTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)

    numberTextField.easy.layout(
      Left(),
      Right(),
      Bottom(16)
    )

    view.addSubview(infoLinkButtonView)
    infoLinkButtonView.isUserInteractionEnabled = true
    infoLinkButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(infoButtonAction)))
    infoLinkButtonView.easy.layout(
      Top(4).to(cardView),
      Left(16),
      Right(16)
    )
    let infoLabel = UILabel()
    infoLabel.font = UIFont.systemFont(ofSize: 14)
    infoLabel.text = "Информация о карте"
    infoLabel.adjustsFontSizeToFitWidth = true
    infoLinkButtonView.addSubview(infoLabel)

    let infoImageView = UIImageView(image: #imageLiteral(resourceName: "iconInfoBonuscard"))
    infoImageView.contentMode = .top
    infoLinkButtonView.addSubview(infoImageView)

    infoImageView.easy.layout(
      Left(),
      CenterY().to(infoLabel)
    )

    infoLabel.easy.layout(
      Left(14).to(infoImageView),
      CenterY(),
      Top(>=2).with(.low),
      Bottom(>=2).with(.low)
    )

    let accessoryImageView = UIImageView(image: #imageLiteral(resourceName: "iconArrowRightBlack"))
    infoLinkButtonView.addSubview(accessoryImageView)
    accessoryImageView.easy.layout(
      Right(),
      CenterY().to(infoImageView)
    )

    view.addSubview(footerButton)
    footerButton.cornerRadius = 4
    footerButton.easy.layout(
      Left(16),
      Right(16),
      Height(54),
      Top(>=5).to(infoLinkButtonView)
    )

    footerButton.handler = { [unowned self] in
      self.footerButton.isEnabled = false
      if !self.isActive {
        ProfileService.instance.addBonusCard(card: self.numberTextField.text?.onlyDigits ?? "", companyId: self.company.id, completion: { result in
          self.footerButton.isEnabled = true
          switch result {
          case .success():
            self.presentAlert(title: "", message: "Карта успешно добавлена", handler: { _ in
              self.navigationController?.popViewController(animated: true)
            })
          case .failure(let error):
            self.presentErrorAlert(message: error.localizedDescription)
          }
        })
      } else {
        ProfileService.instance.removeBonusCard(id: self.company.id, completion: { result in
          self.footerButton.isEnabled = true
          switch result {
          case .success():
            self.presentAlert(title: "", message: "Карта удалена", handler: { _ in
              self.navigationController?.popViewController(animated: true)
            })
          case .failure(let error):
            self.presentErrorAlert(message: error.localizedDescription)
          }
        })
      }
    }

    bottomConstraint = footerButton.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: -16)
    bottomConstraint.priority = .required
    bottomConstraint.isActive = true
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.observers = bottomConstraint.addObserversUpdateWithKeyboard(view: view)
    if !isActive {
      numberTextField.becomeFirstResponder()
    } else {
      numberTextField.isEnabled = false
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    view.endEditing(true)
    observers.forEach { NotificationCenter.default.removeObserver($0) }
    observers = []
  }

  @objc func infoButtonAction() {
    guard let url = company.url else { return }
    let view = WKWebView()
    let viewController = UIViewController()
    viewController.view = view
    viewController.title = company.name
    viewController.automaticallyAdjustsScrollViewInsets = false
    viewController.edgesForExtendedLayout = []
    view.load(URLRequest(url: url))
    navigationController?.pushViewController(viewController, animated: true)
  }


  @objc func textFieldDidChange() {
    footerButton.isEnabled = (numberTextField.text ?? "").count == company.cardLength || isActive
  }
}

extension BonusCardsScreenViewController: UITextFieldDelegate {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    guard let text = textField.text else { return true }
    let newLength = text.count + string.count - range.length
    return newLength <= maxCharsLength
  }
}

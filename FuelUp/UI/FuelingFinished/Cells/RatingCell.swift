//
//  RatingCell.swift
//  FuelUp
//
//  Created by Алексей on 27.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import FloatRatingView
import GrowingTextView

class RatingCell: UITableViewCell {
  @IBOutlet weak var ratingView: FloatRatingView!
  @IBOutlet weak var textView: GrowingTextView!
  @IBOutlet weak var button: FuelUpButtonView!
  @IBOutlet var bottomConstraint: NSLayoutConstraint!
  @IBOutlet var topConstraint: NSLayoutConstraint!

  var ratingChangedHandler: (() -> Void)? = nil
  var buttonHandler: (() -> Void)? {
    get {
      return button.handler
    }
    set {
      button.handler = newValue
    }
  }

  var isExpanded: Bool = false

  func configure() {
    bottomConstraint.isActive = isExpanded
    topConstraint.isActive = !isExpanded
    if isExpanded {
      textView.becomeFirstResponder()
    }
    updateConstraints()
    layoutIfNeeded()
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    configure()
    NotificationCenter.default.addObserver(self, selector: #selector(textDidChange), name: .UITextViewTextDidChange, object: textView)
    ratingView.delegate = self
  }

  @objc func textDidChange() {
    button.title = textView.text.isEmpty ? "Далее" : "Оставить отзыв"
  }
}

extension RatingCell: FloatRatingViewDelegate {
  func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
    ratingChangedHandler?()
  }


}

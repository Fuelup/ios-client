//
//  PaymentCell.swift
//  FuelUp
//
//  Created by Алексей on 27.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class PaymentCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var dateTimeLabel: UILabel!
  @IBOutlet weak var paymentView: UIView!
  @IBOutlet weak var idLabel: UILabel!
  @IBOutlet weak var invoiceButton: UIButton!
  
  var invoiceButtonHandler: (() -> Void)? = nil
  
  let checkoutStackView = UIStackView()
  let lineView = UIView()
  let totalView = CheckoutItemView(title: "К оплате", titleTextColor: .black, valueFont: .boldSystemFont(ofSize: 14))

  override func awakeFromNib() {
    super.awakeFromNib()
    setupPaymentView()
  }

  func setupPaymentView() {
    selectionStyle = .none
    paymentView.addSubview(checkoutStackView)
    checkoutStackView.axis = .vertical
    checkoutStackView.spacing = 8
    checkoutStackView.distribution = .fillEqually
    for item in PaymentConfirmCheckoutItem.allValues {
      checkoutStackView.addArrangedSubview(CheckoutItemView(title: item.name))
    }

    checkoutStackView.easy.layout(
      Top(20),
      Left(20),
      Right(20)
    )

    lineView.backgroundColor = .whiteThree
    paymentView.addSubview(lineView)
    lineView.easy.layout(
      Top(20).to(checkoutStackView),
      Left(20),
      Right(20),
      Height(1)
    )

    paymentView.addSubview(totalView)

    totalView.easy.layout(
      Top(20).to(lineView, .top),
      Left(20),
      Right(20),
      Bottom(20)
    )
  }

  func configure(station: GasStation, order: Order) {
    guard let fuelType = station.fuelTypes.first(where: { $0.id == order.fuelType }) else { return }
    let fuelPrice = fuelType.price
    let volume = fuelPrice == 0 ? 0 : Double(order.cost)/fuelPrice

    idLabel.text = order.id
    nameLabel.text = station.name
    addressLabel.text = station.address
    dateTimeLabel.text = "\(order.dtFinish.shortDate), \(order.dtFinish.time)"
    let numberFormatter = NumberFormatter()
    numberFormatter.minimumFractionDigits = 0
    numberFormatter.maximumFractionDigits = 2
    let values = ["\(order.pump)", fuelType.name, numberFormatter.string(from: NSNumber(value: volume)) ?? ""]
    values.enumerated().forEach { (checkoutStackView.arrangedSubviews[$0] as! CheckoutItemView).valueText = $1 }
    totalView.valueText = order.amountRubles.currencyString
  }

  @IBAction func invoiceButtonAction(_ sender: Any) {
    invoiceButtonHandler?()
  }


}

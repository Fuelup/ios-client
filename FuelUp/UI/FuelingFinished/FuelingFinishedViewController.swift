//
//  FuelingFinishedViewController.swift
//  FuelUp
//
//  Created by Алексей on 27.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import WebKit

class FuelingFinishedViewController: UIViewController {
  let paymentCell = PaymentCell.nibInstance()!
  let ratingCell = RatingCell.nibInstance()!
  let footerButton = FuelUpButtonView(color: .white, title: "Готово", textColor: .tealish)
  let tableView = UITableView()
  var bottomConstraint: NSLayoutConstraint! {
    didSet {
      if oldValue != nil {
        oldValue.isActive = false
      }
      bottomConstraint.isActive = true
    }
  }

  var cells: [UITableViewCell] {
    return [paymentCell, ratingCell]
  }

  private var station: GasStation!
  private var order: Order!

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Заправка завершена"
    view.backgroundColor = UIColor.whiteThree
    view.addSubview(footerButton)

    hideKeyboardWhenTappedAround()

    footerButton.cornerRadius = 4
    footerButton.easy.layout(
      Bottom(30),
      Left(16),
      Right(16),
      Height(54)
    )

    footerButton.handler = { [unowned self] in
      self.dismiss(animated: true, completion: nil)
    }

    paymentCell.invoiceButtonHandler = { [unowned self] in
      guard let url = URL(string: self.order.checkUrl) else { return }
      let view = WKWebView()
      let viewController = UIViewController()
      viewController.view = view
      viewController.title = "Чек"
      viewController.automaticallyAdjustsScrollViewInsets = false
      viewController.edgesForExtendedLayout = []
      view.load(URLRequest(url: url))
      self.navigationController?.pushViewController(viewController, animated: true)

    }

    ratingCell.buttonHandler = { [unowned self] in
      let review = Review()
      review.id = self.order.id
      review.comment = self.ratingCell.textView.text
      review.rate = Int(self.ratingCell.ratingView.rating)
      self.ratingCell.button.isEnabled = false
      GasStationsServce.instance.createReview(review: review, { result in
        self.dismiss(animated: true, completion: nil)
      })
    }

    view.addSubview(tableView)
    tableView.separatorStyle = .none
    tableView.backgroundColor = UIColor.whiteThree

    tableView.easy.layout(
      Top().to(self.topLayoutGuide),
      Left(),
      Right()
    )

    self.bottomConstraint = self.tableView.bottomAnchor.constraint(equalTo: self.footerButton.topAnchor)

    ratingCell.ratingChangedHandler = { [unowned self] in
      guard !self.ratingCell.isExpanded else { return }
      UIView.animate(withDuration: 0.2, animations: { [unowned self] in
        self.bottomConstraint = self.tableView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor)
        self.footerButton.isHidden = true
        self.view.layoutIfNeeded()
        }, completion: { [unowned self] _ in
          self.ratingCell.isExpanded = true
          self.tableView.beginUpdates()
          self.tableView.endUpdates()
      })
    }

    tableView.delegate = self
    tableView.dataSource = self
    tableView.keyboardDismissMode = .onDrag
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(FuelingFinishedViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(FuelingFinishedViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
  }

  @objc func keyboardWillShow(notification: NSNotification) {
    if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
      tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
      tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .bottom, animated: true)
    }
  }

  @objc func keyboardWillHide(notification: NSNotification) {
    UIView.animate(withDuration: 0.2, animations: {
      self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    })
  }

  func configure(station: GasStation, order: Order) {
    self.station = station
    self.order = order
//    let fuelType = station.fuelTypes.first(where: { $0.id == order.fuelType })
//    let fuelPrice = fuelType?.priceRubles ?? 0
//    let volume = fuelPrice == 0 ? 0 : order.costRubles/fuelPrice
    self.paymentCell.configure(station: station, order: order)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    NotificationCenter.default.removeObserver(self)
  }

}

extension FuelingFinishedViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == cells.index(of: ratingCell)! {
      ratingCell.configure()
    }
    return cells[indexPath.section]
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    return cells.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
    case cells.index(of: paymentCell)!:
      return UITableViewAutomaticDimension
    case cells.index(of: ratingCell)!:
      return ratingCell.isExpanded ? 320 : 110
    default:
      return 0
    }
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
    case cells.index(of: paymentCell)!:
      return 254
    case cells.index(of: ratingCell)!:
      return ratingCell.isExpanded ? 320 : 110
    default:
      return 0
    }
  }

}

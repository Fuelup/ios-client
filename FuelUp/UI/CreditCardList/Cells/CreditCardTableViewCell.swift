//
//  CreditCardTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 18.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class CreditCardTableViewCell: UITableViewCell {
  private let mainView = CreditCardView()
  
  var menuHandler: (() -> Void)? {
    get {
      return mainView.menuHandler
    }
    set {
      mainView.menuHandler = newValue
    }
  }

  var data: CreditCardViewData {
    get {
      return mainView.data
    }
    set {
      mainView.data = newValue
    }
  }

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  

  func setup() {
    selectionStyle = .none
    backgroundColor = UIColor.white
    addSubview(mainView)
    mainView.backgroundColor = .white
    mainView.cornerRadius = 4
    mainView.shadow = true
    mainView.easy.layout(
      Top(4),
      Bottom(4),
      Left(16),
      Right(16)
    )
  }


  @objc func menuAction() {
    menuHandler?()
  }
}

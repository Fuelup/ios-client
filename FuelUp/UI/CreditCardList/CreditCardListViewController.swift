//
//  CreditCardListViewController.swift
//  FuelUp
//
//  Created by Алексей on 18.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class CreditCardListViewController: UITableViewController {
  let titleLabel = UILabel()
  var payments: [PaymentHistoryData] = PaymentsService.instance.payments

  var openedAddCard: Bool = false

  var selectedCardId: String? = nil {
    didSet {
      tableView.reloadData()
      guard let selectedCard = payments.first(where: { $0.identifier == selectedCardId }) else { return }
      selectHandler?(selectedCard)
    }
  }
  
  var selectHandler: ((PaymentHistoryData) -> Void)? = nil

  func refresh() {
    PaymentsService.instance.refreshCardList { result in
      switch result {
      case .success(let payments):
        self.payments = payments
        self.tableView.reloadData()
      case .failure(let error):
        self.presentErrorAlert(message: error.localizedDescription)
      }
    }
  }

  override func viewDidLoad() {
    title = "Оплата"
    super.viewDidLoad()
    tableView.separatorStyle = .none
    tableView.tableFooterView = UIView()

    selectedCardId = StorageHelper.loadObjectForKey(.selectedCardId)
    edgesForExtendedLayout = []
    extendedLayoutIncludesOpaqueBars = false
    CreditCardTableViewCell.register(in: tableView)
    titleLabel.font = UIFont.boldSystemFont(ofSize: 21)
    titleLabel.textAlignment = .center
    titleLabel.textColor = .greyishBrown
    titleLabel.numberOfLines = 4
    titleLabel.text = "Выберите карту для оплаты заправки"
    let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 100))
    headerView.addSubview(titleLabel)
    titleLabel.easy.layout(Edges(16))
    tableView.tableHeaderView = headerView

    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
    refresh()
  }

  @objc func navigationLeftButtonAction() {
    dismiss(animated: true, completion: nil)
  }

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return payments.count + 1
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cardData: CreditCardViewData
    if indexPath.row == payments.count {
      cardData = .addCard
    } else {
      switch payments[indexPath.row].type {
      case .card(let card):
        cardData = .card(card: card, number: indexPath.row + 1)
      case .taxi(let taxi):
        cardData = .taxi(taxi: taxi, number: indexPath.row + 1)
      }
    }
    let cell = CreditCardTableViewCell.instance(tableView, indexPath)!
    cell.menuHandler = { [unowned self] in
      self.menuAction(indexPath: indexPath)
    }
    cell.data = cardData
    return cell
  }

  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 84
  }

  func menuAction(indexPath: IndexPath) {
    let card = payments[indexPath.row]
    if card.cardData != nil {
      let alertViewController = UIAlertController(title: "", message: "Карта №\(indexPath.row + 1)", preferredStyle: .alert)
      alertViewController.hideKeyboardWhenTappedAround()
      if !card.cardData!.isActive {
        alertViewController.addAction(UIAlertAction(title: "Сделать активной", style: .default, handler: { [weak self] _ in
          card.cardData!.isActive = true
          self?.tableView.reloadData()
        }))
      }
      alertViewController.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { [weak self] _ in
        
        let alert = UIAlertController(title: "Карта №\(indexPath.row + 1)", message: "Вы действительно хотите удалить данную карту?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { _ in
          PaymentsService.instance.deleteCard(card: card, { result in
            switch result {
            case .success(_):
              self?.payments.remove(at: indexPath.row)
              self?.tableView.deleteRows(at: [indexPath], with: .automatic)
              if let payments = self?.payments, payments.isEmpty {
                let viewController = AddCreditCardViewController()
                self?.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
              }
            case .failure(let error):
              self?.presentErrorAlert(message: error.localizedDescription)
            }
          })
        }))
        self?.present(alert, animated: true)
      }))
      
      alertViewController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
      
      present(alertViewController, animated: true)
    }
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch indexPath.row {
    case payments.count:
      let viewController = AddCreditCardViewController()
      present(FuelUpNavigationController(rootViewController: viewController), animated: true)
    default:
      if selectHandler == nil {
        switch payments[indexPath.row].type {
        case .card(_):
          menuAction(indexPath: indexPath)
        default:
          break
        }
      } else {
        selectedCardId = payments[indexPath.row].identifier
      }
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    refresh()
  }

}

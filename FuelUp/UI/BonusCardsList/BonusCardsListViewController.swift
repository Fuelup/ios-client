//
//  BonusCardsListViewController.swift
//  FuelUp
//
//  Created by Алексей on 11.01.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class BonusCardsListViewController: UITableViewController {
  var companies: [Company] = [] {
    didSet {
      tableView.reloadData()
    }
  }

  var myCompanies: [Company] {
    return companies.filter { ProfileService.instance.profile?.companies[$0.id] != nil }
  }

  var otherCompanies: [Company] {
    return companies.filter { ProfileService.instance.profile?.companies[$0.id] == nil }
  }

  let emptyCell = EmptyPaymentCell()

  func refresh() {
    self.companies = GasStationsServce.instance.companies
    ProfileService.instance.fetchProfile(completion: { _ in
      self.companies = GasStationsServce.instance.companies
      GasStationsServce.instance.fetchCompanies { result in
        switch result {
        case .success(let companies):
          self.companies = companies
        case .failure(_):
          break
        }
      }
    })
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    refresh()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Бонусы"
    tableView.backgroundColor = .white
    BonusCardsListTableViewCell.register(in: tableView)
    tableView.separatorStyle = .none
    tableView.backgroundColor = .white
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))

    emptyCell.descriptionLabel.text = "Бонусные карты отсутствуют"
    emptyCell.titleLabel.text = ""
  }

  @objc func navigationLeftButtonAction() {
    dismiss(animated: true, completion: nil)
  }

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard companies.count != 0 else { return section == 0 ? 1 : 0 }
    return section == 0 ? myCompanies.count : otherCompanies.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if companies.isEmpty {
      return emptyCell
    }
    let cell = BonusCardsListTableViewCell.instance(tableView, indexPath)!
    let company: Company = indexPath.section == 0 ? myCompanies[indexPath.row] : otherCompanies[indexPath.row]

//    let booking = companies[indexPath.row]
    cell.configure(company: companies[indexPath.row], isActive: indexPath.section == 0, code: ProfileService.instance.profile?.companies[company.id]?.card)
    return cell
  }

  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return companies.isEmpty ? 130 : 102
  }

  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return companies.isEmpty ? 0 : 54
  }

  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView()
    if companies.count == 0 {
      return view
    }
    view.backgroundColor = .white
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
    label.text = section == 0 ? "Прикрепленные" : "Доступные"
    view.addSubview(label)
    label.easy.layout(
      Left(16),
      Bottom()
    )
    return view
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let viewController = BonusCardsScreenViewController()
    viewController.company = indexPath.section == 0 ? myCompanies[indexPath.row] : otherCompanies[indexPath.row]
    self.navigationController?.pushViewController(viewController, animated: true)
  }
}

//
//  BonusCardListTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 11.01.2018.
//  Copyright © 2018 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class BonusCardsListTableViewCell: UITableViewCell {
  let mainView = UIView()
  let logoView = UIView()
  let logoLabel = UILabel()
  let titleLabel = UILabel()
  let labelsView = UIStackView()
  let descriptionLabel = UILabel()
  let accessoryImageView = UIImageView(image: #imageLiteral(resourceName: "iconArrowRightBlack"))

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func configure(company: Company, isActive: Bool, code: String?) {
    titleLabel.text = company.name
    logoLabel.text = String(company.name.first!)
    descriptionLabel.text = isActive ? code ?? company.id.uppercased() : ""
    accessoryImageView.image = isActive ? #imageLiteral(resourceName: "iconArrowRightBlack") :  #imageLiteral(resourceName: "iconAddBonuscard")
    if isActive {
      guard descriptionLabel.superview == nil else { return }
      labelsView.addArrangedSubview(descriptionLabel)
    } else {
      labelsView.removeArrangedSubview(descriptionLabel)
      descriptionLabel.removeFromSuperview()
    }
  }

  

  func setup() {
    selectionStyle = .none
    addSubview(mainView)
    mainView.easy.layout(
      Left(16),
      Right(16),
      Top(8),
      Bottom(8)
    )
    mainView.clipsToBounds = false
    mainView.shadow = true
    mainView.backgroundColor = .white
    mainView.addSubview(logoView)
    logoView.backgroundColor = .tealish
    logoView.cornerRadius = 4
    logoView.easy.layout(
      Size(60),
      Left(16),
      CenterY(),
      Top(13),
      Bottom(13)
    )

    logoView.addSubview(logoLabel)
    logoLabel.textColor = .white
    logoLabel.font = UIFont.boldSystemFont(ofSize: 30)
    logoLabel.easy.layout(
      Center()
    )

    labelsView.axis = .vertical
    labelsView.spacing = 6

    titleLabel.font = UIFont(name: "SFProText-Semibold", size: 17.0) ?? UIFont.systemFont(ofSize: 17, weight: .semibold)
    descriptionLabel.font = UIFont(name: "SFProText-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14)
    descriptionLabel.textColor = .coolGrey
    mainView.addSubview(labelsView)
    labelsView.addArrangedSubview(titleLabel)
    mainView.addSubview(accessoryImageView)
    accessoryImageView.easy.layout(
      CenterY(),
      Left(>=16).with(.medium),
      Size(<=22),
      Right(16).with(.high)
    )
    accessoryImageView.setContentHuggingPriority(.required, for: .horizontal)
    labelsView.easy.layout(
      CenterY(),
      Left(20).to(logoView),
      Right(10).to(accessoryImageView)
    )
  }
}

//
//  CardSelectAlertTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 26.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class CardSelectAlertTableViewCell: UITableViewCell {
  private let mainView = CreditCardView()

  var menuHandler: (() -> Void)? {
    get {
      return mainView.menuHandler
    }
    set {
      mainView.menuHandler = newValue
    }
  }

  let checkView = UIImageView(image: #imageLiteral(resourceName: "iconSelectCard"))

  var isSelectedCard: Bool {
    get {
      return !checkView.isHidden
    }
    set {
      checkView.isHidden = !newValue
    }
  }

  var data: CreditCardViewData {
    get {
      return mainView.data
    }
    set {
      mainView.data = newValue
      switch newValue {
      case .addCard:
        mainView.backgroundColor = .white
      case .card(_, _):
        mainView.backgroundColor = .white
        mainView.easy.layout(
          Left(0),
          Right(0),
          Top(0),
          Bottom(0)
        )
      case .taxi(_, _):
        mainView.easy.layout(
          Left(0),
          Right(16 + 23),
          Top(0),
          Bottom(0)
        )
      }
    }
  }

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }


  func setup() {
    selectionStyle = .none
    addSubview(mainView)
    mainView.menuButton.isHidden = true

    addSubview(checkView)
    checkView.easy.layout(
      Right(16),
      Width(23),
      CenterY(0)
    )
    mainView.easy.layout(
      Left(0),
      Right(0),
      Top(0),
      Bottom(0)
    )

    checkView.isHidden = true
    mainView.menuButton.isHidden = true
    mainView.menuButton.alpha = 0
  }


  @objc func menuAction() {
    menuHandler?()
  }
}

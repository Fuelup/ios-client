//
//  CardSelectAlertViewController.swift
//  FuelUp
//
//  Created by Алексей on 22.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class CardSelectAlertViewController: UIViewController {
  let tableView = UITableView()
  let contentView = UIView()
  var payments: [PaymentHistoryData] = [] {
    didSet {
      calculateHeight()
    }
  }

  func calculateHeight() {
    var height: CGFloat = CGFloat((payments.count + 1) * 84 + 68)
    if height > view.frame.height {
      tableView.isScrollEnabled = true
      height = view.frame.height
    } else {
      tableView.isScrollEnabled = false
    }
    tableView.easy.layout(
      Height(height)
    )
    view.layoutIfNeeded()
  }

  var tableViewColor: UIColor {
    return UIColor(white: 227.0 / 255.0, alpha: 1.0)
  }

  var selectHandler: ((PaymentHistoryData) -> Void)? = nil

  override func viewDidLoad() {
    super.viewDidLoad()
    payments = PaymentsService.instance.payments
    setupView()
  }

  var selectedCardId: String? = nil {
    didSet {
      tableView.reloadData()
      guard let selectedCard = payments.first(where: { $0.identifier == selectedCardId }) else { return }
      selectHandler?(selectedCard)
    }
  }

  func refresh() {
    PaymentsService.instance.refreshCardList { result in
      switch result {
      case .success(let payments):
        self.payments = payments
        self.tableView.reloadData()
      case .failure(let error):
        self.presentErrorAlert(message: error.localizedDescription)
      }
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    UIView.animate(withDuration: 0.2) { [weak self] in
      self?.contentView.alpha = 0
      self?.contentView.alpha = 1
    }
    refresh()
  }

  func setupView() {
    view.backgroundColor = UIColor.clear
    view.isOpaque = false

    contentView.backgroundColor = UIColor.black.withAlphaComponent(0.56)
    contentView.isOpaque = false

    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backgroundTapAction))
    tap.cancelsTouchesInView = false
    tap.delegate = self
    contentView.addGestureRecognizer(tap)

    view.addSubview(contentView)
    contentView.easy.layout(Edges())

    tableView.isOpaque = false
    tableView.backgroundColor = .clear
    tableView.clipsToBounds = true
    tableView.cornerRadius = 4
    tableView.separatorStyle = .none

    tableView.delegate = self
    tableView.dataSource = self

    if selectedCardId == nil {
      selectedCardId = StorageHelper.loadObjectForKey(.selectedCardId)
    }

    CardSelectAlertTableViewCell.register(in: tableView)

    let bckView = UIView()
    contentView.addSubview(bckView)
    bckView.backgroundColor = tableViewColor
    bckView.easy.layout(
      CenterY(),
      Left(16),
      Right(16)
    )
    bckView.cornerRadius = 6
    bckView.clipsToBounds = true
    tableView.clipsToBounds = true
    bckView.addSubview(tableView)
    tableView.easy.layout(Edges())

    calculateHeight()
  }

  @objc func backgroundTapAction() {
    UIView.animate(withDuration: 0.2, animations: { [weak self] in
      self?.contentView.alpha = 1
      self?.contentView.alpha = 0
    }, completion: { [weak self] _ in
        self?.dismiss(animated: false, completion: nil)
    })
  }
}

extension CardSelectAlertViewController: UITableViewDelegate, UITableViewDataSource {

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cardData: CreditCardViewData
    let cell = CardSelectAlertTableViewCell.instance(tableView, indexPath)!
    if indexPath.section == payments.count {
      cardData = .addCard
    } else {
      switch payments[indexPath.section].type {
      case .card(let card):
        cardData = .card(card: card, number: indexPath.section + 1)
      case .taxi(let taxi):
        cardData = .taxi(taxi: taxi, number: indexPath.section + 1)
      }
      cell.isSelectedCard = payments[indexPath.section].identifier == selectedCardId
    }
    cell.data = cardData
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch indexPath.section {
    case payments.count:
      let viewController = AddCreditCardViewController()
      viewController.dismissHandler = { [weak self] in
        self?.dismiss(animated: false, completion: nil)
      }
      present(FuelUpNavigationController(rootViewController: viewController), animated: true)
    default:
      view.isUserInteractionEnabled = false
      let card = payments[indexPath.section]
      self.selectHandler?(card)
      self.dismiss(animated: true, completion: nil)
    }
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    return payments.count + 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 76
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    guard section == 0 else { return 0 }
    return 68
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView()
    view.backgroundColor = tableViewColor
    return view
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard section == 0 else { return nil }
    let view = UIView()
    view.backgroundColor = tableViewColor
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
    label.text = "Выберите карту"
    view.addSubview(label)
    label.easy.layout(Center())
    return view
  }

  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 8
  }
}

extension CardSelectAlertViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
    return touch.view == gestureRecognizer.view
  }
}

//
//  EmptyPaymentCell.swift
//  FuelUp
//
//  Created by Алексей on 27.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class EmptyPaymentCell: UITableViewCell {
  let mainView = UIView()
  let titleLabel = UILabel()
  let descriptionLabel = UILabel()

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  init() {
    super.init(style: .default, reuseIdentifier: nil)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func setup() {
    backgroundColor = .clear
    contentView.backgroundColor = .clear

    addSubview(mainView)
    mainView.shadow = true
    mainView.backgroundColor = .white

    titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
    descriptionLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
    descriptionLabel.numberOfLines = 3
    titleLabel.textAlignment = .center
    descriptionLabel.textAlignment = .center

    titleLabel.text = "Вы пока не совершали покупок"
    descriptionLabel.text = "Заправьтесь с помощью FuelUP\nи просмотрите историю здесь!"

    mainView.easy.layout(
      Top(13),
      Left(8),
      Right(8),
      Bottom(13)
    )

    mainView.addSubview(titleLabel)
    mainView.addSubview(descriptionLabel)

    titleLabel.easy.layout(
      Top(14),
      Left(),
      Right()
    )

    descriptionLabel.easy.layout(
      Top(20).to(titleLabel),
      Left(),
      Right()
    )
  }
}

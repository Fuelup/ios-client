//
//  PaymentsHistoryViewController.swift
//  FuelUp
//
//  Created by Алексей on 27.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class PaymentsHistoryViewController: UITableViewController {
  let emptyPaymentCell = EmptyPaymentCell()
  
  override func viewDidLoad() {
    PaymentCell.registerNib(in: tableView)
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
    tableView.backgroundColor = .whiteThree
    tableView.separatorStyle = .none
    title = "История платежей"
    refresh()
  }
  
  var orders: [Order] = [] {
    didSet {
      self.tableView.reloadData()
    }
  }
  
  func refresh() {
    self.orders = GasStationsServce.instance.orders
    GasStationsServce.instance.fetchOrders { result in
      switch result {
      case .success(let orders):
        self.orders = orders
      case .failure(_):
        break
      }
    }
  }
  
  @objc func showInvoice(index: UIButton) {
    let url = URL(string: orders[index.tag].checkUrl)
    let safari: SFSafariViewController = SFSafariViewController(url: url!)
    self.present(safari, animated: true, completion: nil)
  }
  
  @objc func navigationLeftButtonAction() {
    dismiss(animated: true, completion: nil)
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return orders.isEmpty ? 1 : orders.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard !orders.isEmpty else { return emptyPaymentCell }
    let cell = PaymentCell.instance(tableView, indexPath)!
    let order = orders[indexPath.row]
    cell.invoiceButton.tag = indexPath.row
    cell.invoiceButton.addTarget(self, action: #selector(showInvoice(index:)), for: .touchUpInside)
    cell.configure(station: order.gasStation!, order: order)
    return cell
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return orders.isEmpty ? 120 : UITableViewAutomaticDimension
  }
  
  override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return orders.isEmpty ? 120 : 254
  }
}

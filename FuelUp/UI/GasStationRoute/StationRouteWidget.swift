//
//  StationRouteWidget.swift
//  FuelUp
//
//  Created by Алексей on 06.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class StationRouteWidget: UIView {
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var stationNameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var button: FuelUpButtonView!

  var buttonHandler: (() -> Void)? {
    get {
      return button.handler
    }
    set {
      button.handler = newValue
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    button.titleLabel.text = "Поехали"
    button.tintColor = .white
  }

  func configure(station: GasStation, route: GoogleRoute) {
    stationNameLabel.text = station.name
    addressLabel.text = station.address
    timeLabel.text = route.duration
    distanceLabel.text = route.distance
  }
}

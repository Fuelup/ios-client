//
//  GasStationRouteViewController.swift
//  FuelUp
//
//  Created by Алексей on 05.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import CoreLocation
import GoogleMaps

class GasStationRouteViewController: UIViewController {
  let mapView = GMSMapView()
  var locationManager = CLLocationManager()
  let plusButton = UIButton()
  let minusButton = UIButton()
  let locationButton = UIButton()
  let stationWidget = StationRouteWidget.nibInstance()!

  let stationMarker = GMSMarker()

  private var currentLocation: CLLocationCoordinate2D!
  private var station: GasStation! {
    didSet {
      stationMarker.icon = #imageLiteral(resourceName: "iconStation")
      stationMarker.position = station.coordinates!
      stationMarker.map = mapView
      stationWidget.buttonHandler = { [weak self] in
        self?.startNavigation()
      }
    }
  }

  func configureButtons() {
    plusButton.setImage(#imageLiteral(resourceName: "plusButton"), for: .normal)
    minusButton.setImage(#imageLiteral(resourceName: "minusButton"), for: .normal)
    locationButton.setImage(#imageLiteral(resourceName: "iconMyLocationButton"), for: .normal)

    plusButton.addTarget(self, action: #selector(zoomPlusAction), for: .touchUpInside)
    minusButton.addTarget(self, action: #selector(zoomMinusAction), for: .touchUpInside)
    locationButton.addTarget(self, action: #selector(locationAction), for: .touchUpInside)
  }

  var points: [String] = [] {
    didSet {
      for point in points
      {
        let path = GMSPath.init(fromEncodedPath: point)
        let polyline = GMSPolyline.init(path: path)
        polyline.strokeColor = .tealish
        polyline.strokeWidth = 4
        polyline.map = self.mapView
      }
      let path = GMSMutablePath()
      path.add(currentLocation)
      path.add(station.coordinates!)
      let mapBounds = GMSCoordinateBounds(path: path)
      let cameraUpdate = GMSCameraUpdate.fit(mapBounds)
      mapView.moveCamera(cameraUpdate)
    }
  }

  func configure(location: CLLocationCoordinate2D, station: GasStation) {
    self.currentLocation = location
    self.station = station
    let path = GMSMutablePath()
    path.add(currentLocation)
    path.add(station.coordinates!)
    let mapBounds = GMSCoordinateBounds(path: path)
    let cameraUpdate = GMSCameraUpdate.fit(mapBounds)
    mapView.moveCamera(cameraUpdate)

    addRoutes()
  }

  func addRoutes() {
    guard let coordinates = station.coordinates else { return }
    GoogleMapsService.instance.fetchRoutePoints(origin: currentLocation, destination: coordinates) { [weak self] result in
      guard let `self` = self else { return}
      switch result {
      case .success(let route):
        self.points = route.routes
        self.stationWidget.configure(station: self.station, route: route)
      default:
        break
      }
    }
  }

  func addConstraints() {
    view.addSubview(mapView)
    mapView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Bottom().to(bottomLayoutGuide)
    )


    let zoomButtonsView = UIView()
    view.addSubview(zoomButtonsView)

    zoomButtonsView.backgroundColor = .clear

    zoomButtonsView.addSubview(plusButton)
    zoomButtonsView.addSubview(minusButton)
    zoomButtonsView.addSubview(locationButton)

    plusButton.easy.layout([
      Top(),
      Left(),
      Right()
    ])

    minusButton.easy.layout([
      Top(24).to(plusButton, .bottom),
      CenterX().to(plusButton)
    ])

    locationButton.easy.layout([
      Top(24).to(minusButton, .bottom),
      Bottom(),
      CenterX().to(plusButton)
    ])

    zoomButtonsView.easy.layout([
      CenterY().with(.medium),
      Right(20)
    ])

    stationWidget.layoutIfNeeded()
    view.addSubview(stationWidget)

    stationWidget.easy.layout(
      Bottom().to(bottomLayoutGuide),
      Left(),
      Right()
    )
  }

  override func viewDidLoad() {
    locationManager.delegate = self
    mapView.isMyLocationEnabled = true
    locationManager.startUpdatingLocation()
    navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "fuelUpLogo"))

    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(navigationCloseAction))
    addConstraints()
  }

  @objc func navigationCloseAction() {
    dismiss(animated: true, completion: nil)
  }

  @objc func zoomPlusAction() {
    mapView.animate(toZoom: mapView.camera.zoom + 1)
  }

  @objc func zoomMinusAction() {
    mapView.animate(toZoom: mapView.camera.zoom - 1)
  }

  @objc func locationAction() {
    mapView.isMyLocationEnabled = true
    locationManager.startUpdatingLocation()
  }
  
  
  func appleClick() {
    let string = "https://maps.apple.com/?saddr=\(currentLocation.latitude),\(currentLocation.longitude)&daddr=\(station.coordinates!.latitude),\(station.coordinates!.longitude)&z=10&t=s"
    if let url = URL(string: string), UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }

  }
  
  func yandexClick() {
    let string = "yandexnavi://build_route_on_map?lat_to=\(station.coordinates!.latitude)&lon_to=\(station.coordinates!.longitude)"
    guard let url = URL(string: string), UIApplication.shared.canOpenURL(url)
    else {
      let url = URL(string: "https://itunes.apple.com/ru/app/yandeks.navigator/id474500851?mt=8")!
      UIApplication.shared.open(url)
      return
    }
    UIApplication.shared.open(url)
  }
  
  func googleClick() {
    let string = "comgooglemaps://?daddr=\(station.coordinates!.latitude),\(station.coordinates!.longitude)&directionsmode=driving"
    guard let url = URL(string: string), UIApplication.shared.canOpenURL(url) else {
      UIApplication.shared.open(URL(string:
        "https://maps.google.com/?q=@\(Float(station.coordinates!.latitude)),\(Float(station.coordinates!.longitude))")!)
      return
    }
    UIApplication.shared.open(url)
  }
  
  func startNavigation() {
    let alertVC = UIAlertController(title: "Выберете приложение для навигации", message: nil, preferredStyle: .actionSheet)
    
    let appleAction = UIAlertAction(title: "Apple Maps ", style: .default) { (action) in
      self.appleClick()
    }
    alertVC.addAction(appleAction)
    
    let yandexAction = UIAlertAction(title: "Yandex.Navigator", style: .default) { (action) in
      self.yandexClick()
    }
    alertVC.addAction(yandexAction)
    
    let googleAction = UIAlertAction(title: "Google Maps", style: .default) { (action) in
      self.googleClick()
    }
    alertVC.addAction(googleAction)
    
    let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
    alertVC.addAction(cancelAction)
    
    self.present(alertVC, animated: true, completion: nil)
  }
  

}

extension GasStationRouteViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let coordinate = locations.first?.coordinate else { return }
    self.currentLocation = coordinate
    let path = GMSMutablePath()
    path.add(currentLocation)
    path.add(station.coordinates!)
    let mapBounds = GMSCoordinateBounds(path: path)
    let cameraUpdate = GMSCameraUpdate.fit(mapBounds)
    mapView.moveCamera(cameraUpdate)
    locationManager.stopUpdatingLocation()
  }
}

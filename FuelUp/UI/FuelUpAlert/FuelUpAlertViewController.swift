//
//  FuelUpAlertViewController.swift
//  FuelUp
//
//  Created by Алексей on 28.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

enum FuelUpAlertItem {
  case cancel
  case cancelSuccess
  case unknown
  case distance
  case pumpUnavailable
  case custom(title: String, description: String)

  var title: String {
    switch self {
    case .cancel:
      return "Отмена"
    case .cancelSuccess:
      return "Колонка отключена"
    case .unknown:
      return "Ошибка"
    case .distance:
      return "Операция отклонена"
    case .pumpUnavailable:
      return "Колонка недоступна"
    case .custom(let title, _):
      return title
    }
  }

  var description: String {
    switch self {
    case .cancel:
      return "Вы уверены, что хотите отменить операцию?"
    case .cancelSuccess:
      return "Колонка была отключена оператором. Вам было залито 4 литра топлива на сумму 150 рублей."
    case .unknown:
      return "Произошла ошибка при создании заказа"
    case .pumpUnavailable:
      return "Смените колонку"
    case .distance:
      return "Ваше местонахождение не в радиусе АЗС. Если это ошибка, попробуйте включить и выключить геолокацию"
    case .custom(_, let description):
      return description
    }
  }
}

class FuelUpAlertViewController: UIViewController {
  private let contentView = UIView()
  let alertView = UIView()
  private let titleLabel = UILabel()
  private let descriptionLabel = UILabel()
  private let buttonsView = UIStackView()

  override func viewWillAppear(_ animated: Bool) {
    UIView.animate(withDuration: 0.2) { [weak self] in
      self?.contentView.alpha = 0
      self?.contentView.alpha = 1
    }
  }

  func configure(item: OrderStatus) {
    titleLabel.text = item.message
    descriptionLabel.text = item.description
  }

  func configure(item: FuelUpAlertItem) {
    titleLabel.text = item.title
    descriptionLabel.text = item.description
  }


  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }

  func setupView() {
    view.backgroundColor = UIColor.clear
    view.isOpaque = false

    contentView.backgroundColor = UIColor.black.withAlphaComponent(0.56)
    contentView.isOpaque = false

    view.addSubview(contentView)
    contentView.easy.layout(Edges())

    alertView.isOpaque = false
    alertView.backgroundColor = .white
    alertView.clipsToBounds = true
    alertView.cornerRadius = 4

    contentView.addSubview(alertView)

    alertView.easy.layout(
      CenterY(),
      Height(>=320),
      Left(16),
      Right(16)
    )

    titleLabel.font = UIFont.boldSystemFont(ofSize: 21)
    titleLabel.textAlignment = .center
    titleLabel.textColor = .greyishBrown
    titleLabel.numberOfLines = 0
    descriptionLabel.textAlignment = .center
    descriptionLabel.numberOfLines = 0

    alertView.addSubview(titleLabel)

    titleLabel.easy.layout(
      Top(19),
      Left(8),
      Right(8)
    )

    alertView.addSubview(descriptionLabel)

    descriptionLabel.easy.layout(
      Top(30).to(titleLabel),
      Left(8),
      Right(8)
    )

    alertView.addSubview(buttonsView)
    buttonsView.spacing = 10
    buttonsView.distribution = .fillEqually
    buttonsView.axis = .horizontal

    buttonsView.easy.layout(
      Top(>=0).to(descriptionLabel),
      Left(8),
      Right(8),
      Height(54),
      Bottom(8)
    )
  }

  func addAction(title: String, color: UIColor = .tealish, dismisses: Bool = true, handler: @escaping (() -> Void)) {
    let button = FuelUpButtonView(color: color, title: title)
    button.cornerRadius = 4
    button.handler = {
      guard dismisses else {
        self.dismiss(animated: false, completion: handler)
        return
      }
      UIView.animate(withDuration: 0.2, animations: { [weak self] in
        self?.contentView.alpha = 1
        self?.contentView.alpha = 0
        }, completion: { [weak self] _ in
          self?.dismiss(animated: false, completion: handler)
      })
    }
    buttonsView.addArrangedSubview(button)
  }

  @objc func backgroundTapAction() {
  }

}

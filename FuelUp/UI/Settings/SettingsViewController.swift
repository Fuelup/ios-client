//
//  SettingsViewController.swift
//  FuelUp
//
//  Created by Богдан Быстрицкий on 30/12/2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit
import ObjectMapper
import EasyPeasy

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var tableView: UITableView!
  
  var isEditState = false {
    didSet {
      navigationItem.rightBarButtonItem?.isEnabled = isEditState
    }
  }

  var emailField: UITextField!
  var gasStationField: UITextField!

  var isEditingEmail = false {
    didSet {
      isEditState = true
    }
  }

  var tempEmail = String()
  var isEditingIsOn = false {
    didSet {
      isEditState = true
    }
  }

  var isEditingGasTank: Bool = true
  var tempGasTank = String()
  
  var settingsValueLabels = [String]()
  var editSettingsValueLabels = [String]()
  
  var settings: Settings! {
    didSet {
      let numberFormatter = NumberFormatter()
      numberFormatter.minimumFractionDigits = 0
      numberFormatter.maximumFractionDigits = 2
      tempGasTank = numberFormatter.string(from: NSNumber(value: settings.gasTankVolume)) ?? ""
    }
  }

  var profile: Profile? {
    didSet {
      tableView.reloadData()
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Настройки"
    
    settings = Settings.current ?? Settings()
    getDataAboutUser()
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
    tableView.delegate = self
    tableView.dataSource = self
    tableView.reloadData()
    let icon = UIImage(named: "tick")
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(saveChanges))
    navigationItem.rightBarButtonItem?.isEnabled = false

  }
  
  @IBAction func signOutButtonDidTapped(_ sender: Any) {
    let alert : UIAlertController = UIAlertController()
    let exitAction = UIAlertAction(title: "Выйти из профиля", style: .destructive, handler: { [unowned self] action in
      self.dismiss(animated: true, completion: {
        ProfileService.instance.logout()
      })
    })
    let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
    
    alert.addAction(exitAction)
    alert.addAction(cancelAction)
    
    self.present(alert, animated: true, completion: nil)
  }
  
  @objc func navigationLeftButtonAction() {
    if emailField.isFirstResponder {
      emailField.endEditing(true)
      return
    }
    if gasStationField.isFirstResponder {
      gasStationField.endEditing(true)
      return
    }
    if isEditState {
      let alert = UIAlertController(title: "Отменить внесенные изменения", message: nil, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Отменить", style: .destructive, handler: { _ in
        self.tableView.reloadData()
      }))

      alert.addAction(UIAlertAction(title: "Нет", style: .cancel, handler: { _ in
        self.dismiss(animated: true, completion: nil)
      }))

      alert.modalPresentationStyle = .overCurrentContext
      self.present(alert, animated: false)
    } else {
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  @objc func toEditSettings() {
    isEditState = true
    title = "Редактирование"
    navigationItem.rightBarButtonItem?.isEnabled = true
    tableView.reloadData()
  }
  
  @objc func saveChanges() {
    if isEditingEmail {
      isEditingEmail = false
      view.endEditing(true)
      return
    }
    if isEditingGasTank {
      isEditingGasTank = false
      view.endEditing(true)
      return
    }
    if tempEmail != profile?.email {
      if isValidEmail(tempEmail: tempEmail) {
        updateEmail(newEmail: tempEmail)
        settingsValueLabels[1] = tempEmail
        editSettingsValueLabels[0] = tempEmail
        isEditingEmail = false
      } else {
        toShowAlert(text: "Неправильный формат почты")
      }
    }
    
    self.settings?.gasTankVolume = Double(tempGasTank) ?? 0.0
    Settings.current = self.settings
    settingsValueLabels[2] = tempGasTank
    editSettingsValueLabels[1] = tempGasTank

    self.view.endEditing(true)
    self.presentAlert(title: nil, message: "Параметры сохранены", handler: { _
      in
      self.view.endEditing(true)
      self.isEditState = false
      self.navigationItem.rightBarButtonItem?.isEnabled = false
    })
  }

  func isValidEmail(tempEmail: String) -> Bool {
    let emailTest = NSPredicate(format:"SELF MATCHES %@", Constants.emailRegexp)
    return emailTest.evaluate(with: tempEmail)
  }
  
  func isValidVolumeGasTank(tempGasTank: String) -> Bool {
    return true
  }
  
  func toShowAlert(text: String) {
    let alert : UIAlertController = UIAlertController(title: "Ошибка", message: text, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
    alert.addAction(okAction)
    self.present(alert, animated: true, completion: nil)
  }
  
  func getDataAboutUser() {
    self.profile = ProfileService.instance.profile

    ProfileService.instance.fetchProfile { result in
      switch result {
      case .failure(let error):
        print("Error \(error)")
      case .success(let profile):
        self.profile = profile
        self.tableView.reloadData()
      }
    }
    
    let numberFormatter = NumberFormatter()
    numberFormatter.minimumFractionDigits = 0
    numberFormatter.maximumFractionDigits = 2
    let gasTank = numberFormatter.string(from: NSNumber(value: settings.gasTankVolume)) ?? ""
    
    settingsValueLabels.append(profile?.phoneNumber ?? "")
    settingsValueLabels.append(profile?.email ?? "")
    settingsValueLabels.append(gasTank)
    editSettingsValueLabels.append(ProfileService.instance.profile?.email ?? "")
    editSettingsValueLabels.append(gasTank)
  }
  
  func updateEmail(newEmail: String) {
    ProfileService.instance.updateEmail(newEmail: newEmail) { result in
      switch result {
      case .success(_):
        self.getDataAboutUser()
      case .failure(_):
        print("Error")
      }
    }
  }
  
  @objc func textEmailFieldDidChange(textField: UITextField) {
    isEditingEmail = true
    isEditingGasTank = false
    tempEmail = textField.text!
  }
  
  @objc func textFieldGasTankDidChange(textField: UITextField) {
    isEditState = true
    isEditingEmail = false
    isEditingGasTank = true
    tempGasTank = textField.text!
  }

   @objc func setUpdated() {
    isEditState = true
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 4
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch (indexPath.row) {
    case 0:
      let cell = FuelUpInputTableViewCell("Номер телефона")
      if let profile = profile {
        cell.inputField.placeholder = "+7"
        cell.inputField.text = "+\(profile.phoneNumber)"
        cell.inputField.isEnabled = false
      } else {
        cell.inputField.text = ""
      }
      return cell
    case 1:
      let cell = FuelUpInputTableViewCell("E-mail")
      cell.inputField.text = settingsValueLabels[indexPath.row]
      emailField = cell.inputField
      cell.inputField.keyboardType = .emailAddress
      cell.inputField.addTarget(self, action: #selector(self.textEmailFieldDidChange), for: .editingChanged)
      cell.inputField.addTarget(self, action: #selector(self.setUpdated), for: .editingChanged)
      return cell
    case 2:
      let cell = FuelUpInputTableViewCell("Примерный объем бака")
      cell.inputField.text = settingsValueLabels[indexPath.row]
      cell.formatterLabel.text = "л"
      gasStationField = cell.inputField
      cell.inputField.keyboardType = .numberPad
      cell.inputField.addTarget(self, action: #selector(self.textFieldGasTankDidChange), for: .editingChanged)
      cell.inputField.addTarget(self, action: #selector(self.setUpdated), for: .editingChanged)
      return cell
    case 3:
      let cell = FuelUpSwitchTableViewCell("Всегда полный бак")
      cell.isOn = (settings?.alwaysFull) ?? true
      cell.handler = { [unowned self] isOn in
        self.view.endEditing(true)
        self.isEditingEmail = false
        self.isEditingGasTank = false
        self.isEditingIsOn = true
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.settings?.alwaysFull = isOn
      }
      return cell
    default:
      let cell = UITableViewCell()
      return cell
    }
  }
}


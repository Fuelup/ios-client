  //
//  GasStationParamsFuelTypeSelectCell.swift
//  FuelUp
//
//  Created by Алексей on 13.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import PickerView

private let rowHeight: CGFloat = 50

private enum PickerParamData: Hashable, Equatable {
  case fuelType(FuelType)
  case none

  var hashValue: Int {
    switch self {
    case .fuelType(let fuelType):
      return fuelType.id
    default:
      return -1
    }
  }

  static func ==(lhs: PickerParamData, rhs: PickerParamData) -> Bool {
    return lhs.hashValue == rhs.hashValue
  }
}

class GasStationParamsFuelTypeSelectCell: UITableViewCell {
  let pickerView = PickerView()
  let selectTriangleIconView = UIImageView(image: #imageLiteral(resourceName: "rightSelectTriangle"))

  var fuelTypes: [FuelType] = [] {
    didSet {
      fuelTypeCellVisiblee = true
      selectedFuelType = nil
      pickerView.currentSelectedRow = 0
      pickerView.reloadPickerView()
      pickerView.selectRow(0, animated: true)
    }
  }

  private var fuelTypeCellVisiblee: Bool = true {
    didSet {
      guard fuelTypeCellVisiblee != oldValue else { return }
      pickerView.reloadPickerView()
      if fuelTypeCellVisiblee {
        pickerView.selectRow(0, animated: true)
      } else {
        pickerView.reloadPickerView()
      }
    }
  }

  var selectedFuelType: FuelType? = nil {
    didSet {
      if selectedFuelType != nil && selectedFuelType?.id != oldValue?.id {
        fuelTypeCellVisiblee = false
        pickerView.reloadPickerView()
      }
      handler?(selectedFuelType)
    }
  }

  var handler: ((FuelType?) -> Void)? = nil

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func setup() {
    selectionStyle = .none
    pickerView.delegate = self
    pickerView.dataSource = self
    pickerView.selectionStyle = .none
    addSubview(pickerView)
    pickerView.translatesAutoresizingMaskIntoConstraints = false
    pickerView.easy.layout(
      Top(),
      Left(),
      Right(),
      Bottom()
    )

    addSubview(selectTriangleIconView)

    selectTriangleIconView.easy.layout(
      Left(6),
      CenterY()
    )
  }
}

extension GasStationParamsFuelTypeSelectCell: PickerViewDelegate, PickerViewDataSource {

  func pickerViewNumberOfRows(_ pickerView: PickerView) -> Int {
    return fuelTypes.count + 1
  }

  func pickerViewHeightForRows(_ pickerView: PickerView) -> CGFloat {
    return 64
  }

  func pickerView(_ pickerView: PickerView, titleForRow row: Int, index: Int) -> String {
    return fuelTypes[row].name
  }

  func pickerView(_ pickerView: PickerView, viewForRow row: Int, index: Int, highlighted: Bool, reusingView view: UIView?) -> UIView? {
    let gasView = view as? GasStationFuelTypeOptionView ?? GasStationFuelTypeOptionView(width: frame.width)
    gasView.isSelected = highlighted
    if index == 0 {
      if fuelTypeCellVisiblee {
        gasView.fuelType = nil
        gasView.isHidden = false
      } else {
        gasView.isHidden = true
      }
      gasView.isSelected = highlighted
    } else {
      gasView.isHidden = false
      let fuelType = fuelTypes[index - 1]
      gasView.fuelType = fuelType
      gasView.isSelected = highlighted && selectedFuelType?.id == fuelType.id
    }
    return gasView
  }

  func pickerView(_ pickerView: PickerView, didSelectRow row: Int, index: Int) {
    guard row != 0 else {
      if !fuelTypeCellVisiblee {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
          pickerView.currentSelectedRow = 1
          pickerView.selectRow(1, animated: true)
          pickerView.reloadPickerView()
        })
      }
      pickerView.reloadPickerView()
      return
    }
    selectedFuelType = fuelTypes[index - 1]
    pickerView.reloadPickerView()
  }

  func pickerView(_ pickerView: PickerView, didTapRow row: Int, index: Int) {
    guard !(index == 0) else {
      pickerView.reloadPickerView()
      return
    }
    selectedFuelType = fuelTypes[index - 1]
  }

}

//
//  CheckoutTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 14.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

enum CheckoutTypeItem: Int {
  case rubles = 0
  case litres
  case fullTank

  var title: String {
    switch self {
    case .rubles:
      return "Рубли"
    case .litres:
      return "Литры"
    case .fullTank:
      return "Полный Бак"
    }
  }

  var editingSymbol: String {
    switch self {
    case .rubles:
      return "₽"
    default:
      return "л."
    }
  }

  var orderType: OrderType {
    switch self {
    case .rubles:
      return .money
    default:
      return .liter
    }
  }

  static let allItems = [rubles, litres, fullTank]
}

class GasStationCheckoutCell: UITableViewCell {
  var headerItemViews: [GoBarHeaderItemView] = []
  let headerView = UIStackView()
  let inputField = UITextField()
  let inputFieldLabel = UILabel()
  let warningLabel = UILabel()

  var isValidHandler: ((Bool) -> Void)? = nil

  var currentItem: CheckoutTypeItem = .rubles {
    didSet {
      headerItemViews.enumerated().forEach { index, view in
        view.isSelected = currentItem.rawValue == index
      }

      inputField.isEnabled = currentItem != .fullTank
      inputField.keyboardType = .decimalPad
      inputFieldLabel.text = currentItem.editingSymbol

      let numberFormatter = NumberFormatter()
      numberFormatter.minimumFractionDigits = 0
      numberFormatter.maximumFractionDigits = 2
      switch currentItem {
      case .litres where oldValue == CheckoutTypeItem.rubles:
        guard let value = value, value != 0, let price = pricePerLiter, price != 0 else { break }
        self.value = value / price
        self.inputField.text = numberFormatter.string(from: NSNumber(value: value/price))
      case .rubles where oldValue == .litres || oldValue == .fullTank:
        guard let value = value, value != 0, let price = pricePerLiter, price != 0 else { break }
        self.value = value * price
        self.inputField.text = numberFormatter.string(from: NSNumber(value: value*price))
      case .fullTank where gasTankVolume != nil:
        self.value = gasTankVolume
        self.inputField.text = numberFormatter.string(from: NSNumber(value: gasTankVolume!))
      default:
        break
      }
      validate()
      handler?(currentItem, value)
    }
  }

  func reset() {
    if currentItem != .fullTank {
      value = 0
      self.inputField.text = nil
      validate()
    }
  }

  override var isSelected: Bool {
    didSet {
      guard isSelected else { return }
      inputField.becomeFirstResponder()
    }
  }

  var gasTankVolume: Double? = nil {
    didSet {
      if let gasTankVolume = gasTankVolume, gasTankVolume != 0 && currentItem == .fullTank {
        let numberFormatter = NumberFormatter()
        value = gasTankVolume
        self.inputField.text = numberFormatter.string(from: NSNumber(value: gasTankVolume))
      }
    }
  }

  var pricePerLiter: Double? = nil

  func validate() {
    guard let value = self.value else {
      inputField.textColor = UIColor.greyishBrown
      inputFieldLabel.textColor = UIColor.greyishBrown
      inputField.placeholderColor = UIColor.greyishBrown
      warningLabel.isHidden = true

      return
    }
    switch currentItem {
    case .litres where value < 4, .fullTank where value < 4, .rubles where value < 150:
      warningLabel.isHidden = false
      inputField.textColor = UIColor.tomatoTwo
      inputFieldLabel.textColor = UIColor.tomatoTwo
      inputField.placeholderColor = UIColor.tomatoTwo
      warningLabel.text = "Минимальная сумма заправки 150 руб. или 4 литра"
      isValidHandler?(false)
    case .litres where value > 250, .fullTank where value > 250, .rubles where value > 9999:
      warningLabel.isHidden = false
      inputField.textColor = UIColor.tomatoTwo
      inputFieldLabel.textColor = UIColor.tomatoTwo
      inputField.placeholderColor = UIColor.tomatoTwo
      warningLabel.text = "Максимальная сумма заправки 9999 руб. или 250 литров"
      isValidHandler?(false)
    default:
      inputField.textColor = UIColor.greyishBrown
      inputFieldLabel.textColor = UIColor.greyishBrown
      inputField.placeholderColor = UIColor.greyishBrown
      warningLabel.isHidden = true
      isValidHandler?(true)
    }

  }

  var handler: ((CheckoutTypeItem, Double?) -> Void)? = nil
  var fullTankHandler: (() -> Void)? = nil


  var value: Double? {
    get {
      let numberFormatter = NumberFormatter()
      numberFormatter.minimumFractionDigits = 0
      numberFormatter.maximumFractionDigits = 2
      guard let text = inputField.text, let value = numberFormatter.number(from: text)?.doubleValue else { return nil }
      return value
    }
    set {
      let numberFormatter = NumberFormatter()
      numberFormatter.minimumFractionDigits = 0
      numberFormatter.maximumFractionDigits = 2
      inputField.text = numberFormatter.string(from: NSNumber(value: value ?? 0))
      validate()
    }
  }

  var rubles: Double {
    guard let value = value else { return 0 }
    switch currentItem {
    case .litres:
      return (pricePerLiter ?? 0) * value
    case .rubles:
      return value
    case .fullTank:
      return (pricePerLiter ?? 0) * (gasTankVolume ?? value)
    }
  }

  @objc func inputFieldAction() {
    validate()
    handler?(currentItem, value)
    layoutIfNeeded()

  }

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func setup() {
    selectionStyle = .none
    addSubview(headerView)
    contentView.isUserInteractionEnabled = false
    headerView.axis = .horizontal
    headerView.distribution = .fillEqually
    headerView.spacing = 1
    inputField.keyboardType = .decimalPad

    CheckoutTypeItem.allItems.forEach { item in
      let view = GoBarHeaderItemView(title: item.title)
      view.handler = { [weak self] in
        self?.currentItem = item
      }
      headerView.addArrangedSubview(view)
      headerItemViews.append(view)
    }

    headerItemViews[CheckoutTypeItem.rubles.rawValue].isSelected = true

    headerView.easy.layout(
      Top(),
      Left(20),
      Right(20),
      Height(44)
    )

    let inputView = UIView()
    addSubview(inputView)
    inputView.easy.layout(
      CenterX(),
      Top(20).to(headerView)
    )

    inputView.addSubview(inputField)
    inputView.addSubview(inputFieldLabel)

    inputField.placeholder = "0"
    inputField.placeholderColor = UIColor.greyishBrown
    inputField.textColor = UIColor.greyishBrown
    inputField.textAlignment = .center
    inputField.delegate = self
    inputField.font = UIFont.boldSystemFont(ofSize: 36)

    inputFieldLabel.textAlignment = .center
    inputFieldLabel.text = currentItem.editingSymbol
    inputFieldLabel.textColor = UIColor.greyishBrown
    inputFieldLabel.font = UIFont.boldSystemFont(ofSize: 36)

    inputField.easy.layout(
      Left(),
      Top(),
      Bottom()
    )

    inputFieldLabel.easy.layout(
      CenterY().to(inputField),
      Left(4).to(inputField),
      Top(),
      Bottom(),
      Right()
    )

    addSubview(warningLabel)

    warningLabel.easy.layout(
      Bottom(8),
      Left(),
      Right()
    )

    warningLabel.font = UIFont.systemFont(ofSize: 11)
    warningLabel.textColor = .tomato
    warningLabel.adjustsFontSizeToFitWidth = true
    warningLabel.textAlignment = .center
    warningLabel.text = "Минимальная сумма заправки 150 руб. или 4 литра"

    inputField.addTarget(self, action: #selector(inputFieldAction), for: .editingChanged)

  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    guard selected else {
      return
    }
    guard currentItem != .fullTank else {
      fullTankHandler?()
      return
    }
    inputField.becomeFirstResponder()
  }
}

extension GasStationCheckoutCell: UITextFieldDelegate {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

    let currentCharacterCount = textField.text?.count ?? 0
    if (range.length + range.location > currentCharacterCount){
      return false
    }
    let newLength = currentCharacterCount + string.count - range.length
    return newLength <= 10
  }
}

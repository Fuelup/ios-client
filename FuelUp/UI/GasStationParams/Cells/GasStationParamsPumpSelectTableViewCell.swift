//
//  GasStationParamsPumpSelectTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 13.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

struct GasStationPumpCollectionViewConstants {
  static let itemSize = 54
  static let verticInset = 8
  static let horizInset = 20

  static let collectionReuseIdentifier = "PumpCollectionViewCell"
}

class GasStationPumpSelectTableViewCell: UITableViewCell {
  let collectionView = UICollectionView(frame: CGRect.null, collectionViewLayout: UICollectionViewFlowLayout())
  
  var pumps: [Pump] = [] {
    didSet {
      collectionView.reloadData()
      (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).invalidateLayout()
      (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).prepare()
    }
  }

  var selectedPumpHandler: ((Pump) -> Void)? = nil

  var selectedPump: Pump? = nil {
    didSet {
      guard let selectedPump = selectedPump else { return }
      selectedPumpHandler?(selectedPump)
    }
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup() {
    selectionStyle = .none
    collectionView.isScrollEnabled = false
    collectionView.dataSource = self
    collectionView.delegate = self

    if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      layout.sectionInset = UIEdgeInsetsMake(8, 20, 8, 20)
      layout.minimumInteritemSpacing = 16
      layout.minimumLineSpacing = 16
    }
    addSubview(collectionView)
    collectionView.easy.layout(Edges())
    collectionView.backgroundColor = UIColor.white

    collectionView.register(PumpCollectionViewCell.self, forCellWithReuseIdentifier: GasStationPumpCollectionViewConstants.collectionReuseIdentifier)
  }
}

extension GasStationPumpSelectTableViewCell: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return pumps.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GasStationPumpCollectionViewConstants.collectionReuseIdentifier, for: indexPath) as! PumpCollectionViewCell
    let pump = pumps[indexPath.row]
    cell.isEnabled = !pump.isBlock
    cell.isSelected = pump.num == (selectedPump?.num ?? 0)
    cell.number = pump.num
    return cell
  }

  func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
    return !pumps[indexPath.row].isBlock
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    selectedPump = pumps[indexPath.row]
  }
}

extension GasStationPumpSelectTableViewCell: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 54, height: 54)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 16
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 16
  }
}

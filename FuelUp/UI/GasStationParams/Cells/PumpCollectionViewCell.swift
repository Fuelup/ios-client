//
//  PumpCollectionViewCell.swift
//  FuelUp
//
//  Created by Алексей on 13.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class PumpCollectionViewCell: UICollectionViewCell {
  private let circleView = UIView()
  private let numberLabel = UILabel()
  
  override var isSelected: Bool {
    didSet {
      updateBackgroundColor()
    }
  }

  var isEnabled: Bool = true {
    didSet {
      updateBackgroundColor()
    }
  }

  private func updateBackgroundColor() {
    guard isEnabled else {
      circleView.backgroundColor = .warmGreyTwo
      return
    }
    circleView.backgroundColor = isSelected ? .greenyBlue : .silver
  }
  
  var number: Int? = nil {
    didSet {
      numberLabel.text = "\(number ?? 1)"
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup() {
    addSubview(circleView)
    circleView.easy.layout(Edges())
    circleView.addSubview(numberLabel)
    numberLabel.textColor = .white
    numberLabel.font = UIFont.boldSystemFont(ofSize: 20)
    numberLabel.easy.layout(Center())
    
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    circleView.cornerRadius = circleView.frame.height / 2
  }
}

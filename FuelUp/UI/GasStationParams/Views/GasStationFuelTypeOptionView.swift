//
//  GasStationFuelTypeOptionView.swift
//  FuelUp
//
//  Created by Алексей on 13.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class GasStationFuelTypeOptionView: UIView {
  let circleView = UIView()
  let fuelTypeShortLabel = UILabel()
  let fuelTypeLabel = UILabel()
  let priceLabel = UILabel()

  var isSelected: Bool = false {
    didSet {
      guard oldValue != isSelected else { return }
      UIView.animate(withDuration: 0.2) { [unowned self] in
        self.fuelTypeShortLabel.textColor = self.isSelected ? UIColor.white : .black
        self.fuelTypeShortLabel.font = self.isSelected ? UIFont.systemFont(ofSize: 17, weight: .medium) : UIFont.systemFont(ofSize: 13)
        self.fuelTypeLabel.font = self.isSelected ? UIFont.systemFont(ofSize: 16, weight: .medium) : UIFont.systemFont(ofSize: 13)
        self.circleView.borderWidth = self.isSelected ? 0 : 0.8
        self.circleView.backgroundColor = self.isSelected ? .greenyBlue : .white
      }

    }
  }

  var fuelType: FuelType? = nil {
    didSet {
      fuelTypeLabel.text = fuelType?.name ?? "Выберите топливо"
      fuelTypeShortLabel.text = fuelType?.code ?? "?"
      priceLabel.text = fuelType?.price.currencyString ?? ""
    }
  }

  init(width: CGFloat) {
    super.init(frame: CGRect(x: 0, y: 0, width: width, height: 64))
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setup() {
    addSubview(circleView)
    circleView.addSubview(fuelTypeShortLabel)
    addSubview(fuelTypeLabel)
    addSubview(priceLabel)

    circleView.easy.layout(
      Left(22),
      Right(8).to(fuelTypeLabel, .left),
      Top(5),
      Bottom(5),
      Size(54)
    )

    circleView.addSubview(fuelTypeShortLabel)
    circleView.cornerRadius = 54/2
    circleView.borderColor = UIColor.pinkishGrey
    circleView.borderWidth = 0.8

    fuelTypeShortLabel.easy.layout(
      Center(),
      Left(>=1),
      Right(<=1)
    )

    fuelTypeShortLabel.textAlignment = .center
    fuelTypeShortLabel.adjustsFontSizeToFitWidth = true
    fuelTypeShortLabel.font = UIFont.systemFont(ofSize: 13)
    fuelTypeShortLabel.text = "?"

    priceLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
    priceLabel.textAlignment = .right
    priceLabel.textColor = .coolBlue

    priceLabel.easy.layout(
      Right(24).with(.high),
      CenterY()
    )

    priceLabel.setContentHuggingPriority(.required, for: .horizontal)
    fuelTypeLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)

    fuelTypeLabel.font = UIFont.systemFont(ofSize: 13)
    fuelTypeLabel.textColor = .blackTwo
    fuelTypeLabel.text = "Выберите топливо"

    fuelTypeLabel.easy.layout(
      Right().to(priceLabel),
      CenterY()
    )

    fuelTypeLabel.numberOfLines = 2
  }


}

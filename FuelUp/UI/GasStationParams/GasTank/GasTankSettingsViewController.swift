//
//  GasTankSettingsViewController.swift
//  FuelUp
//
//  Created by Алексей on 14.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

private let descriptionText = "Чтобы воспользоваться данной услугой, укажите примерный объем топливного бака вашего автомобиля"

class GasTankSettingsViewController: UIViewController {
  private let tableView = UITableView()
  private let headerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 140))
  private let inputFieldCell = FuelUpInputTableViewCell("Примерный объем бака", formatterSymbols: "л.")
  private let switchCell = FuelUpSwitchTableViewCell("Всегда полный бак")
  private let footerButton = FuelUpButtonView(color: .tealish, title: "Сохранить")
  private let footerView = UIView()

  var bottomConstraint: NSLayoutConstraint!

  var observers: [Any] = []

  private var cells: [UITableViewCell] {
    return [inputFieldCell, switchCell]
  }

  var settings: Settings! {
    didSet {
      guard settings != oldValue else { return }
      let numberFormatter = NumberFormatter()
      numberFormatter.minimumFractionDigits = 0
      numberFormatter.maximumFractionDigits = 2
      inputFieldCell.inputField.text = numberFormatter.string(from: NSNumber(value: settings.gasTankVolume))
      switchCell.isOn = settings.alwaysFull
    }
  }

  var completionHandler: ((Settings?) -> Void)? = nil

  override func viewDidLoad() {
    automaticallyAdjustsScrollViewInsets = false
    edgesForExtendedLayout = []
    settings = Settings.current ?? Settings()

    title = "Заправка"

    setupHeader()
    setupFooter()
    setupTableView()

    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
  }

  @objc func navigationLeftButtonAction() {
    completionHandler?(nil)
    dismiss(animated: true, completion: nil)
  }

  func setupHeader() {
    let titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.textColor = UIColor.greyishBrown
    titleLabel.font = UIFont.boldSystemFont(ofSize: 21)
    titleLabel.text = "Полный бак"

    headerView.addSubview(titleLabel)
    titleLabel.easy.layout(
      Top(16),
      Left(16),
      Right(16)
    )

    let descriptionLabel = UILabel()
    descriptionLabel.numberOfLines = 0
    descriptionLabel.textAlignment = .center
    descriptionLabel.textColor = UIColor.greyishBrown
    descriptionLabel.font = UIFont.systemFont(ofSize: 17, weight: .light)
    descriptionLabel.text = descriptionText

    headerView.addSubview(descriptionLabel)
    descriptionLabel.easy.layout(
      Top(16).to(titleLabel),
      Left(16),
      Right(16),
      Bottom(16)
    )

    tableView.tableHeaderView = headerView
  }

  private func setupTableView() {
    view.addSubview(tableView)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none

    tableView.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
    tableView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Bottom().to(footerView, .top)
    )

    inputFieldCell.handler = { [unowned self] text in
      if let num = Double(text) {
        self.settings.gasTankVolume = num
      }
      self.footerButton.isEnabled = true
    }

    switchCell.handler = { [unowned self] isOn in
      self.settings.alwaysFull = isOn
      self.footerButton.isEnabled = true
    }

    tableView.tableFooterView = UIView()
    tableView.backgroundColor = UIColor.whiteThree
  }

  private func setupFooter() {
    view.addSubview(footerView)
    footerView.backgroundColor = .whiteThree
    footerButton.handler = { [unowned self] in
      Settings.current = self.settings
      self.completionHandler?(self.settings)
      self.dismiss(animated: true, completion: nil)
    }

    footerButton.isEnabled = false
    footerView.isUserInteractionEnabled = true
    footerView.addSubview(footerButton)
    footerButton.cornerRadius = 4
    footerButton.easy.layout(
      Top(12),
      Bottom(12),
      Left(30),
      Right(30),
      Height(56)
    )

    footerView.easy.layout(
      Left(),
      Right()
    )

    self.bottomConstraint = footerView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: 0)
    bottomConstraint.isActive = true
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    observers = bottomConstraint.addObserversUpdateWithKeyboard(view: self.view)
    inputFieldCell.inputField.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    observers.forEach { NotificationCenter.default.removeObserver($0) }
    observers = []
  }
}

extension GasTankSettingsViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cells.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return cells[indexPath.row]
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 53
  }
}

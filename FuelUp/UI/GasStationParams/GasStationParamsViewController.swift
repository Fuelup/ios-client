//
//  GasStationParamsViewController.swift
//  FuelUp
//
//  Created by Алексей on 13.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import Moya

class GasStationParamsViewController: UIViewController {
  let tableView = UITableView()
  let footerButton = FuelUpButtonView(color: .tealish, title: "Далее")
  let footerView = UIView()

  var bottomConstraint: NSLayoutConstraint!

  let pumpSelectCell = GasStationPumpSelectTableViewCell()
  let fuelTypeSelectCell = GasStationParamsFuelTypeSelectCell()
  let checkoutCell = GasStationCheckoutCell()
  let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
  let loaderAlert = LoaderAlertViewController()

  var observers: [Any] = []

  var currentLocation: CLLocationCoordinate2D!

  var request: Cancellable? = nil

  var gasStation: GasStation! {
    didSet {
      if gasStation.minified {
        request = GasStationsServce.instance.fetchStation(id: gasStation.id, { [weak self] result in
          self?.indicatorView.stopAnimating()
          switch result {
          case .success(let station):
            self?.gasStation = station
          case .failure(_):
            break
          }
        })
      }
      pumpSelectCell.pumps = gasStation.pumps
      tableView.reloadData()
    }
  }

  var settings: Settings? = nil {
    didSet {
      checkoutCell.gasTankVolume = settings?.gasTankVolume
      if let settings = settings, settings.alwaysFull {
        checkoutCell.currentItem = .fullTank
      }
    }
  }

  var selectedPump: Pump? = nil {
    didSet {
      guard selectedPump?.num != oldValue?.num else { return }

      if let pump = selectedPump {
        selectedFuelType = nil
        fuelTypeSelectCell.fuelTypes = gasStation.fuelTypes.filter { type in
          return pump.fuelTypes.contains(where: { $0.id == type.id})
        }
        
        if oldValue == nil {
          fuelTypeSelectCell.selectedFuelType = nil
          selectedFuelType = nil
          if #available(iOS 11.0, *) {
            tableView.performBatchUpdates({
              tableView.insertSections(IndexSet(integer: 1), with: .automatic)
            }, completion: { completed in
              guard completed else { return }
              self.fuelTypeSelectCell.handler = { [unowned self] fuelType in
                self.selectedFuelType = fuelType
              }
            })
          } else {
            tableView.beginUpdates()
            tableView.insertSections(IndexSet(integer: 1), with: .automatic)
            tableView.endUpdates()
            fuelTypeSelectCell.handler = { [unowned self] fuelType in
              self.selectedFuelType = fuelType
            }
          }
        }

      } else {
        tableView.reloadData()
      }
    }
  }

  var selectedFuelType: FuelType? = nil {
    didSet {
      guard selectedFuelType?.id != oldValue?.id else { return }
      checkoutCell.reset()
      if checkoutCell.currentItem != .fullTank && settings != nil {
        footerButton.isEnabled = false
      }
      guard let type = selectedFuelType else {
        tableView.deleteSections(IndexSet(integer: 2), with: .automatic)
        return
      }
      checkoutCell.pricePerLiter = type.price
      if oldValue == nil && selectedFuelType != nil {
        tableView.insertSections(IndexSet(integer: 2), with: .automatic)
      }
      footerButton.isEnabled = checkoutCell.currentItem == .fullTank
    }
  }
  
  var cells: [UITableViewCell] {
    var cells: [UITableViewCell] = [pumpSelectCell]
    if selectedPump != nil {
      cells.append(fuelTypeSelectCell)
    }
    if selectedFuelType != nil {
      cells.append(checkoutCell)
    }
    return cells
  }
  
  override func viewDidLoad() {
    title = "Параметры"
    view.backgroundColor = UIColor.whiteThree
    automaticallyAdjustsScrollViewInsets = false
    edgesForExtendedLayout = []
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    self.settings = Settings.current
    hideKeyboardWhenTappedAround()
    setupFooter()
    setupTableView()
    setupHandlers()
  }
  
  private func setupFooter() {
    view.addSubview(footerView)
    footerView.backgroundColor = .whiteThree
    footerButton.handler = { [unowned self] in
//      let createOrder = CreateOrder(station: <#T##String#>, pump: <#T##Int#>, fuel: <#T##String#>, value: <#T##Int#>, cost: <#T##Double#>, payment: <#T##String#>)
      /*CreateOrder*/
      let confirmViewController = PaymentConfirmViewController()
      let order = Order()
      order.pump = self.selectedPump?.num ?? 0
      /*
 order.amountRubles = self.checkoutCell.rubles
 order.fuelType = self.selectedFuelType?.id ?? 0
 order.gasStationId = self.gasStation.id*/
//      switch self.checkoutCell.currentItem {
//      case .rubles:
//        order.cost
//        order.cost = self.checkoutCell.value ?? 0
//      case .litres,.fullTank:
//        order.request = Int(self.checkoutCell.value ?? 0)
//      }
//      order.fuel = self.selectedFuelType?.fuel
      order.gasStation = self.gasStation
      confirmViewController.currentLocation = self.currentLocation
      confirmViewController.configure(station: self.gasStation, order: order)
      self.navigationController?.pushViewController(confirmViewController, animated: true)
    }
    
    footerButton.isEnabled = false
    footerView.isUserInteractionEnabled = true
    footerView.addSubview(footerButton)
    footerButton.cornerRadius = 4
    footerButton.easy.layout(
      Top(5),
      Bottom(16),
      Left(30),
      Right(30),
      Height(54)
    )
    
    footerView.easy.layout(
      Left(),
      Right()
    )

    self.bottomConstraint = footerView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: 0)
    bottomConstraint.isActive = true
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if gasStation.minified {
      indicatorView.startAnimating()
    }
  }
  
  private func setupHandlers() {
    pumpSelectCell.selectedPumpHandler = { [unowned self] pump in
      self.selectedPump = pump
    }

    checkoutCell.fullTankHandler = {
      let viewController = GasTankSettingsViewController()
      viewController.completionHandler = { settings in
        self.settings = settings ?? Settings.current
      }
      self.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
    }

    checkoutCell.handler = { [unowned self] item, value in
      let value = value ?? 0
      switch item {
      case .litres where value > 150, .fullTank where value > 150, .rubles where value > 9999:
        self.footerButton.isEnabled = false
      case .litres where value < 4, .fullTank where value < 4, .rubles where value < 150:
        self.footerButton.isEnabled = false
      case .fullTank where value < 4 || self.settings == nil:
        self.footerButton.isEnabled = false
        let viewController = GasTankSettingsViewController()
        viewController.completionHandler = { settings in
          self.settings = settings
        }
        self.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
      default:
        self.footerButton.isEnabled = true
      }
    }
  }
  
  private func setupTableView() {
    view.addSubview(tableView)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    tableView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Bottom().to(footerView, .top)
    )

    tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0)

    tableView.tableFooterView = UIView()
    tableView.backgroundColor = UIColor.whiteThree

    indicatorView.hidesWhenStopped = true
    view.addSubview(indicatorView)
    indicatorView.easy.layout(Center())
    indicatorView.color = .gray
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    settings = Settings.current
    observers = bottomConstraint.addObserversUpdateWithKeyboard(view: self.view)
  }

  func keyboardWasShown (notification: NSNotification) {
    guard let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }

    var contentInsets: UIEdgeInsets
    contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);

    tableView.contentInset = contentInsets

    tableView.scrollToRow(at: IndexPath(row: 0, section: cells.count - 1), at: .top, animated: true)
    tableView.scrollIndicatorInsets = tableView.contentInset
  }


  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    observers.forEach { NotificationCenter.default.removeObserver($0) }
    observers = []
  }
}

extension GasStationParamsViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return cells.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return cells[indexPath.section]
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
    case cells.index(of: pumpSelectCell)!:
      return pumpSelectCell.collectionView.collectionViewLayout.collectionViewContentSize.height
    case cells.index(of: fuelTypeSelectCell)!:
      return 150
    case cells.index(of: checkoutCell)!:
      return 140
    default:
      return 0
    }
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView()
    let label = UILabel()
    view.backgroundColor = UIColor.whiteThree
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    view.addSubview(label)

    label.easy.layout(
      Left(24),
      Top(10)
    )

    switch section {
    case cells.index(of: pumpSelectCell)!:
      label.text = "Выберите колонку:"
    case cells.index(of: fuelTypeSelectCell)!:
      label.text = "Выберите топливо:"
    case cells.index(of: checkoutCell)!:
      label.text = "Выберите способ расчета:"
    default:
      break
    }
    return view
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 34
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 133
  }
}

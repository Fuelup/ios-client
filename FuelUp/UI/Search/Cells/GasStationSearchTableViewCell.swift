//
//  GasStationSearchTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 03.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class GasStationSearchTableViewCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var routeBeginButton: RouteBeginButtonView!

  var routeButtonHandler: (() -> Void)? {
    get {
      return routeBeginButton.handler
    }
    set {
      routeBeginButton.handler = newValue
    }
  }

  func configure(station: GasStation, myLocation: CLLocationCoordinate2D?) {
    nameLabel.text = station.name
    addressLabel.text = station.address
    routeBeginButton.distance = station.distanceToString(myLocation)
  }
}

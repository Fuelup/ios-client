//
//  LocationsSearchViewController.swift
//  FuelUp
//
//  Created by Алексей on 03.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class GasStationsSearchViewController: UITableViewController {
  var searchController: UISearchController?

  var didSelectHandler: ((GasStation) -> Void)? = nil
  var routeButtonHandler: ((GasStation) -> Void)? = nil

  var gasStations: [GasStation] = [] {
    didSet {
      filterStations(searchController?.searchBar.text ?? "")
    }
  }

  var filteredStations: [GasStation] = [] {
    didSet {
      tableView.reloadData()
    }
  }

  var myLocation: CLLocationCoordinate2D? = nil {
    didSet {
      tableView.reloadData()
    }
  }


  func filterStations(_ filterString: String) {
    if filterString.isEmpty {
      filteredStations = []
    } else {
      filteredStations = gasStations.filter { $0.name.lowercased().contains(filterString.lowercased()) ||
        $0.address.lowercased().contains(filterString.lowercased()) }
    }
  }

  override func viewDidLoad() {
    tableView.backgroundColor = .tableBackground
    extendedLayoutIncludesOpaqueBars = false
    edgesForExtendedLayout = []
    setupSearchController()
    title = "Поиск"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(closeButtonAction))
    GasStationSearchTableViewCell.registerNib(in: tableView)
    tableView.separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
    tableView.tableFooterView = UIView()
    setupSearchController()
    navigationController?.navigationBar.isTranslucent = false
  }

  @objc func closeButtonAction() {
    dismiss(animated: true, completion: nil)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    searchController?.searchBar.setShowsCancelButton(false, animated: false)
    searchController?.isActive = true

  }

  func setupSearchController() {
    searchController = NoCancelSearchController(searchResultsController: nil)
    searchController!.searchResultsUpdater = self
    searchController!.searchBar.searchBarStyle = .minimal
    searchController!.hidesNavigationBarDuringPresentation = false
    searchController!.dimsBackgroundDuringPresentation = false
    searchController!.delegate = self
    searchController!.searchBar.backgroundColor = .tableBackground

    searchController!.searchBar.placeholder = "Введите название или адрес АЗС"
    tableView.tableHeaderView = searchController?.searchBar
    //searchController!.searchBar.setImage(#imageLiteral(resourceName: "search"), for: .search, state: .normal)

  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredStations.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = GasStationSearchTableViewCell.instance(tableView, indexPath)!
    let station = filteredStations[indexPath.row]
    cell.configure(station: station, myLocation: myLocation)
    cell.routeButtonHandler = { [weak self] in
      guard let `self` = self else { return }
      let viewController = GasStationRouteViewController()
      viewController.configure(location: self.myLocation!, station: station)
      self.searchController?.dismiss(animated: true, completion: {
        self.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
      })
    }
    return cell
  }

  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }

  override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 77
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    didSelectHandler?(filteredStations[indexPath.row])
    dismiss(animated: true, completion: nil)
    tableView.deselectRow(at: indexPath, animated: true)
  }

  override func dismiss(animated: Bool, completion: (() -> Void)?) {
    if let searchController = searchController, searchController.isActive {
      searchController.dismiss(animated: true, completion: {
        super.dismiss(animated: true, completion: nil)
      })
    } else {
      super.dismiss(animated: true, completion: nil)
    }
  }

}

extension GasStationsSearchViewController: UISearchResultsUpdating, UISearchControllerDelegate {
  func updateSearchResults(for searchController: UISearchController) {
    filterStations(searchController.searchBar.text ?? "")
  }

  func didPresentSearchController(_ searchController: UISearchController) {
    DispatchQueue.main.async {
      searchController.searchBar.becomeFirstResponder()
    }
  }
}

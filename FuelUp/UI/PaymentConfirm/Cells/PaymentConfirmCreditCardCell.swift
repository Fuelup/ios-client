//
//  PaymentConfirmCreditCardCell.swift
//  FuelUp
//
//  Created by Алексей on 21.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class PaymentConfirmCreditCardCell: UITableViewCell {
  //Card
  @IBOutlet weak var cardView: UIView!
  @IBOutlet weak var bankCardIconView: UIImageView!
  @IBOutlet weak var actionLabel: UILabel!
  @IBOutlet weak var cardNumberLabel: UILabel!
  //Company
  @IBOutlet weak var companyView: UIView!
  @IBOutlet weak var companyLogoView: UIImageView!
  @IBOutlet weak var companyNameLabel: UILabel!
  @IBOutlet weak var balanceLabel: UILabel!
  
  func setCompany(_ name: String, logo: String?, balance: Float) {
    if let logo = logo {
      let url = URL(string: logo)
      companyLogoView.kf.setImage(with: url)
    }
    companyNameLabel.text = name
    balanceLabel.text = balance.currencyString
    companyView.isHidden = false
    cardView.isHidden = true
  }

  func setCard(_ cardName: String?) {
    bankCardIconView.image = cardName == nil ? #imageLiteral(resourceName: "iconBankcardRed") : #imageLiteral(resourceName: "iconBankcard")
    actionLabel.text = cardName == nil ? "Добавить" : "Изменить"
    cardNumberLabel.text = cardName ?? "...?"
    cardNumberLabel.font = cardName == nil ? UIFont.boldSystemFont(ofSize: 35) : UIFont.boldSystemFont(ofSize: 18)
    cardNumberLabel.textColor = cardName == nil ? UIColor.lipstick : UIColor.tealish
    companyView.isHidden = true
    cardView.isHidden = false

  }
}

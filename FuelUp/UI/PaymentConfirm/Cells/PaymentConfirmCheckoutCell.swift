//
//  PaymentConfirmCheckoutCell.swift
//  FuelUp
//
//  Created by Алексей on 21.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class CheckoutItemView: UIView {
  private let titleLabel = UILabel()
  private let valueLabel = UILabel()

  var valueText: String? {
    get {
      return valueLabel.text
    }
    set {
      valueLabel.text = newValue
    }
  }

  init(title: String, titleTextColor: UIColor = .coolGrey, valueFont: UIFont = .systemFont(ofSize: 14, weight: .medium)) {
    super.init(frame: CGRect.null)
    backgroundColor = .clear
    titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    titleLabel.textAlignment = .left
    titleLabel.textColor = titleTextColor
    titleLabel.text = title
    valueLabel.textAlignment = .right
    valueLabel.font = valueFont
    titleLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
    addSubview(titleLabel)
    addSubview(valueLabel)

    titleLabel.easy.layout(
      Left(),
      Top(),
      Bottom()
    )

    valueLabel.easy.layout(
      Right(),
      Bottom(),
      Left().to(titleLabel, .right)
    )
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

enum PaymentConfirmCheckoutItem: Int {
  case pump = 0
  case fuel
  case volume

  var name: String {
    switch self {
    case .pump:
      return "Колонка"
    case .fuel:
      return "Топливо"
    case .volume:
      return "Литры"
    }
  }

  static let allValues = [pump, fuel, volume]
}

class PaymentConfirmCheckoutCell: UITableViewCell {
  let checkoutStackView = UIStackView()
  let lineView = UIView()
  let totalView = CheckoutItemView(title: "К оплате", titleTextColor: .black, valueFont: .boldSystemFont(ofSize: 14))

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  init() {
    super.init(style: .default, reuseIdentifier: nil)
    setup()
  }

  func setup() {
    selectionStyle = .none
    addSubview(checkoutStackView)
    checkoutStackView.axis = .vertical
    checkoutStackView.spacing = 8
    checkoutStackView.distribution = .fillEqually
    for item in PaymentConfirmCheckoutItem.allValues {
      checkoutStackView.addArrangedSubview(CheckoutItemView(title: item.name))
    }

    checkoutStackView.easy.layout(
      Top(20),
      Left(20),
      Right(20)
    )

    lineView.backgroundColor = .whiteThree
    addSubview(lineView)
    lineView.easy.layout(
      Top(20).to(checkoutStackView),
      Left(20),
      Right(20),
      Height(1)
    )

    addSubview(totalView)

    totalView.easy.layout(
      Top(20).to(lineView, .top),
      Left(20),
      Right(20),
      Bottom(20)
    )
  }

  func configure(fuelName: String, pumpNum: Int, volume: Double, amount: Double) {
    let numberFormatter = NumberFormatter()
    numberFormatter.minimumFractionDigits = 0
    numberFormatter.maximumFractionDigits = 2
    let values = ["\(pumpNum)", fuelName, numberFormatter.string(from: NSNumber(value: volume)) ?? ""]
    values.enumerated().forEach { (checkoutStackView.arrangedSubviews[$0] as! CheckoutItemView).valueText = $1 }
    totalView.valueText = amount.currencyString
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

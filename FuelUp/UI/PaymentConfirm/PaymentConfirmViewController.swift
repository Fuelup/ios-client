//
//  PaymentConfirmViewController.swift
//  FuelUp
//
//  Created by Алексей on 21.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import Moya

class PaymentConfirmViewController: UIViewController {
  let tableView = UITableView()
  let cardCell = PaymentConfirmCreditCardCell.nibInstance()!
  let checkoutCell = PaymentConfirmCheckoutCell()
  var cells: [UITableViewCell] {
    return [cardCell, checkoutCell]
  }
  let loaderViewController = LoaderAlertViewController()
  var currentRequest: Cancellable? = nil

  var selectedCard: PaymentHistoryData? = nil {
    didSet {
      footerButton.isEnabled = selectedCard != nil
      
      if let selectedCard = selectedCard {
        switch selectedCard.type {
        case .card(let card):
          cardCell.setCard(card.maskedLastDigitsPan)
        case .taxi(let taxi):
          cardCell.setCompany(taxi.company, logo: taxi.logo, balance: taxi.balance)
        }
      }
      else {
        cardCell.setCard(nil)
      }
    }
  }

  var station: GasStation!
  var order: Order!

  var currentLocation: CLLocationCoordinate2D!

  let footerButton = FuelUpButtonView(color: .bloodOrange, title: "Оплатить")
  let footerView = UIView()

  func configure(station: GasStation, order: Order) {
    self.station = station
    self.order = order
    let fuelType = station.fuelTypes.first(where: { $0.id == order.fuelType })
    let fuelPrice = fuelType?.price ?? 0
    let volume = fuelPrice == 0 ? 0 : order.amountRubles/fuelPrice
    checkoutCell.configure(fuelName: fuelType?.name ?? "", pumpNum: order.pump, volume: volume, amount: order.amountRubles)
  }

  override func viewDidLoad() {
    title = "Подтверждение"
    selectedCard = PaymentsService.instance.currentCard
    setupFooter()
    setupTableView()
  }

  private func setupFooter() {
    view.addSubview(footerView)

    footerView.backgroundColor = .whiteThree
    footerButton.handler = { [unowned self] in
      if let coordinates = self.station.coordinates {
        let distanceInMeters = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude).distance(from: CLLocation(latitude: self.currentLocation.latitude, longitude: self.currentLocation.longitude))
        if distanceInMeters > 100 {
          let alertViewController = FuelUpAlertViewController()
          alertViewController.configure(item: .distance)
          alertViewController.addAction(title: "Попробовать еще раз", color: .tealish, handler: {
            self.navigationController?.popViewController(animated: true)
          })
          alertViewController.modalPresentationStyle = .overCurrentContext
          self.present(alertViewController, animated: false)
          return
        }
      }
      self.loaderViewController.titleText = "Проверка доступности колонки"
      self.loaderViewController.removeActions()
      self.loaderViewController.addAction(title: "Отмена", closes: false, handler: { [unowned self] in
        self.loaderViewController.presentCancelAlert {
          self.currentRequest?.cancel()
          if !self.order.id.isEmpty {
            OrdersService.instance.cancelOrder(orderId: self.order.id, { _ in
            })
          }
        }
      })
      self.currentRequest = GasStationsServce.instance.fetchStation(id: self.station.id, { result in
        switch result {
        case .success(let gasStation):
          if let pump = gasStation.pumps.first(where: { $0.num == self.order.pump }), !pump.isBlock {
            self.createOrder()
          } else {
            self.loaderViewController.goBack {
              let alertViewController = FuelUpAlertViewController()
              alertViewController.configure(item: .pumpUnavailable)
              alertViewController.addAction(title: "Попробовать еще раз", color: .tealish, handler: {
                self.navigationController?.popViewController(animated: true)
              })
              alertViewController.modalPresentationStyle = .overCurrentContext
              self.present(alertViewController, animated: false)
            }
          }
        case .failure(_):
          self.loaderViewController.goBack {
            let alertViewController = FuelUpAlertViewController()
            alertViewController.configure(item: .unknown)
            alertViewController.addAction(title: "Попробовать еще раз", color: .tealish, handler: { })
            alertViewController.modalPresentationStyle = .overCurrentContext
            self.present(alertViewController, animated: false)
          }
        }
      })
      self.loaderViewController.modalPresentationStyle = .overCurrentContext
      self.present(self.loaderViewController, animated: false)
    }

    footerView.addSubview(footerButton)
    footerButton.cornerRadius = 4
    footerButton.easy.layout(
      Top(16),
      Bottom(16),
      Left(16),
      Right(16),
      Height(54)
    )

    footerView.easy.layout(
      Left(),
      Right(),
      Bottom().to(bottomLayoutGuide)
    )
  }

  func createOrder() {
    currentRequest = OrdersService.instance.createOrder(order: self.order) { [unowned self] result in
      switch result {
      case .success(let order):
        self.order = order
        if !order.status.error {
          self.loaderViewController.titleText = order.status.message
          self.beginRefreshingOrder()
        } else {
          let alertViewController = FuelUpAlertViewController()
          alertViewController.configure(item: order.status)
          alertViewController.addAction(title: "Попробовать еще раз", color: .tealish, handler: { })
          alertViewController.modalPresentationStyle = .overCurrentContext
          self.present(alertViewController, animated: false)
        }
      case .failure(_):
        self.loaderViewController.goBack {
          let alertViewController = FuelUpAlertViewController()
          alertViewController.configure(item: .unknown)
          alertViewController.addAction(title: "Попробовать еще раз", color: .tealish, handler: { })
          alertViewController.modalPresentationStyle = .overCurrentContext
          self.present(alertViewController, animated: false)
        }
      }
    }
  }

  func beginRefreshingOrder() {
    OrdersService.instance.beginRefreshOrder(order) { order in
      if !order.status.error {
        if order.status == .fueling {
          self.loaderViewController.goBack {
            let viewController = FuelingProcessViewController()
            viewController.configure(station: self.station, order: order)
            viewController.finishHandler = { [weak self] in
              self?.navigationController?.popToRootViewController(animated: false)
            }
            self.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
          }
        }
      } else {
        self.loaderViewController.goBack {
          let alertViewController = FuelUpAlertViewController()
          alertViewController.configure(item: order.status)
          alertViewController.addAction(title: "Попробовать еще раз", color: .tealish, handler: { })
          alertViewController.modalPresentationStyle = .overCurrentContext
          self.present(alertViewController, animated: false)
        }
      }
    }
  }

  private func setupTableView() {
    view.addSubview(tableView)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    tableView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Bottom().to(footerView, .top)
    )

    tableView.tableFooterView = UIView()
    tableView.backgroundColor = UIColor.whiteThree
  }

}

extension PaymentConfirmViewController: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return cells.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return cells[indexPath.section]
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView()
    let label = UILabel()
    view.backgroundColor = UIColor.whiteThree
    label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
    view.addSubview(label)

    label.easy.layout(
      Left(20),
      Top(16)
    )

    switch section {
    case cells.index(of: cardCell)!:
      label.text = "Выберите карту для оплаты:"
    case cells.index(of: checkoutCell)!:
      label.text = "Все правильно?"
    default:
      break
    }
    return view
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
    case cells.index(of: cardCell)!:
      return 114
    case cells.index(of: checkoutCell)!:
      return 160
    default:
      return 0
    }
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch indexPath.section {
    case cells.index(of: cardCell)!:
      let viewController = CardSelectAlertViewController()
      viewController.modalPresentationStyle = .overCurrentContext
      viewController.selectedCardId = self.selectedCard?.identifier
      viewController.selectHandler = { [weak self] card in
        self?.selectedCard = card
      }
      present(viewController, animated: false)
    default:
      break
    }
  }

}

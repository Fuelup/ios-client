//
//  GasStationPreviewTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 01.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class GasStationPreviewTableViewCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var routeView: RouteBeginButtonView!

  var routeButtonHandler: (() -> Void)? {
    get {
      return routeView.handler
    }
    set {
      routeView.handler = newValue
    }
  }

  func configure(station: GasStation, myLocation: CLLocationCoordinate2D?) {
    nameLabel.text = station.name
    addressLabel.text = station.address
    routeView.distance = station.distanceToString(myLocation)
  }
}

//
//  EnableLocationMapView.swift
//  FuelUp
//
//  Created by Алексей on 01.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class EnableLocationMapView: UIView {

  @IBAction func enableLocationViewAction(_ sender: Any) {
    if !CLLocationManager.locationServicesEnabled() {
      if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
        UIApplication.shared.open(url)
      }
    } else {
      if let url = URL(string: UIApplicationOpenSettingsURLString) {
        UIApplication.shared.open(url)
      }
    }
  }
}

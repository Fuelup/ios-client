//
//  GasStationsTableView.swift
//  FuelUp
//
//  Created by Алексей on 03.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import PullUpController

open class GasStationsListViewController: PullUpController {
  private let tableView = GasStationsListTableView()
  private let swipeView = UIView()

  var gasStations: [GasStation] {
    get {
      return tableView.gasStations
    }
    set {
      tableView.gasStations = newValue
      scrollToFirstRow()
    }
  }

  var myLocation: CLLocationCoordinate2D? {
    get {
      return tableView.myLocation
    }
    set {
      tableView.myLocation = newValue
    }
  }

  var routeButtonHandler: ((GasStation) -> Void)? {
    get {
      return tableView.routeButtonHandler
    }
    set {
      tableView.routeButtonHandler = newValue
    }
  }

  open override var pullUpControllerPreviewOffset: CGFloat {
    return 125
  }

  open override var pullUpControllerPreferredSize: CGSize {
    return CGSize(width: UIScreen.main.bounds.width, height: view.frame.height - additionalTopOffset - 64)
  }

  var additionalTopOffset: CGFloat = 0 {
    didSet {
      guard additionalTopOffset != oldValue else { return }
      pullUpControllerMoveToVisiblePoint(pullUpControllerPreviewOffset, completion: nil)
    }
  }

  var handler: ((GasStation) -> Void)? = nil

  func scrollToFirstRow() {
    pullUpControllerMoveToVisiblePoint(pullUpControllerPreviewOffset, completion: nil)
  }

  override open func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(tableView)
    tableView.clipsToBounds = true
    tableView.cornerRadius = 4
    view.backgroundColor = .clear

    tableView.easy.layout(
      Top(),
      Left(8),
      Right(8),
      Bottom()
    )

    tableView.attach(to: self)

    //tableView.clipsToBounds = true
    view.backgroundColor = UIColor.clear

    tableView.handler = { [unowned self] station in
      self.scrollToFirstRow()
      self.handler?(station)
    }
  }

}

class GasStationsListTableView: UITableView, UITableViewDelegate, UITableViewDataSource	 {

  var gasStations: [GasStation] = [] {
    didSet {
      reloadData()
    }
  }

  var myLocation: CLLocationCoordinate2D? = nil {
    didSet {
      reloadData()
    }
  }

  var routeButtonHandler: ((GasStation) -> Void)? = nil
  var handler: ((GasStation) -> Void)? = nil
  var isAtTopHandler: ((Bool) -> Void)? = nil

  init() {
    super.init(frame: CGRect.null, style: .plain)
    delegate = self
    dataSource = self

    separatorInset = UIEdgeInsetsMake(0, 16, 0, 16)
    backgroundColor = .clear
    showsVerticalScrollIndicator = false
    tableFooterView =  UIView()
    tableFooterView?.backgroundColor = .clear
    tableFooterView?.clipsToBounds = true
    tableFooterView?.cornerRadius = 4
    GasStationPreviewTableViewCell.registerNib(in: self)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return gasStations.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = GasStationPreviewTableViewCell.instance(tableView, indexPath)!
    let station = gasStations[indexPath.row]
    cell.configure(station: station, myLocation: myLocation)
    cell.routeButtonHandler = { [weak self] in
      self?.routeButtonHandler?(station)
    }
    if indexPath.row == gasStations.count {
      cell.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
    }
    cell.clipsToBounds = true
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 76
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView()
    view.clipsToBounds = true
    view.backgroundColor = .white
    let indicatorView = UIImageView(image: #imageLiteral(resourceName: "toggleIndicator"))
    indicatorView.contentMode = .scaleAspectFit
    view.addSubview(indicatorView)
    view.cornerRadius = 4
    indicatorView.easy.layout(Bottom(),Top(12), CenterX())
    return view
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 20
  }

  func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: true)
    handler?(gasStations[indexPath.row])
  }
}



//
//  BankCardView.swift
//  FuelUp
//
//  Created by Алексей on 01.12.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class BankCardView: UIView {

  var tapHandler: (() -> Void)? = nil

  override func awakeFromNib() {
    super.awakeFromNib()
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapAction)))
  }

  @objc func tapAction() {
    tapHandler?()
  }
}

//
//  GasStationPreviewView.swift
//  FuelUp
//
//  Created by Алексей on 30.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class GasStationPreviewView: UIView {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var buttonView: FuelUpButtonView!

  @IBOutlet weak var routeBeginButtonView: RouteBeginButtonView!

  let numberFormatter = NumberFormatter()

  var beginButtonHandler: (() -> Void)? {
    get {
      return buttonView.handler
    }
    set {
      buttonView.handler = newValue
    }
  }

  var routeButtonHandler: (() -> Void)? {
    get {
      return routeBeginButtonView.handler
    }
    set {
      routeBeginButtonView.handler = newValue
    }
  }

  var handler: (() -> Void)? = nil

  override func awakeFromNib() {
    super.awakeFromNib()
    
    buttonView.arrowView.tintColor = UIColor.white
    buttonView.titleLabel.text = "Начать заправку"
    buttonView.backgroundColor = .tomato

  }

  func configure(_ station: GasStation, location: CLLocationCoordinate2D?) {
    titleLabel.text = station.name
    addressLabel.text = station.address
    guard let distString = numberFormatter.string(from: NSNumber(value: station.distanceTo(location))) else { return }
    routeBeginButtonView.distance = "\(distString) км"
  }

  @IBAction func routeInfoWindowTapAction(_ sender: Any) {
    handler?()
  }

}

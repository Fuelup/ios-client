  //
//  GasStationsMapViewController.swift
//  FuelUp
//
//  Created by Алексей on 29.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import CoreLocation
import SideMenu
import PullUpController
import GoogleMaps

class POIItem: NSObject, GMUClusterItem {
  var position: CLLocationCoordinate2D
  var id: String
  var name: String!
  var marker: GMSMarker? = nil

  init(position: CLLocationCoordinate2D, name: String, id: String) {
    self.name = name
    self.id = id
    self.position = position
    super.init()
  }

  init(station: GasStation) {
    id = station.id
    name = station.name
    position = station.coordinates ?? CLLocationCoordinate2DMake(0, 0)
    super.init()
  }

  override func isEqual(_ object: Any?) -> Bool {
    guard let station = object as? POIItem else { return false }
    return station.id == self.id
  }
}

class GasStationsMapViewController: UIViewController {
  var mapView: GMSMapView! {
    didSet {
      mapView.delegate = self
      mapView.isMyLocationEnabled = true
      locationManager.delegate = self
      let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
      let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 50, 100])
      let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                               clusterIconGenerator: iconGenerator)
      renderer.animatesClusters = true
      renderer.delegate = self
      clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                         renderer: renderer)
      clusterManager?.setDelegate(self, mapDelegate: self)
    }
  }

  let locationManager = CLLocationManager()
  private var clusterManager: GMUClusterManager!
  let plusButton = UIButton()
  let minusButton = UIButton()
  let locationButton = UIButton()
  var infoWindow = GasStationPreviewView.nibInstance()!
  var tableViewController = GasStationsListViewController()
  let cardView = EnableLocationMapView.nibInstance()!
  let bankCardView = BankCardView.nibInstance()!
  var oldZoom: Float = 0

  var authorizationInited: Bool = false

  var activePoint : POIItem?

  var poiItems: [POIItem] = [] {
    didSet {
      DispatchQueue.main.async { [unowned self] in
        guard self.mapView != nil else { return }
        self.clusterManager.clearItems()
        self.clusterManager.add(self.poiItems)
      }
    }
  }

  var gasStations: [GasStation] = [] {
    didSet {
      poiItems = gasStations.map(POIItem.init)
      tableViewController.gasStations = gasStations
      if gasStations.count > 1 {
        tableViewController.scrollToFirstRow()
      }
    }
  }

  var zoom: Int = 14 {
    didSet {
      mapView.animate(toZoom: Float(zoom))
      locationManager.stopUpdatingLocation()
      updateMarker()
    }
  }

  var currentLocation: CLLocationCoordinate2D? {
    didSet {
      tableViewController.myLocation = currentLocation
    }
  }

  var selectedMarker: GMSMarker? = nil

  var selectedMarkerId: String? = nil {
    didSet {
      clusterManager.cluster()
      guard selectedMarkerId != nil else {
        selectedMarker = nil
        infoWindow.removeFromSuperview()
        return
      }
      if let station = gasStations.first(where: { $0.id == selectedMarkerId }) {
        infoWindow.configure(station, location: currentLocation)
        infoWindow.routeButtonHandler = { [weak self] in
          let viewController = GasStationRouteViewController()
          viewController.configure(location: (self?.currentLocation)!, station: station)
          self?.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
        }
        infoWindow.beginButtonHandler = { [weak self] in
          self?.presentErrorAlert(message: "Не работает")
//          guard let `self` = self else { return }
//          let viewController = GasStationParamsViewController()
//          viewController.gasStation = station
//          viewController.currentLocation = self.currentLocation!
//          self.navigationController?.pushViewController(viewController, animated: true)

        }
        infoWindow.handler = { [weak self] in
          guard let `self` = self else { return }
          let viewController = GasStationScreenViewController()
          viewController.currentLocation = self.currentLocation!
          viewController.gasStation = station
          self.navigationController?.pushViewController(viewController, animated: true)
        }
      }

      if infoWindow.superview == nil {
        mapView.addSubview(infoWindow)
      }
      updateMarker()
    }
  }
  
  var currentCoordinates: CLLocationCoordinate2D? = nil

  override func viewDidLoad()
  {
    mapView = GMSMapView()
    addConstraints()
    configureButtons()
    didChangeStatus(CLLocationManager.authorizationStatus())
    locationManager.startUpdatingLocation()
    navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "fuelUpLogo"))
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "burgerMenuIcon"), style: .plain, target: self, action: #selector(burgerMenuTapped))
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(searchButtonTapped))
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    refresh()
    PushService.instance.registerForPushNotifications()    
  }

  func refresh() {
    GasStationsServce.instance.fetchFuels { _ in
      GasStationsServce.instance.fetchStations { [weak self] result in
        switch result {
        case .success(let stations):
          guard let `self` = self else { return }
          self.gasStations = stations.sorted { $0.distanceTo(self.currentLocation) < $1.distanceTo(self.currentLocation) }
        case .failure(_):
          break
        }
      }
    }
  }
  
  func updateSorting() {
    self.gasStations = self.gasStations.sorted { $0.distanceTo(self.currentLocation) < $1.distanceTo(self.currentLocation) }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    didChangeStatus(CLLocationManager.authorizationStatus())
    updateCardView()
    if PaymentsService.instance.payments.isEmpty {
      PaymentsService.instance.refreshCardList { _ in
        self.updateCardView()
      }
    }
  }

  func updateCardView() {
    let isHidden = !PaymentsService.instance.payments.isEmpty
    tableViewController.additionalTopOffset = isHidden ? 0 : 64
    guard bankCardView.isHidden != isHidden else { return }
    bankCardView.isHidden = isHidden
    UIView.animate(withDuration: 0.2, animations: {
      self.bankCardView.easy.layout(
        Top(isHidden ? -64 : 0).to(self.topLayoutGuide),
        Left(),
        Right(),
        Height(64)
      )
      self.view.layoutIfNeeded()
    }, completion: { _ in
      self.bankCardView.isHidden = isHidden
    })

  }

  @objc func burgerMenuTapped() {
    present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
  }

  @objc func searchButtonTapped() {
    let searchController = GasStationsSearchViewController()
    searchController.gasStations = gasStations
    searchController.myLocation = currentLocation
    searchController.didSelectHandler = { [weak self] station in
      self?.select(station)
    }
    
    present(FuelUpNavigationController(rootViewController: searchController), animated: true)
  }

    func configureButtons() {
    plusButton.setImage(#imageLiteral(resourceName: "plusButton"), for: .normal)
    minusButton.setImage(#imageLiteral(resourceName: "minusButton"), for: .normal)
    locationButton.setImage(#imageLiteral(resourceName: "iconMyLocationButton"), for: .normal)

    plusButton.addTarget(self, action: #selector(zoomPlusAction), for: .touchUpInside)
    minusButton.addTarget(self, action: #selector(zoomMinusAction), for: .touchUpInside)
    locationButton.addTarget(self, action: #selector(locationAction), for: .touchUpInside)
  }


  @objc func zoomPlusAction() {
    zoom += 1
  }

  @objc func zoomMinusAction() {
    zoom -= 1
  }

  @objc func locationAction() {
    mapView.isMyLocationEnabled = true
    locationManager.startUpdatingLocation()

  }

  func addConstraints() {
    view.addSubview(mapView)
    mapView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Bottom()
    )

    let zoomButtonsView = UIView()
    view.addSubview(zoomButtonsView)

    zoomButtonsView.backgroundColor = .clear

    zoomButtonsView.addSubview(plusButton)
    zoomButtonsView.addSubview(minusButton)
    zoomButtonsView.addSubview(locationButton)

    plusButton.easy.layout([
      Top(),
      Left(),
      Right()
    ])

    minusButton.easy.layout([
      Top(24).to(plusButton, .bottom),
      CenterX().to(plusButton)
    ])

    locationButton.easy.layout([
      Top(24).to(minusButton, .bottom),
      Bottom(),
      CenterX().to(plusButton)
    ])

    zoomButtonsView.easy.layout([
      CenterY().with(.medium),
      Right(20)
    ])

    setupTableView()

    view.addSubview(bankCardView)
    bankCardView.easy.layout(
      Top().to(topLayoutGuide),
      Left(),
      Right(),
      Height(64)
    )
  }


  func configureLocation() {
    locationManager.requestAlwaysAuthorization()
    locationManager.startUpdatingLocation()
  }

  func setupTableView() {
    cardView.removeFromSuperview()
    tableViewController.handler = { [weak self] station in
      self?.select(station)
    }

    tableViewController.routeButtonHandler = { [weak self] station in
      let viewController = GasStationRouteViewController()
      viewController.configure(location: (self?.currentLocation)!, station: station)
      self?.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
    }

    addPullUpController(tableViewController)

    bankCardView.tapHandler = { [unowned self] in
      let viewController = AddCreditCardViewController()
      self.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
    }
  }

  func select(_ station: GasStation) {
    self.mapView.animate(to: GMSCameraPosition.camera(withLatitude: station.coordinates!.latitude, longitude: station.coordinates!.longitude, zoom: 17))
    zoom = 17
    selectedMarkerId = station.id
    selectedMarker = poiItems.first(where: { $0.id == station.id })?.marker
    clusterManager.cluster()
    tableViewController.scrollToFirstRow()
  }

  func configureInfoWindowHandlers() {

  }

  func didChangeStatus(_ status: CLAuthorizationStatus) {
    if status == .authorizedWhenInUse || status == .authorizedAlways {
      tableViewController.view.isHidden = false
      cardView.removeFromSuperview()
    } else {
      tableViewController.view.isHidden = true
      view.addSubview(cardView)
      cardView.easy.layout(
        Bottom().to(bottomLayoutGuide),
        Left(8),
        Right(8),
        Height(107)
      )
    }   
  }

}
extension GasStationsMapViewController: GMUClusterManagerDelegate, GMUClusterRendererDelegate {
  func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
    let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                             zoom: mapView.camera.zoom + 2)
    let update = GMSCameraUpdate.setCamera(newCamera)
    mapView.moveCamera(update)
    return true
  }
  func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
    guard let item = clusterItem as? POIItem else { return false }
    selectedMarkerId = item.id
    selectedMarker = item.marker
    return true
  }

  func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
    guard let item = marker.userData as? POIItem else { return }
    item.marker = marker
    marker.icon = item.id == selectedMarkerId && selectedMarkerId != nil ? #imageLiteral(resourceName: "iconStationSelected") : #imageLiteral(resourceName: "iconStation")
    if item.id == selectedMarkerId && selectedMarkerId != nil {
      selectedMarker = marker
    }
    updateMarker()
  }
}
extension GasStationsMapViewController: GMSMapViewDelegate {
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    mapView.animate(to: GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.longitude, zoom: mapView.camera.zoom))
    return false

  }

  func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    infoWindow.removeFromSuperview()
    selectedMarker = nil
    selectedMarkerId = nil
  }
}


extension GasStationsMapViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let coordinate = locations.first?.coordinate else { return }
    self.currentLocation = coordinate
    self.zoom = 13
    mapView.animate(to: GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: Float(self.zoom)))
    locationManager.stopUpdatingLocation()
  }

  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    didChangeStatus(status)
    if status != .authorizedWhenInUse && status != .authorizedAlways && authorizationInited {
      let viewController = EnableLocationViewController()
      viewController.modalPresentationStyle = .overFullScreen
      present(viewController, animated: false)
    }
    authorizationInited = true
  }

  func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    updateMarker()
    let zoom = position.zoom
    if zoom != self.oldZoom {
      self.oldZoom = zoom
    }
  }

  func                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            updateMarker(offset: CGFloat? = nil) {
    guard let location = selectedMarker?.position else { return }
    var point = mapView.projection.point(for: location)
    point.y -= infoWindow.frame.height/2 + #imageLiteral(resourceName: "iconStation").size.height
    if let offset = offset {
      point.y += offset
    }
    infoWindow.center = point
  }

  func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {

    updateMarker()

  }
}



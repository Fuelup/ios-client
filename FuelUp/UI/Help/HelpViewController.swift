//
//  HelpViewController.swift
//  FuelUp
//
//  Created by Богдан Быстрицкий on 29/12/2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
  
  override func viewDidLoad() {
    title = "Помощь"
    super.viewDidLoad()
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    
    navigationController?.navigationBar.isTranslucent = false
  }
  
  @objc func navigationLeftButtonAction() {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func feedbackButtonDidTapped(_ sender: Any) {
    let email = "support@fuelup.ru"
    if let url = URL(string: "mailto:\(email)") {
      UIApplication.shared.open(url)
    }
  }
  
  @IBAction func faqButtonDidTapped(_ sender: Any) {
    let faqVC = FaqViewController()
    self.navigationController?.show(faqVC, sender: self)
  }
  
  @IBAction func onboardingButtonDidTapped(_ sender: Any) {
    let viewController = AuthorizationViewController()
    viewController.shouldSkipRegistration = true
    present(FuelUpNavigationController(rootViewController: viewController))
  }
}

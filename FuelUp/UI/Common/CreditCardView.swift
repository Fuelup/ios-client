//
//  CreditCardView.swift
//  FuelUp
//
//  Created by Алексей on 26.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import Kingfisher

enum CreditCardViewData {
  case card(card: CardData, number: Int)
  case taxi(taxi: TaxiData, number: Int)
  case addCard
}

class CreditCardView: UIView {
  private let iconView = UIImageView()
  private let titleLabel = UILabel()
  private let descriptionLabel = UILabel()
  private let balanceLabel = UILabel()
  private let titleView = UIStackView()
  let menuButton = UIButton()

  var menuHandler: (() -> Void)? = nil

  var data: CreditCardViewData! {
    didSet {
      guard let data = data else { return }
      switch data {
      case .card(let card, let number):
        menuButton.isHidden = false
        if descriptionLabel.superview == nil {
          titleView.addArrangedSubview(descriptionLabel)
        }        
        if balanceLabel.superview == nil {
          self.addSubview(balanceLabel)
          balanceLabel.easy.layout(
            Right(16),
            CenterY(0)
          )
        }
        titleLabel.text = card.isActive ? "Активная карта" : "Карта №\(number)"
        descriptionLabel.text = card.maskedLastDigitsPan
        titleLabel.textColor = card.isActive ? .black : .warmGreyTwo
        descriptionLabel.textColor = card.isActive ? .black : .warmGreyTwo
        titleLabel.font = card.isActive ? UIFont.systemFont(ofSize: 17, weight: .semibold) : UIFont.systemFont(ofSize: 17)
        descriptionLabel.font = card.isActive ? UIFont.systemFont(ofSize: 17, weight: .semibold) : UIFont.systemFont(ofSize: 17)
        iconView.image = card.cardType?.image
        menuButton.isHidden = false
        iconView.borderWidth = 0
        balanceLabel.removeFromSuperview()
      case .taxi(let taxi, _):
        menuButton.isHidden = false
        if descriptionLabel.superview == nil {
          titleView.addArrangedSubview(descriptionLabel)
        }
        if balanceLabel.superview == nil {
          self.addSubview(balanceLabel)
          balanceLabel.easy.layout(
            Right(16),
            CenterY(0)
          )
        }
        titleLabel.text = taxi.company
        descriptionLabel.text = ""
        balanceLabel.text = taxi.balance.currencyString
        titleLabel.textColor = .black
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        balanceLabel.font = UIFont.systemFont(ofSize: 17, weight: .heavy)
        if let logo = taxi.logo {
          iconView.kf.setImage(with: URL(string: logo))
        }
        else {
          iconView.image = #imageLiteral(resourceName: "TestCompanyLogo")
        }
        menuButton.isHidden = true
      case .addCard:
        menuButton.isHidden = true
        titleView.removeArrangedSubview(descriptionLabel)
        descriptionLabel.removeFromSuperview()
        balanceLabel.removeFromSuperview()
        iconView.image = UIImage()
        iconView.borderWidth = 0.5
        titleLabel.textColor = .bloodOrange
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        titleLabel.text = "Добавить новую карту"
      }
    }
  }

  init() {
    super.init(frame: CGRect.null)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }


  func setup() {
    iconView.cornerRadius = 1.6
    iconView.borderColor = .bloodOrange
    addSubview(iconView)
    iconView.easy.layout(
      Left(24),
      CenterY(),
      Width(40),
      Height(24)
    )
    iconView.contentMode = .center

    titleView.axis = .vertical
    titleView.alignment = .fill
    titleView.spacing = 4
    addSubview(titleView)

    titleView.addArrangedSubview(titleLabel)
    titleView.addArrangedSubview(descriptionLabel)
    self.addSubview(balanceLabel)
    balanceLabel.easy.layout(
      Right(16),
      CenterY(0)
    )    

    menuButton.setImage(#imageLiteral(resourceName: "icon-dots-menu").withRenderingMode(.alwaysTemplate), for: .normal)
    menuButton.tintColor = .warmGrey
    menuButton.addTarget(self, action: #selector(menuAction), for: .touchUpInside)
    addSubview(menuButton)
    menuButton.easy.layout(
      Size(50),
      Right(),
      Top()
    )

    titleView.easy.layout(
      Left(16).to(iconView),
      Right().to(menuButton, .left),
      CenterY()
    )
  }

  @objc func menuAction() {
    menuHandler?()
  }
}

//
//  RouteBeginButtonView.swift
//  FuelUp
//
//  Created by Алексей on 08.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class RouteBeginButtonView: UIView {
  private let button = UIButton()
  private let iconView = UIImageView(image: #imageLiteral(resourceName: "iconGpsRoute"))
  private let routeDistanceLabel = UILabel()

  var handler: (() -> Void)? = nil

  var distance: String? {
    get {
      return routeDistanceLabel.text
    }
    set {
      routeDistanceLabel.text = newValue
    }
  }

  init() {
    super.init(frame: CGRect.null)
    setup()
  }

  func setup() {
    backgroundColor = UIColor.tealish
    cornerRadius = 4
    routeDistanceLabel.text = "0 км"
    addSubview(iconView)
    iconView.easy.layout(
      Top(9),
      CenterX()
    )
    routeDistanceLabel.font = UIFont.systemFont(ofSize: 10, weight: .bold)
    routeDistanceLabel.textAlignment = .center
    addSubview(routeDistanceLabel)
    routeDistanceLabel.easy.layout(
      Top(5).to(iconView),
      Left(),
      Right(),
      Bottom(>=5)
    )
    routeDistanceLabel.textColor = UIColor.white
    addSubview(button)
    button.easy.layout(Edges())
    button.addTarget(self, action: #selector(buttonActionTouchUpInside), for: .touchUpInside)
    button.addTarget(self, action: #selector(buttonActionTouchDownInside), for: .touchDown)
    button.addTarget(self, action: #selector(buttonActionTouchUpInside), for: .touchUpInside)
    button.addTarget(self, action: #selector(buttonActionTouchDownInside), for: .touchDragEnter)
    button.addTarget(self, action: #selector(buttonActionTouchCancel), for: .touchDragExit)
    button.addTarget(self, action: #selector(buttonActionTouchCancel), for: .touchCancel)

  }

  @objc func buttonActionTouchUpInside() {
    backgroundColor = UIColor.tealish
    handler?()
  }

  @objc func buttonActionTouchDownInside() {
    backgroundColor = UIColor.tealish.darker()
  }

  @objc func buttonActionTouchCancel() {
    backgroundColor = UIColor.tealish
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
}

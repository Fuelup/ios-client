//
//  GoBarItemView.swift
//  FuelUp
//
//  Created by Алексей on 14.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class GoBarHeaderItemView: UIView {
  private let lineView = UIView()
  private let titleButton = UIButton(type: .system)

  var handler: (() -> Void)? = nil

  var isSelected: Bool = false {
    didSet {
      let color = isSelected ? UIColor.coolBlue : UIColor.warmGreyTwo
      self.titleButton.setTitleColor(color, for: .normal)
      self.lineView.backgroundColor = color
    }
  }

  init(title: String, color: UIColor = .warmGreyTwo) {
    super.init(frame: CGRect.null)

    titleButton.setTitle(title, for: .normal)
    titleButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
    titleButton.tintColor = color
    titleButton.setTitleColor(color, for: .normal)
    lineView.backgroundColor = .silver

    addSubview(titleButton)
    addSubview(lineView)

    titleButton.easy.layout(
      Edges()
    )

    lineView.easy.layout(
      Height(1),
      Left(),
      Right(),
      Bottom()
    )

    titleButton.addTarget(self, action: #selector(buttonTapAction), for: .touchUpInside)
  }

  @objc func buttonTapAction() {
    handler?()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}

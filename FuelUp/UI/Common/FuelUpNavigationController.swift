//
//  FuelUpNavigationController.swift
//  PASSCITY
//
//  Created by Алексей on 28.09.17.
//  Copyright © 2017 PASSCITY. All rights reserved.
//

import Foundation
import UIKit

protocol LightContentViewController { }
protocol TransparentViewController: class { }

class FuelUpNavigationController: UINavigationController, UINavigationControllerDelegate {

  var nextViewController: UIViewController? = nil

  override func loadView() {
    super.loadView()
    delegate = self
    navigationBar.isTranslucent = false
    view.backgroundColor = .clear
    navigationBar.tintColor = UIColor.white
    navigationBar.titleTextAttributes =
      [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold), NSAttributedStringKey.foregroundColor: UIColor.black]
    setNeedsStatusBarAppearanceUpdate()
  }

  func navigationController(_ navigationController: UINavigationController,
                            willShow viewController: UIViewController, animated: Bool) {
    nextViewController = viewController

    let tintColor: UIColor = viewController is LightContentViewController ? .white : .black
    navigationBar.tintColor = tintColor
    navigationBar.titleTextAttributes =
      [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold), NSAttributedStringKey.foregroundColor: tintColor]

    view.backgroundColor = viewController is TransparentViewController ? .clear : .white
    navigationBar.isTranslucent = viewController is TransparentViewController
    navigationBar.backgroundColor = viewController is TransparentViewController ? .clear : .white
    navigationBar.barStyle = viewController is TransparentViewController ? UIBarStyle.blackOpaque : UIBarStyle.default
    navigationBar.shadowImage = viewController is TransparentViewController ? UIImage() : nil
    navigationBar.setBackgroundImage(viewController is TransparentViewController ? UIImage() : nil, for: .default)
    setNeedsStatusBarAppearanceUpdate()
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    guard let viewController = nextViewController, viewController is LightContentViewController else {
      return .default
    }
    return .lightContent
  }

  override var prefersStatusBarHidden: Bool {
    return false
  }
  
}

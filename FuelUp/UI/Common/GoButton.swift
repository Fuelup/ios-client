//
//  GoButton.swift
//  PASSCITY
//
//  Created by Алексей on 28.09.17.
//  Copyright © 2017 PASSCITY. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

@IBDesignable class GoButton: UIButton {
  let animationDuration = 0.2

  var disabledAlpha: CGFloat = 0.5

  @IBInspectable var defaultBackgroundColor: UIColor = UIColor.tealish

  override var isEnabled: Bool {
    didSet {
      let isEnabled = self.isEnabled
      UIView.animate(withDuration: animationDuration, animations: { [weak self] in
        self?.backgroundColor = !isEnabled ? UIColor.disabledBtnColor : self?.defaultBackgroundColor
        self?.alpha = !isEnabled ? (self?.disabledAlpha ?? 0.5) : 1
        self?.layoutIfNeeded()
      })
    }
  }

  override var isHighlighted: Bool {
    didSet {
      let isHighlighted = self.isHighlighted
      guard isEnabled else { return }
      UIView.animate(withDuration: animationDuration, animations: { [weak self] in
        self?.backgroundColor = isHighlighted ? self?.defaultBackgroundColor.darker() : self?.defaultBackgroundColor
        self?.layoutIfNeeded()
      })
    }
  }

  override func layoutSubviews() {
    super.layoutSubviews()
  }

  init(color: UIColor = .tealish, title: String = "", image: UIImage? = nil, textColor: UIColor = .white, font: UIFont = UIFont.buttonCommonFont) {
    super.init(frame: .zero)
    setTitle(title.uppercased(), for: .normal)
    setTitleColor(textColor, for: .normal)
    setImage(image, for: .normal)
    self.titleLabel?.font = font
    defaultBackgroundColor = color
    backgroundColor = defaultBackgroundColor
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
}

class FuelUpButtonView: UIView {
  let backgroundButton: GoButton
  let titleLabel = UILabel()
  let arrowView = UIImageView(image: #imageLiteral(resourceName: "arrow").withRenderingMode(.alwaysTemplate))

  override var cornerRadius: CGFloat {
    didSet {
      backgroundButton.cornerRadius = cornerRadius
    }
  }

  @IBInspectable override var backgroundColor: UIColor? {
    didSet {
      backgroundButton.backgroundColor = backgroundColor
      backgroundButton.defaultBackgroundColor = backgroundColor ?? .tealish
    }
  }

  @IBInspectable var textColor: UIColor {
    get {
      return titleLabel.textColor
    }
    set {
      titleLabel.textColor = newValue
      arrowView.tintColor = newValue
    }
  }

	@IBInspectable var isEnabled: Bool {
		get {
			return backgroundButton.isEnabled
		}
		set {
			backgroundButton.isEnabled  = newValue
		}
	}

  @IBInspectable var title: String? {
    get {
      return titleLabel.text
    }
    set {
      titleLabel.text  = newValue
    }
  }

  override var tintColor: UIColor! {
    didSet {
      backgroundButton.tintColor = tintColor
    }
  }

  var handler: (() -> Void)? = nil
  var disabledHandler: (() -> Void)? = nil

  init(color: UIColor = .tealish, title: String = "", textColor: UIColor = .white, font: UIFont = UIFont.buttonCommonFont) {
    backgroundButton = GoButton(color: color, textColor: textColor, font: font)
    super.init(frame: .zero)
    titleLabel.text = title
    titleLabel.textColor = textColor
    arrowView.tintColor = textColor

    setupView()
  }

  @objc func backgroundButtonTapped() {
    handler?()
  }

  @objc func backgroundDisabledTapped() {
    disabledHandler?()
  }

  func setupView() {
    backgroundButton.clipsToBounds = true

    addSubview(backgroundButton)
    addSubview(titleLabel)
    addSubview(arrowView)

    backgroundButton.easy.layout(
      Edges()
    )

    addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(backgroundDisabledTapped)))

    backgroundButton.addTarget(self, action: #selector(backgroundButtonTapped), for: .touchUpInside)

    titleLabel.font = UIFont.systemFont(ofSize: 21, weight: .medium)

    titleLabel.easy.layout(
      Left(16),
      Right(10).to(arrowView),
      CenterY()
    )

    arrowView.easy.layout(
      Right(16),
      CenterY(),
      Size(21)
    )

  }

  required init?(coder aDecoder: NSCoder) {

    backgroundButton = GoButton()
    super.init(coder: aDecoder)
    setupView()
    titleLabel.textColor = .white
    arrowView.tintColor = .white

  }

}

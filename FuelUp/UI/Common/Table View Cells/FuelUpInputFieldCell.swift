//
//  FuelUpInputFieldCell.swift
//  FuelUp
//
//  Created by Алексей on 14.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class FuelUpInputTableViewCell: UITableViewCell {
  let inputField = UITextField()
  let titleLabel = UILabel()
  let formatterLabel = UILabel()

  var handler: ((String) -> Void)? = nil

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  init(_ title: String, formatterSymbols: String? = nil, image: UIImage? = nil) {
    super.init(style: .default, reuseIdentifier: nil)
    titleLabel.text = title
    formatterLabel.text = formatterSymbols
    setup()
  }

  func setup() {
    selectionStyle = .none
    addSubview(titleLabel)
    addSubview(inputField)
    addSubview(formatterLabel)

    titleLabel.textAlignment = .left
    titleLabel.font = UIFont.systemFont(ofSize: 15)
    titleLabel.textColor = .warmGreyTwo
    titleLabel.adjustsFontSizeToFitWidth = true

    formatterLabel.textAlignment = .right
    formatterLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    formatterLabel.textColor = .greyishBrown
    formatterLabel.adjustsFontSizeToFitWidth = true

    inputField.textAlignment = .right
    inputField.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    inputField.textColor = .greyishBrown
    inputField.autocapitalizationType = .none
    inputField.autocorrectionType = .no
    inputField.keyboardType = .numberPad

    titleLabel.easy.layout(
      CenterY(),
      Left(16)
    )

    formatterLabel.easy.layout(
      CenterY(),
      Right(16)
    )

    inputField.easy.layout(
      CenterY(),
      Right(1).to(formatterLabel),
      Left(>=1).to(titleLabel)
    )

    inputField.addTarget(self, action: #selector(inputFieldAction), for: .editingChanged)
  }

  @objc func inputFieldAction() {
    handler?(inputField.text ?? "")
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: true)
    if selected {
      inputField.becomeFirstResponder()
    }
  }
}

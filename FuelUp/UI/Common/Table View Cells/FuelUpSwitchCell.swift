//
//  FuelUpSwitchCell.swift
//  FuelUp
//
//  Created by Алексей on 14.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class FuelUpSwitchTableViewCell: UITableViewCell {
  let uiSwitch = UISwitch()
  let titleLabel = UILabel()
  let descriptionLabel = UILabel()
  let titleView = UIView()
  let iconView = UIImageView()

  var isOn: Bool {
    get {
      return uiSwitch.isOn
    }
    set {
      uiSwitch.isOn = newValue
    }
  }

  var handler: ((Bool) -> Void)? = nil

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  init(_ title: String, description: String? = nil, image: UIImage? = nil) {
    super.init(style: .default, reuseIdentifier: nil)
    titleLabel.text = title
    descriptionLabel.text = description
    iconView.image = image
    setup()
  }

  func setup() {
    selectionStyle = .none
    addSubview(titleView)
    titleView.addSubview(titleLabel)
    titleLabel.textAlignment = .left
    titleLabel.font = UIFont.systemFont(ofSize: 15)
    titleLabel.textColor = .warmGreyTwo
    titleLabel.adjustsFontSizeToFitWidth = true

    titleLabel.easy.layout(
      Top(),
      Left(),
      Right()
    )

    if let text = descriptionLabel.text, !text.isEmpty {
      descriptionLabel.textColor = .warmGrey
      descriptionLabel.font = UIFont.systemFont(ofSize: 10, weight: .light)
      titleView.addSubview(descriptionLabel)
      descriptionLabel.easy.layout(
        Top().to(titleLabel),
        Left(),
        Right(),
        Bottom()
      )
    } else {
      titleLabel.easy.layout(Bottom())
    }

    addSubview(uiSwitch)

    if iconView.image != nil {
      addSubview(iconView)
      iconView.contentMode = .scaleAspectFit
      iconView.easy.layout(
        Left(16).with(.high),
        Size(20),
        CenterY()
      )
      titleView.easy.layout(
        Left(15).to(iconView),
        Right(5).to(uiSwitch),
        CenterY()
      )
    } else {
      titleView.easy.layout(
        Left(16),
        Right(5).to(uiSwitch),
        CenterY()
      )
    }


    uiSwitch.onTintColor = UIColor.aquaMarine
    uiSwitch.tintColor = UIColor.warmGrey

    uiSwitch.easy.layout(
      Right(16),
      CenterY()
    )

    uiSwitch.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)
    uiSwitch.setContentCompressionResistancePriority(UILayoutPriority.required, for: .horizontal)

    uiSwitch.addTarget(self, action: #selector(switchAction), for: .valueChanged)
  }

  @objc func switchAction() {
    handler?(uiSwitch.isOn)
  }
}

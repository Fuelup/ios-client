//
//  NoCancelSearchController.swift
//  FuelUp
//
//  Created by Алексей on 10.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class NoCancelSearchBar: UISearchBar {

  override func layoutSubviews() {
    super.layoutSubviews()
    setShowsCancelButton(false, animated: false)
  }

  override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
    super.setShowsCancelButton(false, animated: false)
  }
}

class NoCancelSearchController: UISearchController, UISearchBarDelegate {

  lazy var _searchBar: NoCancelSearchBar = { [unowned self] in
    let result = NoCancelSearchBar(frame: CGRect.zero)
    result.delegate = self

    return result
  }()

  override var searchBar: UISearchBar {
    get {
      return _searchBar
    }
  }
}

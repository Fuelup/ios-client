//
//  CodeInputView.swift
//  FuelUp
//
//  Created by Алексей on 10.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

protocol MYDeleteActionTextFieldDelegate {
  func textFieldDidSelectDeleteButton(_ textField: UITextField) -> Void
}
class DeleteHandledTextField: UITextField {
  var emptyDeleteHandler: (() -> Void)? = nil

  override func deleteBackward() {
    if text == nil || text == "" {
      self.emptyDeleteHandler?()
    }
    super.deleteBackward()
  }
}

class CodeInputView: UIView {
	let fieldsStackView: UIStackView
	let codeFields = [DeleteHandledTextField(), DeleteHandledTextField(), DeleteHandledTextField(), DeleteHandledTextField()]
	let descriptionLabel = UILabel()
	let sendAgainButton = UIButton(type: .system)
	let buttonAttrs : [NSAttributedStringKey: Any] = [
		NSAttributedStringKey.font : UIFont.systemFont(ofSize: 13),
    NSAttributedStringKey.underlineColor: UIColor.tealish,
		NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
	let sendAgainDescriptionLabel = UILabel()
	let timeLabel = UILabel()

	var observers: [Any] = []
	var bottomConstraint: NSLayoutConstraint!

  var timer: Timer!

	var didEnterCodeHandler: ((String?) -> Void)? = nil
	var resendButtonHandler: (() -> Void)? = nil


	var phone: String? = nil {
		didSet {
			guard let phone = phone else { return }
			descriptionLabel.text = "Мы отправили вам  SMS с кодом подтверждения на номер телефона \(phone)"
		}
	}

  var seconds: Int = 60 {
    didSet {
      timeLabel.text = "\(seconds)"
      if seconds == 0 {
        timer.invalidate()
        sendAgainButton.isEnabled = true
        sendAgainButton.alpha = 1
      }
    }
  }

  func clear() {
    codeFields.forEach { $0.text = nil }
    codeFields.first?.becomeFirstResponder()
  }

  var code: String? {
    let characters = codeFields
      .map { $0.text?.first }
    return String(characters.filter { $0 != nil }.map { $0! })
  }

	func finishEnteringCode(lastChar: Character?) {
		var characters = codeFields
			.map { $0.text?.first }
		characters.append(lastChar)

		didEnterCodeHandler?(String(characters.filter { $0 != nil }.map { $0! }))
	}


	init() {
		fieldsStackView = UIStackView(arrangedSubviews: codeFields)
		super.init(frame: CGRect.null)
    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
		setup()


	}

  @objc func updateTimer() {
    seconds -= 1
  }

	func setup() {
		configureFields()
		descriptionLabel.font = UIFont.systemFont(ofSize: 11, weight: .light)
		descriptionLabel.textColor = .warmGrey
		descriptionLabel.numberOfLines = 0
		sendAgainDescriptionLabel.text = "Вы не получили код?"
		sendAgainDescriptionLabel.font = UIFont.systemFont(ofSize: 13, weight: .light)
		sendAgainDescriptionLabel.textColor = .warmGrey
    sendAgainDescriptionLabel.textAlignment = .center
		sendAgainDescriptionLabel.numberOfLines = 0
		sendAgainButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .light)
		timeLabel.font = UIFont.systemFont(ofSize: 15, weight: .light)
		timeLabel.textColor = .warmGrey
    timeLabel.text = "60"
		let buttonTitle = NSAttributedString(string: "Отправить еще раз", attributes: buttonAttrs)
		sendAgainButton.setAttributedTitle(buttonTitle, for: .normal)
    sendAgainButton.alpha = 0.5
    sendAgainButton.isEnabled = false
    sendAgainButton.addTarget(self, action: #selector(sendAgainAction), for: .touchUpInside)

    let timeView = UIView()
    let nestedView = UIView()

    addSubview(timeView)
    timeView.addSubview(nestedView)

    timeView.easy.layout(
      Top().to(fieldsStackView),
      Bottom(),
      Left(),
      Right()
    )

    nestedView.easy.layout(
      Center(),
      Left(5),
      Right(5)
  )

    nestedView.addSubview(descriptionLabel)
    nestedView.addSubview(sendAgainDescriptionLabel)
    nestedView.addSubview(sendAgainButton)
    nestedView.addSubview(timeLabel)

    descriptionLabel.textAlignment = .center
		descriptionLabel.easy.layout(
			Left(),
			Right(),
			Top()
		)

		sendAgainDescriptionLabel.easy.layout(
			Left(),
			Top(15).to(descriptionLabel)
		)

		sendAgainButton.easy.layout(
			Left(3).to(sendAgainDescriptionLabel),
      Right().with(.high),
			CenterY().to(sendAgainDescriptionLabel),
      Bottom()
		)

		timeLabel.easy.layout(
			Top().to(sendAgainButton),
      Bottom(),
      CenterX()
		)


	}

  @objc func sendAgainAction() {
    resendButtonHandler?()
    seconds = 60
    sendAgainButton.alpha = 0.5
    sendAgainButton.isEnabled = false
    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
  }

	func configureFields() {
		addSubview(fieldsStackView)
		fieldsStackView.axis = .horizontal
    fieldsStackView.distribution = .fillEqually
    fieldsStackView.alignment = .fill
		fieldsStackView.spacing = 16
		fieldsStackView.easy.layout(
			Top(39),
			Left(39),
      Right(39)
    )

		codeFields.enumerated().forEach { (index, field) in
			field.delegate = self
			field.easy.layout(
				Height(64),
				Width(54).with(.low),
        Width(>=30)
			)
      field.emptyDeleteHandler = {
        if index > 0 {
          DispatchQueue.main.async {
            self.codeFields[index - 1].becomeFirstResponder()
          }
        }
      }
			field.keyboardType = .numberPad
			field.textAlignment = .center
			field.font = UIFont.systemFont(ofSize: 28)
			field.backgroundColor = UIColor.whiteThree
		}


	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension CodeInputView: UITextFieldDelegate {
	func textFieldShouldClear(_ textField: UITextField) -> Bool {
		textField.text = ""
    guard let index = codeFields.index(of: textField as! DeleteHandledTextField), index > 0 else { return true }
		codeFields[index - 1].becomeFirstResponder()
		return true
	}

	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    guard let text = textField.text else { return true }
    let newLength = text.count + string.count - range.length
    guard let index = codeFields.index(of: textField as! DeleteHandledTextField) else { return true }
		if newLength == 1 {
			if index == codeFields.count - 1 {
				DispatchQueue.main.async {
					self.finishEnteringCode(lastChar: string.first)
				}
        if newLength > 1 {
          return false
        }
			} else {
				DispatchQueue.main.async {
					self.codeFields[index + 1].becomeFirstResponder()
				}
			}
		} else if newLength > 1 {
      if index < self.codeFields.count - 1 {
        guard let char = string.first else { return false }
        DispatchQueue.main.async {
          self.codeFields[index + 1].text = "\(char)"
          self.codeFields[index + 1].becomeFirstResponder()
          self.finishEnteringCode(lastChar: nil)
        }
        return false
      } else {
        return false
      }
    } else if newLength == 0 {
      DispatchQueue.main.async {
        if index > 0 {
          self.codeFields[index - 1].becomeFirstResponder()
        }
      }
    }
    didEnterCodeHandler?(text + string)
		return true
	}
}


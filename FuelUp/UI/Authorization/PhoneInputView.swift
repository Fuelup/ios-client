//
//  PhoneInputView.swift
//  FuelUp
//
//  Created by Алексей on 10.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class PhoneInputView: UIView {
	let inputField = UITextField()
	let ruLabel = UILabel()
	let lineView = UIView()
	let descriptionLabel = UILabel()
	let agreementlabel = UILabel()

  var agreementHandler: (() -> Void)? = nil
  var isValidHandler: ((Bool) -> Void)?  = nil

  var isValid: Bool = false {
    didSet {
      isValidHandler?(isValid)
    }
  }

	let maskedInput = MaskedInput(formattingType: .pattern("*** *** ** **"))

	var phone: String? {
		return inputField.text?.onlyDigits
	}

	init() {
		super.init(frame: CGRect.null)
		descriptionLabel.font = UIFont.systemFont(ofSize: 11)
    inputField.font = UIFont.systemFont(ofSize: 21, weight: .bold)
			  ruLabel.font = UIFont.systemFont(ofSize: 21, weight: .bold)
		agreementlabel.font = UIFont.systemFont(ofSize: 11, weight: .light)
		agreementlabel.textColor = .warmGrey
		descriptionLabel.numberOfLines = 0
		agreementlabel.numberOfLines = 0

		ruLabel.text = "RU (+7)"
		descriptionLabel.text = "Для регистрации необходимо указать номер вашего телефона. Мы не раскрываем персональные данные третьим лицам"
		agreementlabel.text = "Регистрируясь, вы принимаете Пользовательское Соглашение"
    let attributedString = NSMutableAttributedString(string: "Регистрируясь, вы принимаете Пользовательское Соглашение")
    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.tealish, range: NSRange(location: 28, length: 28))
    attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 28, length: 28))
    agreementlabel.attributedText = attributedString
		agreementlabel.lineBreakMode = .byWordWrapping
    agreementlabel.isEnabled = true
    agreementlabel.isUserInteractionEnabled = true
    agreementlabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(agreementAction)))
		addSubview(ruLabel)
		addSubview(inputField)
		addSubview(lineView)
		addSubview(descriptionLabel)
		addSubview(agreementlabel)

		ruLabel.easy.layout([
			Top(30),
			Left(20),
		])
		ruLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)

		inputField.easy.layout([
			CenterY().to(ruLabel),
			Left(16).to(ruLabel),
			Right(16)
		])

		lineView.easy.layout([
			Top(8).to(inputField),
			Left(16),
			Right(16),
			Height(1)
		])

		descriptionLabel.easy.layout([
			Top(16).to(lineView),
			Left(16),
			Right(16)
		])
		descriptionLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .vertical)

		agreementlabel.easy.layout([
			Top(8).to(descriptionLabel),
			Left(16),
			Right(16)
		])

		maskedInput.configure(textField: inputField)
		inputField.autocorrectionType = .no
		inputField.keyboardType = .numberPad
		maskedInput.isValidHandler = { [weak self] valid in
			self?.isValid = valid ?? false
		}
	}

  @objc func agreementAction() {
    agreementHandler?()
  }

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

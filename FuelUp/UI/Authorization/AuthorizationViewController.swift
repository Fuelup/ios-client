//
//  AuthorizationViewController.swift
//  FuelUp
//
//  Created by Алексей on 31.10.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy


// TODO REFACTOR
class AuthorizationViewController: UIViewController, LightContentViewController, TransparentViewController {
  let blueView = UIView()
  let containerView = UIView()
  let onboardingView = AuthorizationOnboardingView()
  let phoneInputView = PhoneInputView()
  let codeInputView = CodeInputView()
  let titleLabel = UILabel()
  let scrollView = UIScrollView()
  let emailInputView = EmailInputView()

  var layoutConstraint: NSLayoutConstraint!

  var registrationViews: [UIView] {
    return [phoneInputView, codeInputView, emailInputView]
  }

  var onboardingViews: [UIView] = []

  var observers: [Any] = []

  let onboardingImageView = UIImageView()
  let pageControl = UIPageControl()

	let buttonView = FuelUpButtonView(title: "Далее")

  var nextButtonHandler : (() -> Void)? {
    get {
      return buttonView.handler
    }
    set {
      buttonView.handler = newValue
    }
  }

  var shouldSkipRegistration: Bool = false

  var currentItem: AuthorizationItem = .onboarding {
    didSet {
      guard currentItem != oldValue else { return }
      configureCurrentItem()
    }
  }

  func configureCurrentItem() {
    guard isViewLoaded else { return }
    switch currentItem {
    case .onboarding:
      registrationViews.forEach { $0.removeFromSuperview() }
      scrollView.isHidden = false
      pageControl.isHidden = false
      if onboardingView.superview != containerView {
        containerView.addSubview(onboardingView)
        onboardingView.easy.layout(
          Top().to(topLayoutGuide),
          Left(),
          Right(),
          Bottom().to(buttonView)
        )
      }
      currentOnboardingItem = .welcome
      navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Пропустить", style: .plain, target: self, action: #selector(skipButtonAction))

    case .registration:
      onboardingView.removeFromSuperview()
      titleLabel.isHidden = false
      titleLabel.text = "Регистрация"
      titleLabel.textColor = UIColor.white
      titleLabel.font = UIFont.systemFont(ofSize: 21, weight: .bold)
      titleLabel.numberOfLines = 2
      titleLabel.textAlignment = .center

      scrollView.isHidden = true
      pageControl.isHidden = true
      navigationItem.rightBarButtonItem = UIBarButtonItem()

      buttonView.isEnabled = false
    }

  }

  var currentOnboardingItem: OnboardingItem = .welcome {
    didSet {
      onboardingView.configure(title: currentOnboardingItem.title, description: currentOnboardingItem.description, image: currentOnboardingItem.image)

      switch currentOnboardingItem {
      case .welcome:
        //imageView.isHidden = true
        pageControl.isHidden = true
      default:
        //imageView.isHidden = false
        pageControl.isHidden = false
      }
    }
  }

  var currentRegistratonItem: RegistrationItem? = nil {
    didSet {
      setRegistrationItemViews()
    }
  }

  func setRegistrationItemViews() {
    guard isViewLoaded else { return }
    switch currentRegistratonItem! {
    case .phoneInput:
      self.containerContentView = phoneInputView
      phoneInputView.inputField.becomeFirstResponder()
      self.buttonView.isEnabled = false
    case .codeInput:
      self.containerContentView = codeInputView
      titleLabel.text = "Введите код из смс"
      codeInputView.codeFields.first?.becomeFirstResponder()
      self.buttonView.isEnabled = false
    case .email:
      navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Пропустить", style: .plain, target: self, action: #selector(skipEmail))
      containerContentView = emailInputView
      emailInputView.inputField.becomeFirstResponder()
    }
  }


  var containerContentView: UIView? {
    didSet {
      if let oldValue = oldValue {
        oldValue.removeFromSuperview()
      }
      containerView.addSubview(containerContentView!)
      containerContentView!.easy.layout(
        Top(),
        Left(),
        Right(),
        Bottom().to(buttonView)
      )
    }
  }


  @objc func skipEmail() {
    RootViewController.instance?.setMapViewController()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let item = currentRegistratonItem, item == .email {
      emailInputView.email = ProfileService.instance.profile?.email
    }
    if let item = currentRegistratonItem, item == .phoneInput {
      buttonView.isEnabled = phoneInputView.isValid
    }
    observers = layoutConstraint.addObserversUpdateWithKeyboard(view: containerView)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    observers.forEach { NotificationCenter.default.removeObserver($0) }
    observers = []
  }

  override func viewDidLoad() {
    view.backgroundColor = .white
    view.addSubview(blueView)
    blueView.backgroundColor = .tealish
    view.addSubview(blueView)
    view.addSubview(containerView)
    containerView.addSubview(buttonView)

    scrollView.isHidden = currentItem == .registration
    
    hideKeyboardWhenTappedAround()
	
    blueView.easy.layout(
      Top(),
      Left(),
      Right(),
      Height(*0.57).like(view)
    )
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    blueView.addSubview(titleLabel)

    titleLabel.easy.layout(
      Center(),
      Left(>=5),
      Right(>=5)
    )

    titleLabel.isHidden = true

    blueView.addSubview(scrollView)
    scrollView.delegate = self
    scrollView.alwaysBounceVertical = false
    scrollView.easy.layout(
      Bottom(),
      Top(),
      Left(),
      Right()
    )

    scrollView.isPagingEnabled = true
    scrollView.isUserInteractionEnabled = true
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.showsVerticalScrollIndicator = false
    scrollView.bounces = false

    for (index, item) in OnboardingItem.allValues.enumerated() {
      let itemView = UIView()
      itemView.backgroundColor = .clear

      if index == 0 {
        let welcomeTitleLabel = UILabel()
        blueView.addSubview(welcomeTitleLabel)
        welcomeTitleLabel.text = "Добро пожаловать в \nFUELUP"
        welcomeTitleLabel.textColor = UIColor.white
        welcomeTitleLabel.font = UIFont.systemFont(ofSize: 21, weight: .bold)
        welcomeTitleLabel.numberOfLines = 2
        welcomeTitleLabel.textAlignment = .center
        itemView.addSubview(welcomeTitleLabel)
        welcomeTitleLabel.easy.layout(
          Center(),
          Left(>=5),
          Right(>=5)
        )
      } else {
        let imageView = UIImageView()
        imageView.image = item.stepImage
        itemView.addSubview(imageView)
        imageView.easy.layout(Center())
      }

      scrollView.addSubview(itemView)
      itemView.easy.layout(
        Top(),
        Left(self.view.frame.size.width * CGFloat(index)),
        Width().like(self.blueView),
        Height().like(self.blueView),
        Bottom()
      )

      if index == OnboardingItem.allValues.count - 1 {
        itemView.easy.layout(Right())
      }

      codeInputView.didEnterCodeHandler = { code in
        guard let code = code, code.count == 4 else {
          self.buttonView.isEnabled = false
          return
        }
        self.buttonView.isEnabled = true
      }
    }
    let height: CGFloat = UIDevice.current.screenType == .iPhones_5_5s_5c_SE ? 250 : 316
    containerView.easy.layout(
      Left(16),
      Right(16),
      Height(height)
    )

	layoutConstraint = containerView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: -20)
	layoutConstraint.isActive = true
    containerView.cornerRadius = 4
    containerView.shadow = true
    containerView.backgroundColor = UIColor.white
	buttonView.handler = nextButtonHandler
    nextButtonHandler = { [weak self ] in
		self?.nextButtonTap()
   }
    pageControl.numberOfPages = 3
    pageControl.currentPage = 0
    pageControl.addTarget(self, action: #selector(self.pageControlAction), for: UIControlEvents.valueChanged)
    pageControl.isHidden = true
    view.addSubview(pageControl)

    pageControl.easy.layout(
      Bottom(24).to(containerView),
      CenterX()
    )

	buttonView.cornerRadius = 4

	buttonView.easy.layout(
		Left(16),
		Bottom(16),
		Right(16),
		Height(54)
	)

  emailInputView.isValidHandler = { [unowned self] isValid in
    self.buttonView.isEnabled = isValid
  }
  onboardingView.configure(title: currentOnboardingItem.title, description: currentOnboardingItem.description)
	containerView.addSubview(onboardingView)
	onboardingView.easy.layout(
		Top(),
		Left(),
		Right(),
		Bottom().to(buttonView)
	)
	configureHanlders()

    navigationItem.rightBarButtonItem?.tintColor = UIColor.white
  }

	func configureHanlders() {
		phoneInputView.isValidHandler = { [weak self] isValid in
			  self?.buttonView.isEnabled = isValid
		}

    phoneInputView.agreementHandler = { [unowned self] in
      self.view.endEditing(true)
      let offerVC = OfferViewController()
      self.navigationController?.pushViewController(offerVC, animated: true)
    }

    codeInputView.resendButtonHandler = { [weak self] in
      self?.auth(resend: true)
    }

    configureCurrentItem()
    if currentRegistratonItem != nil {
      setRegistrationItemViews()
    }
  }

  @objc func pageControlAction() {
    changePage(pageControl.currentPage + 1)
  }

  func changePage(_ page: Int = -1) {
    let x = CGFloat(page == -1 ? pageControl.currentPage : page) * scrollView.frame.size.width
    scrollView.setContentOffset(CGPoint(x: x,y :0), animated: true)
    currentOnboardingItem = OnboardingItem(rawValue: page == -1 ? pageControl.currentPage : page)!
  }

	@objc func skipButtonAction() {
    if shouldSkipRegistration {
      dismiss(animated: true, completion: nil)
    } else {
      let viewController = AuthorizationViewController()
      viewController.currentItem = .registration
      viewController.currentRegistratonItem = .phoneInput
      self.navigationController?.pushViewController(viewController, animated: true)
    }
	}

	func nextButtonTap() {
		switch currentItem {
		case .onboarding:
			if let item = OnboardingItem(rawValue: self.currentOnboardingItem.rawValue + 1) {
				self.currentOnboardingItem = item
        self.pageControl.currentPage = self.currentOnboardingItem.rawValue - 1
        changePage(item.rawValue)
			} else {
        if shouldSkipRegistration {
          dismiss(animated: true, completion: nil)
        } else {
          let viewController = AuthorizationViewController()
          viewController.currentItem = .registration
          viewController.currentRegistratonItem = .phoneInput
          viewController.buttonView.isEnabled = false
          self.navigationController?.pushViewController(viewController, animated: true)
        }
			}
		case .registration:
      switch currentRegistratonItem! {
      case .codeInput:
        guard let code = codeInputView.code else {
          presentErrorAlert(message: "Код имеет неправильный формат")
          return
        }
        buttonView.isEnabled = false
        ProfileService.instance.checkCode(code: code, completion: { [weak self] result in
          switch result {
          case .failure(let error):
            self?.codeInputView.clear()
            self?.presentErrorAlert(message: error.localizedDescription)
            self?.buttonView.isEnabled = true
          case .success(_):
            ProfileService.instance.fetchProfile(completion: { _ in
              let viewController = AuthorizationViewController()
              viewController.currentItem = .registration
              viewController.currentRegistratonItem = .email
              self?.navigationController?.pushViewController(viewController, animated: true)
            })
          }
        })
      case .phoneInput:
        auth()
      case .email:
        ProfileService.instance.updateEmail(newEmail: emailInputView.email ?? "", completion: { _ in
          RootViewController.instance?.setMapViewController()
        })
      }
    }
  }

  func auth(resend: Bool = false) {
    buttonView.isEnabled = false
    guard let phone = resend ? codeInputView.phone : "+7\(phoneInputView.phone!)" else {
      self.presentErrorAlert(message: "Введите корректный номер телефона")
      return
    }
    ProfileService.instance.auth(phoneString: phone, completion: { result in
      switch result {
      case .success(_):
        DispatchQueue.main.async {
          if resend {
            self.codeInputView.clear()
            self.presentAlert(title: nil, message: "Код был отправлен еще раз")
            self.buttonView.isEnabled = false
          } else {
            let viewController = AuthorizationViewController()
            viewController.currentItem = .registration
            viewController.currentRegistratonItem = .codeInput
            viewController.buttonView.isEnabled = false
            self.navigationController?.pushViewController(viewController, animated: true)
          }
        }
      case .failure(let error):
        self.presentErrorAlert(message: error.localizedDescription)
        self.phoneInputView.inputField.text = nil
      }
    })

  }
}

extension AuthorizationViewController: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
    pageControl.currentPage = Int(pageNumber - 1)
    currentOnboardingItem = OnboardingItem(rawValue: Int(pageNumber))!
  }
}

//
//  EmailInputView.swift
//  FuelUp
//
//  Created by Алексей on 31.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

private let emailRegExp = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

class EmailInputView: UIView {
  let inputField = UITextField()
  let lineView = UIView()
  let titleLabel = UILabel()
  let descriptionLabelFirst = UILabel()
  let descriptionLabelSecond = UILabel()

  var isValidHandler: ((Bool) -> Void)?  = nil

  var email: String? {
    get {
      return inputField.text
    }
    set {
      inputField.text = newValue
      isValidHandler?(isValidEmail)
    }
  }

  var isValidEmail: Bool {
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegExp)
    return emailTest.evaluate(with: inputField.text ?? "")
  }

  init() {
    super.init(frame: CGRect.null)
    titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .ultraLight)
    titleLabel.textAlignment = .left
    titleLabel.numberOfLines = 0
    descriptionLabelFirst.font = UIFont.systemFont(ofSize: 11)
    descriptionLabelSecond.font = UIFont.systemFont(ofSize: 11)
    inputField.font = UIFont.systemFont(ofSize: 21, weight: .light)
    inputField.autocorrectionType = .no
    inputField.autocapitalizationType = .none
    descriptionLabelFirst.textColor = .warmGrey
    descriptionLabelSecond.textColor = .warmGrey
    descriptionLabelFirst.numberOfLines = 0
    descriptionLabelSecond.numberOfLines = 0

    titleLabel.text = "Получайте квитанции о заправке на адрес электронной почты"
    descriptionLabelFirst.text = "После заправки чек будет отправлен по указанному электронному адресу"
    descriptionLabelSecond.text = "На данный адрес будет выслана ссылка для подтверждения адреса электронной почты"
    descriptionLabelSecond.lineBreakMode = .byWordWrapping
    addSubview(inputField)
    addSubview(lineView)
    addSubview(titleLabel)
    addSubview(descriptionLabelFirst)
    addSubview(descriptionLabelSecond)

    titleLabel.easy.layout(
      Top(20),
      Left(16),
      Right(16)
    )

    inputField.easy.layout(
      Top(10).to(titleLabel),
      Left(16),
      Right(16)
    )

    lineView.easy.layout([
      Top(8).to(inputField),
      Left(16),
      Right(16),
      Height(1)
    ])

    descriptionLabelFirst.easy.layout(
      Top(10).to(lineView),
      Left(16),
      Right(16)
    )

    descriptionLabelFirst.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .vertical)

    descriptionLabelSecond.easy.layout(
      Top(8).to(descriptionLabelFirst),
      Left(16),
      Right(16),
      Bottom(10)
    )

    inputField.autocorrectionType = .no
    inputField.keyboardType = .emailAddress
    inputField.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
  }

  @objc func valueChanged() {
    isValidHandler?(isValidEmail)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

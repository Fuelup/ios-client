//
//  AuthorizationContentView.swift
//  FuelUp
//
//  Created by Алексей on 31.10.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class AuthorizationOnboardingView: UIView {
  let titleLabel = UILabel()
  let descriptionLabel = UILabel()
  let imageView = UIImageView()

  var updatingViews: [UIView] {
    return [titleLabel, descriptionLabel, imageView]
  }

  init() {
    super.init(frame: CGRect.null)
    titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
    titleLabel.numberOfLines = 0
    descriptionLabel.numberOfLines = 0
    descriptionLabel.font = UIFont.systemFont(ofSize: 15)
    descriptionLabel.textColor = .greyishBrown
    titleLabel.textAlignment = .center
    descriptionLabel.textAlignment = .center
    addSubview(titleLabel)
    addSubview(descriptionLabel)
    addSubview(imageView)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func configure(title: String, description: String, image: UIImage? = nil) {
    if !subviews.isEmpty {
      UIView.animate(withDuration: 0.2, animations: { [weak self] in
        self?.updatingViews.forEach { $0.alpha = 0 }
        self?.layoutIfNeeded()
      }, completion: { [weak self]  _ in
        self?.titleLabel.text = title
        self?.descriptionLabel.text = description
        self?.imageView.image = image
        self?.setup()
      })
    }
  }

  private func setup() {
    updatingViews.forEach { subview in
      subview.removeFromSuperview()
      self.addSubview(subview)
    }

    titleLabel.setContentHuggingPriority(.required, for: .vertical)
    titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)

    titleLabel.easy.layout(
      Top(20).with(.high),
      Left(13),
      Right(13)
    )

    if imageView.image != nil {
      imageView.easy.layout(
        Top(10).to(titleLabel),
        CenterX(),
        Height(31),
        Bottom(23).to(descriptionLabel)
      )
    } else {
      descriptionLabel.easy.layout(
        Top(23).to(titleLabel, .bottom)
      )
    }

    descriptionLabel.easy.layout(
      Left(13),
      Right(13)
    )
	
    updateConstraints()
    layoutIfNeeded()

    UIView.animate(withDuration: 0.2, animations: { [weak self] in
      self?.updatingViews.forEach { $0.alpha = 1 }
      self?.layoutIfNeeded()
    }, completion: { [weak self] _ in
      self?.superview?.updateConstraints()
      self?.superview?.layoutIfNeeded()
    })

  }
}

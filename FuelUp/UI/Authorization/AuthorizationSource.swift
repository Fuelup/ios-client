//
//  AuthorizationSource.swift
//  FuelUp
//
//  Created by Алексей on 11.11.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

enum AuthorizationItem {
	case onboarding
	case registration
}

enum OnboardingItem: Int {
	case welcome
	case step1
	case step2
	case step3

	var title: String {
		switch self {
		case .welcome:
			return "Сервис, позволяющий заправить ваш автомобиль и оплатить топливо, не покидая его салона!"
		case .step1:
			return "Подъедьте к доступной АЗС в приложении и припаркуйтесь у колонки"
		case .step2:
			return "Нажмите кнопку"
		case .step3:
			return "Заправщик сделает всю работу за вас"
		}
	}

	var description: String {
		switch self {
		case .welcome:
			return "Для оплаты необходимо привязать банковскую карту"
		case .step1:
			return "Ближайшие АЗС отображены на карте приложения"
		case .step2:
			return "Выберите номер колонки, на которой вы находитесь, а также тип и количество топлива"
		case .step3:
			return "Средства спишутся автоматически с вашей банковской карты\n\nЧек по итогу операции будет выслан вам на электронную почту"
		}
	}

	var image: UIImage? {
		switch self {
		case .step2:
			return #imageLiteral(resourceName: "beginButton")
		default:
			return nil
		}
	}

	var stepImage: UIImage? {
		switch self {
		case .step1:
			return #imageLiteral(resourceName: "onboardingLogo1")
		case .step2:
			return #imageLiteral(resourceName: "onboardingLogo2")
		case .step3:
			return #imageLiteral(resourceName: "onboardingLogo3")
		default:
			return nil
		}

	}


  static let allValues = [welcome, step1, step2, step3]
}

enum RegistrationItem {
	case phoneInput
	case codeInput
  case email
}

//
//  OfferViewController.swift
//  FuelUp
//
//  Created by Богдан Быстрицкий on 29/12/2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit

class OfferViewController: UIViewController {
  @IBOutlet weak var contentView: UITextView!
  
  override func viewDidLoad() {
    title = "Оферта"
    edgesForExtendedLayout = []
    extendedLayoutIncludesOpaqueBars = false
    super.viewDidLoad()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.view.layoutSubviews()
  }

  override func viewDidLayoutSubviews() {
    self.contentView.setContentOffset(.zero, animated: false)
  }
}

//
//  AboutAppViewController.swift
//  FuelUp
//
//  Created by Богдан Быстрицкий on 29/12/2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit
import WebKit

private let confidURL = URL(string: "http://confidential.fuelup.ru/")!

class AboutAppViewController: UIViewController {
  let webViewController = UIViewController()
  let webView = WKWebView()

  @IBOutlet weak var currentVersionAppLabel: UILabel!
  
  override func viewDidLoad() {
    title = "О приложении"
    super.viewDidLoad()
    
    let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    currentVersionAppLabel.text = "FuelUP v\(versionNumber)"
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(navigationLeftButtonAction))
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
  
  @objc func navigationLeftButtonAction() {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func offerButtonDidTapped(_ sender: Any) {
    let offerVC = OfferViewController()
    self.navigationController?.pushViewController(offerVC, animated: true)
  }

  @IBAction func confidentialButtonTapped(_ sender: Any) {
    webViewController.title = "Политика конфиденциальности"
    webView.navigationDelegate = self
    webViewController.view = webView
    webView.load(URLRequest(url: confidURL))
    navigationController?.pushViewController(webViewController, animated: true)
  }

  @IBAction func updateButtonDidTapped(_ sender: Any) {
    let urlStr = "itms://itunes.apple.com/us/app/fuelup/id1212049348?ls=1&mt=8"
    UIApplication.shared.open(URL(string: urlStr)!)
  }

  @objc func dismissWebViewController() {
    webViewController.dismiss(animated: true, completion: nil)

  }
}

extension AboutAppViewController: WKNavigationDelegate { }

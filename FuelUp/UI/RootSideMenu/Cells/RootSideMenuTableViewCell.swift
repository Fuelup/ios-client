//
//  RootSideMenuTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 11.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class RootSideMenuTableViewCell: UITableViewCell {
  let label = UILabel()
  let lineView = UIView()

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func setup() {
    backgroundColor = .clear
    addSubview(label)
    addSubview(lineView)

    label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    label.textColor = .white

    lineView.backgroundColor = UIColor.white.withAlphaComponent(0.36)

    label.easy.layout(
      Left(3),
      CenterY()
    )

    lineView.easy.layout(
      Height(1.8),
      Width(187),
      Left()
    )
  }
}

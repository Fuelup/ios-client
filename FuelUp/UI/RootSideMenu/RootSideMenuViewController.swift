//
//  RootSideMenuViewController.swift
//  FuelUp
//
//  Created by Алексей on 11.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

enum MenuRow: Int {
  case payment
//  case bonusCard
  case history
  case help
  case aboutApp
  case settings

  var text: String {
    switch self {
    case .payment:
      return "Оплата"
//    case .bonusCard:
//      return "Бонусы"
    case .history:
      return "История"
    case .help:
      return "Помощь"
    case .aboutApp:
      return "О приложении"
    case .settings:
      return "Настройки"
    }
  }

  static let allValues = [payment, history, help, aboutApp, settings]
}

class RootSideMenuViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.separatorStyle = .none
    RootSideMenuTableViewCell.register(in: tableView)
  }
}

extension RootSideMenuViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return MenuRow.allValues.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = RootSideMenuTableViewCell.instance(tableView, indexPath)!
    if indexPath.row == 0 {
      cell.lineView.isHidden = true
    }
    cell.label.text = MenuRow.allValues[indexPath.row].text
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 65
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let menuItem = MenuRow(rawValue: indexPath.row)!
    switch menuItem {
    case .payment:
      let paymentVC = CreditCardListViewController()
      present(FuelUpNavigationController(rootViewController: paymentVC), animated: true)
    case .history:
      let historyVC = PaymentsHistoryViewController()
      present(FuelUpNavigationController(rootViewController: historyVC), animated: true)
    case .settings:
      let settingsVC = SettingsViewController()
      present(FuelUpNavigationController(rootViewController: settingsVC), animated: true)
    case .aboutApp:
        let aboutAppVC = AboutAppViewController()
        present(FuelUpNavigationController(rootViewController: aboutAppVC), animated: true)
    case .help:
        let helpVC = HelpViewController()
        present(FuelUpNavigationController(rootViewController: helpVC), animated: true)
//    case .bonusCard:
//      let bonusCardVC = BonusCardsListViewController()
//      present(FuelUpNavigationController(rootViewController: bonusCardVC), animated: true)
    }
  }
  
}

class RootSideMenuNavigationController: UISideMenuNavigationController {
  override func viewDidLoad() {
    SideMenuManager.default.menuLeftNavigationController = self
    navigationBar.isHidden = true
  }
  
}

//
//  GasStationTableHeaderView.swift
//  FuelUp
//
//  Created by Алексей on 08.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import Kingfisher

class GasStationTableHeaderView: UIView {
  let iconAddressView = UIImageView(image: #imageLiteral(resourceName: "iconAddress"))
//  let iconModeView = UIImageView(image: #imageLiteral(resourceName: "iconMode"))
  let addressLabel = UILabel()
//  let durationLabel = UILabel()
  let routeButtonView = RouteBeginButtonView()
  
  private let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter(dateStyle: .none)
    dateFormatter.timeStyle = .medium
    dateFormatter.dateStyle = .none
    return dateFormatter
  }()

  var routeButtonHandler: (() -> Void)? {
    get {
      return routeButtonView.handler
    }
    set {
      routeButtonView.handler = newValue
    }
  }
  
  init() {
    super.init(frame: CGRect.null)
    backgroundColor = UIColor.white
    addSubview(iconAddressView)
//    addSubview(iconModeView)
    addSubview(addressLabel)
//    addSubview(durationLabel)
    addSubview(routeButtonView)

    iconAddressView.easy.layout(
      Top(36),
      Left(22),
      Size(22),
      Bottom(20)
    )

    addressLabel.easy.layout(
      Left(16).to(iconAddressView),
      Right(3).to(routeButtonView),
      CenterY().to(iconAddressView),
      Top(>=10),
      Bottom(>=10)
    )

    addressLabel.numberOfLines = 0
    addressLabel.font = UIFont.systemFont(ofSize: 15)

    routeButtonView.easy.layout(
      Right(20),
      Size(54),
      CenterY().to(addressLabel)
    )

//    iconModeView.easy.layout(
//      Top(33).to(iconAddressView),
//      Left().to(iconAddressView, .left),
//      Size(22),
//      Bottom(20)
//    )
//
//
//    durationLabel.easy.layout(
//      Left(16).to(iconModeView),
//      Right(>=20),
//      CenterY().to(iconModeView),
//      Bottom(>=10)
//    )
    
//    durationLabel.font = UIFont.systemFont(ofSize: 15)
//    durationLabel.numberOfLines = 0
  }

  func configure(station: GasStation, location: CLLocationCoordinate2D) {
    addressLabel.text = station.address
    //TODO: implement
//    let start = Date(timeIntervalSince1970: station.workStartTime)
//    let end = Date(timeIntervalSince1970: station.workEndTime)
//    durationLabel.text = "\(dateFormatter.string(from: start)) - \(dateFormatter.string(from: end))"
    routeButtonView.distance = station.distanceToString(location)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

//
//  GasStationHeaderView.swift
//  FuelUp
//
//  Created by Алексей on 07.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import Kingfisher

class GasStationScreenHeaderView: UIView {
  let imageView = UIImageView()
  let gradientView = GradientView(first: .black, second: .clear, bckColor: .clear, axis: .horizontal)

  var imageURL: URL? = nil {
    didSet {
      imageView.kf.setImage(with: imageURL){ [weak self] _, _, _, _ in
        self?.gradientView.isHidden = false
      }
    }
  }

  init() {
    super.init(frame: CGRect.null)
    backgroundColor = UIColor.tealish
    addSubview(imageView)
    addSubview(gradientView)
    gradientView.isHidden = true
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

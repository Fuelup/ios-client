//
//  GasStationScreenTableFooterView.swift
//  FuelUp
//
//  Created by Алексей on 08.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import UIKit
import EasyPeasy
import Kingfisher

class GasStationScreenTableFooterView: UIView {
  let infoImageView = UIImageView(image: #imageLiteral(resourceName: "iconNotice"))
  let descriptionLabel = UILabel()

  init() {
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 120))
    setup()
  }

  func setup() {
    descriptionLabel.font = UIFont.systemFont(ofSize: 13)
    descriptionLabel.textColor = .coolGrey
    descriptionLabel.numberOfLines = 2
    descriptionLabel.textAlignment = .left
    descriptionLabel.text = "Цены на топливо актуальны на 12:00 МСК \(Date().shortDate), Указаны в рублях РФ"

    addSubview(infoImageView)
    addSubview(descriptionLabel)

    infoImageView.easy.layout(
      Left(18),
      Size(18),
      Top(20)
    )

    descriptionLabel.easy.layout(
      Left(18).to(infoImageView),
      Right(<=20),
      Top().to(infoImageView, .top)
    )
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}


//
//  GasStationScreenTableViewCell.swift
//  FuelUp
//
//  Created by Алексей on 08.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class GasStationScreenTableViewCell: UITableViewCell {
  let titleLabel = UILabel()
  let priceLabel = UILabel()
  let rublesLabel = UILabel()

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier )
    titleLabel.font = UIFont.systemFont(ofSize: 15)
    priceLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    rublesLabel.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
    rublesLabel.textColor = .tealish
    priceLabel.textColor = .tealish

    rublesLabel.text = "₽"

    [titleLabel, priceLabel, rublesLabel].forEach { addSubview($0) }

    titleLabel.easy.layout(
      Left(20),
      CenterY(),
      Right(>=5).to(priceLabel)
    )

    addSubview(rublesLabel)

    rublesLabel.easy.layout(
      Right(20),
      CenterY()
    )

    priceLabel.easy.layout(
      Right(2).to(rublesLabel),
      CenterY()
    )
  }

  func configure(_ fuelType: FuelType) {
    titleLabel.text = fuelType.name
    priceLabel.text = "\(fuelType.price)"
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

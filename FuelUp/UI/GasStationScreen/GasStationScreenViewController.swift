//
//  GasStationScreenViewController.swift
//  FuelUp
//
//  Created by Алексей on 07.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class GasStationScreenViewController: UIViewController, LightContentViewController, TransparentViewController {
  let headerView = GasStationScreenHeaderView()
  let tableHeaderView = GasStationTableHeaderView()
  let tableFooterView = GasStationScreenTableFooterView()
  let tableView = UITableView()

  var currentLocation: CLLocationCoordinate2D!

  var gasStation: GasStation? = nil {
    didSet {
      guard let gasStation = gasStation else { return }
      tableHeaderView.configure(station: gasStation, location: currentLocation)
      navigationItem.title = gasStation.name
      if gasStation.minified {
        GasStationsServce.instance.fetchStation(id: gasStation.id) { (result) in
          switch result {
          case .success(let station):
            self.gasStation = station
          case .failure(_):
            break
          }
        }
      }
      tableView.reloadData()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }

  func setupView() {
    view.addSubview(headerView)
    headerView.easy.layout(
      Top(),
      Left(),
      Right(),
      Height(180)
    )

    view.addSubview(tableView)
    tableView.easy.layout(
      Top().to(headerView, .bottom),
      Left(),
      Right(),
      Bottom().to(bottomLayoutGuide)
    )

    tableHeaderView.routeButtonHandler = { [weak self] in
      guard let `self` = self, let station = self.gasStation else { return }
      let viewController = GasStationRouteViewController()
      viewController.configure(location: self.currentLocation, station: station)
      self.present(FuelUpNavigationController(rootViewController: viewController), animated: true)
    }

    GasStationScreenTableViewCell.register(in: tableView)
    tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 0)
    tableView.setAndLayoutTableHeaderView(header: tableHeaderView)
    tableView.tableFooterView = tableFooterView
    tableView.delegate = self
    tableView.dataSource = self
  }
}

extension GasStationScreenViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return gasStation?.fuelTypes.count ?? 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = GasStationScreenTableViewCell.instance(tableView)!
    guard let station = gasStation else { return cell }
    cell.configure(station.fuelTypes[indexPath.row])
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 50
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView()
    let label = UILabel()
    label.text = "ТОПЛИВО"
    label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
    label.textColor = .coolGrey

    view.addSubview(label)
    label.easy.layout(
      Bottom(6),
      Left(20)
    )
    return view
  }
}

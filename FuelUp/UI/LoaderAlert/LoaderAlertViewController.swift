//
//  LoaderAlertViewController.swift
//  FuelUp
//
//  Created by Алексей on 29.12.2017.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy

class LoaderAlertViewController: UIViewController {
  private let contentView = UIView()
  private let alertView = UIView()
  //private let circleView = UIImageView(image: #imageLiteral(resourceName: "loaderView"))
  private let activityIndicator = UIActivityIndicatorView()
  private let titleLabel = UILabel()
  private let buttonsView = UIStackView()

  override func viewWillAppear(_ animated: Bool) {
    UIView.animate(withDuration: 0.2) { [weak self] in
      self?.contentView.alpha = 0
      self?.contentView.alpha = 1
    }
  }

  var titleText: String? = nil {
    didSet {
      titleLabel.text = titleText
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }

  func setupView() {
    view.backgroundColor = UIColor.clear
    view.isOpaque = false

    contentView.backgroundColor = UIColor.black.withAlphaComponent(0.56)
    contentView.isOpaque = false

    view.addSubview(contentView)
    contentView.easy.layout(Edges())

    alertView.isOpaque = false
    alertView.backgroundColor = .clear
    alertView.cornerRadius = 4

    contentView.addSubview(alertView)

    alertView.easy.layout(
      CenterY(),
      Left(16),
      Right(16)
    )

    titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
    titleLabel.textAlignment = .center
    titleLabel.textColor = .white
    titleLabel.numberOfLines = 0

    alertView.addSubview(activityIndicator)
    alertView.addSubview(titleLabel)

    activityIndicator.easy.layout(
      Top(),
      CenterX(),
      Size(54)
    )

    titleLabel.easy.layout(
      Top(16).to(activityIndicator),
      Left(),
      Right(),
      Bottom()
    )

    contentView.addSubview(buttonsView)
    buttonsView.spacing = 10
    buttonsView.distribution = .fillEqually
    buttonsView.axis = .horizontal

    buttonsView.easy.layout(
      Left(16),
      Right(16),
      Height(54),
      Bottom(16)
    )

    activityIndicator.hidesWhenStopped = false
    activityIndicator.startAnimating()
    activityIndicator.center = view.center
  }

  func goBack(completion: @escaping (() -> Void) = { }) {
    UIView.animate(withDuration: 0.2, animations: { [weak self] in
      self?.contentView.alpha = 1
      self?.contentView.alpha = 0
      }, completion: { [weak self] _ in
        self?.dismiss(animated: false, completion: completion)
    })
  }

  func presentCancelAlert(handler: @escaping (() -> Void)) {
    let alertViewController = FuelUpAlertViewController()
    alertViewController.configure(item: .cancel)

    alertViewController.addAction(title: "Нет", handler: { })

    alertViewController.addAction(title: "Да", color: .fadedRed, handler: { [unowned self] in
      self.goBack(completion: handler)
    })

    alertViewController.modalPresentationStyle = .overCurrentContext
    self.present(alertViewController, animated: false)

  }

  func addAction(title: String, closes: Bool = true, handler: @escaping (() -> Void)) {
    let button = FuelUpButtonView(color: .white, title: title, textColor: .tealish)
    button.cornerRadius = 4
    button.handler = { [weak self] in
      if closes {
        self?.goBack(completion: handler)
      } else {
        handler()
      }
    }
    buttonsView.addArrangedSubview(button)
  }

  func removeActions() {
    for view in buttonsView.arrangedSubviews {
      view.isHidden = true
      buttonsView.removeArrangedSubview(view)
      view.removeFromSuperview()
    }
  }

  @objc func backgroundTapAction() {
  }


}

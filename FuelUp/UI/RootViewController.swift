//
//  Root.swift
//  FuelUp
//
//  Created by Алексей on 31.10.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit
import EasyPeasy
import SideMenu

class RootViewController: UIViewController {
  private(set) static var instance: RootViewController!
  let sideMenuVC = RootSideMenuNavigationController(rootViewController: RootSideMenuViewController.storyboardInstance()!)

  var currentViewController: UIViewController? = nil {
    didSet {
      oldValue?.removeFromParentViewController()
      oldValue?.view.removeFromSuperview()
      guard let currentViewController = currentViewController else { return }
      self.addChildViewController(currentViewController)
      currentViewController.view.frame = self.view.frame
      self.view.addSubview(currentViewController.view)
      currentViewController.didMove(toParentViewController: self)
    }
  }

  let authViewController = AuthorizationViewController()
  let mapViewController = GasStationsMapViewController()

  override func viewDidLoad() {
    super.viewDidLoad()
    if !ProfileService.instance.isAuthorized {
      currentViewController = FuelUpNavigationController(rootViewController: authViewController)
    } else {
      currentViewController = FuelUpNavigationController(rootViewController: mapViewController)
      configureSideMenu()
    }
  }

  func setMapViewController() {
    mapViewController.refresh()
    currentViewController = FuelUpNavigationController(rootViewController: mapViewController)
    configureSideMenu()
  }

  func setAuthViewController() {
    currentViewController = FuelUpNavigationController(rootViewController: authViewController)
  }

  func configureSideMenu() {
    SideMenuManager.default.menuLeftNavigationController = sideMenuVC
    SideMenuManager.default.menuRightNavigationController = nil
    SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view)
    SideMenuManager.default.menuFadeStatusBar = false
    SideMenuManager.default.menuAnimationBackgroundColor = UIColor.clear
    SideMenuManager.default.menuAllowPushOfSameClassTwice = false
  }

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return currentViewController?.preferredStatusBarStyle ?? .lightContent
  }

  override func loadView() {
    RootViewController.instance = self
    super.loadView()
  }

}

//
//  RootNavigationController.swift
//  FuelUp
//
//  Created by Алексей on 17.10.17.
//  Copyright © 2017 FUELUP-iOS-TEST. All rights reserved.
//

import Foundation
import UIKit

class RootNavigationController: UINavigationController {
  static private(set) var instance: RootNavigationController? = nil

  override func viewDidLoad() {
    super.viewDidLoad()
    RootNavigationController.instance = self
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

  }
}
